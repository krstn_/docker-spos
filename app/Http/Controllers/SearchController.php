<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;

class SearchController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/arsip');
        // }
        // $uri = $request->thn;
        //dd($uri);
        $xmlPath = Config::get('xmldata.breaking');
        $xmlPath2 = Config::get('xmldata.topic');
        $now = Carbon::now();
        // $y = $request->thn;
        // if(empty($y)) {
        //     $thn = $now->year;
        //     $bln = $now->month;
        //     $tgl = $now->day;
        // }else{
        //     $thn = $request->segment(2);
        //     $bln = $request->segment(3);
        //     $tgl = $request->segment(4);
        // }
        //dd($y);

        $keyword = $request->s;

        // $tagId = $tag[0]['id'];
        // $tagName = $tag[0]['name'];
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/posts?tags='.$tagId.'&per_page=50');
        //$data = Http::get('https://cmsx.solopos.com/api/breaking/tag/posts?tags='.$tagId);
        //$data = Http::get('https://cmsx.solopos.com/api/wp/v2/search?search='.$tagName.'&per_page=50&_embed');
        $res = Http::get('https://api.solopos.com/api/post/search?s='.$keyword);

        $video = Helper::read_xml($xmlPath, 'breaking-videos');
        $popular = Helper::read_xml($xmlPath, 'breaking-popular');
        $news = Helper::read_xml($xmlPath, 'breaking-news');
        $bisnis = Helper::read_xml($xmlPath, 'breaking-bisnis');
        $lifestyle = Helper::read_xml($xmlPath, 'breaking-lifestyle');
        $kolom = Helper::read_xml($xmlPath, 'breaking-kolom');
        $jateng = Helper::read_xml($xmlPath, 'breaking-jateng');
        $otomotif = Helper::read_xml($xmlPath, 'breaking-otomotif');
        $espospedia = Helper::read_xml($xmlPath, 'breaking-espospedia');
        $bola = Helper::read_xml($xmlPath, 'breaking-bola');
        $story = Helper::read_xml($xmlPath, 'breaking-story');
        //$widget = Helper::read_xml($xmlPath2, 'Ekspedisi-Ekonomi-Digital-2021');
        $datawidget = Http::get('https://api.solopos.com/api/breaking/tag/posts?tags=781384');
        $widget = $datawidget->json();

        $data = $res->json();

        // if(empty($data)) {
        //     //abort('404');
        //     return view('errors.empty');
        // }

        // dd($data);

        $catid = $data[0]['categories'][0] ?? 'News';
        //dd($catid);
        //dd($data);
        // foreach($tags as $e){
        //     $tagList[] = $e['_embedded']['self'][0];
        // }
        //dd($tagList);
        $header = array(
            'title' => 'Hasil Pencarian Berita '.ucwords($keyword),
            'category' => 'Search',
            'category_parent' => 'Search',
            'is_premium' => '',
            'focusKeyword' => ucwords($keyword),
            'description' => 'Kumpulan kabar berita Solopos.com situs portal terlengkap yang menyajikan informasi terhangat baik peristiwa politik, entertainment dan lain lain',
            'link'  => 'https://www.solopos.com/search',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
        );

        //return $tags;

        return view('pages.search', ['breaking' => $data, 's'=>$keyword, 'popular' => $popular, 'widget' => $widget, 'news' => $news, 'story' => $story, 'video' => $video, 'bola' => $bola, 'bisnis' => $bisnis, 'lifestyle' => $lifestyle, 'kolom' => $kolom, 'jateng' => $jateng, 'otomotif' => $otomotif, 'espospedia' => $espospedia, 'header' => $header]);
    }
}
