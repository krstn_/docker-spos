<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class VideoController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // Redirect when on mobile device
        // if(Helper::mobile_detect()) {
        //     return redirect()->away(Config::get('app.mobile_url').'/videos');
        // }

        $xmlPath = Config::get('xmldata.breaking');
        $story = Helper::read_xml($xmlPath, 'breaking-story');
        $video = Helper::read_xml($xmlPath, 'breaking-videos');

        // dd($video);

        $header = [
            'title' => 'Berita Video Terkini Hari Ini - Solopos.com',
            'description' => 'Menyajikan berita terpopuler hari ini, berita trending Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'link'  => 'https://www.solopos.com/videos',
            'category' => 'Berita Video',
            'category_parent' => 'Berita Video',
            'is_premium' => '',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Berita, Terkini, trending, terpopuler, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',  
        ];

        return view('pages.video', ['story' => $story, 'video' => $video, 'header' => $header]);
    }
}
