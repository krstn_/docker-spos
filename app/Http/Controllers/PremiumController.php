<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class PremiumController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $premium_content = 'premium';

        $xmlPath = Config::get('xmldata.breaking');

        $story = Helper::read_xml($xmlPath, 'breaking-story');
        $premium = Helper::read_xml($xmlPath, 'breaking-premium');
        $popular = Helper::read_xml($xmlPath, 'breaking-popular');
        $kolom = Helper::read_xml($xmlPath, 'breaking-kolom');
        $news = Helper::read_xml($xmlPath, 'breaking-news');
        $lifestyle = Helper::read_xml($xmlPath, 'breaking-lifestyle');

        $header = array(
            'title' => 'Espos Plus',
            'category' => 'Espos Plus',
            'category_parent' => 'Espos Plus',
            'is_premium' => '',
            'focusKeyword' => 'Berita Premium',
            'description' => 'Espos Premium adalah artikel yang menyajikan informasi dari fakta dan peristiwa yang diolah serta dipaparkan dengan cara kreatif dan lebih visual.',
            'link'  => 'https://www.solopos.com/plus',
            'image' => 'https://www.solopos.com/images/solopos.jpg',
            'editor' => 'Solopos.com',
            'author' => 'Solopos.com',
            'keyword' => 'Espos Plus, Berita Premium, Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
            'news_keyword' => 'Espos Plus, Berita Premium, Berita, Terkini, terlengkap, politik, bisnis, olahraga, bola, entertainment, gosip, lifestyle, tekno, otomotif, liga, viral, nasional, internasional, soloraya',
        );

        return view('pages.premium', ['story' => $story, 'premium' => $premium, 'premium_content' => $premium_content, 'popular' => $popular,  'kolom' => $kolom, 'header' => $header, 'news' => $news, 'lifestyle' => $lifestyle]);
    }
}
