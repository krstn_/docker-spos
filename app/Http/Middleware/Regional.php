<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Regional
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $url = request()->getHost();
        $domain = explode('.', $url);
        $subdomain = $domain[0];
        // dd($subdomain);

        $regionalList = [
            // 'www',
            'sekolah',
            // 'news',
            // 'soloraya',
            // 'lifestyle',
            // 'jatim',
            // 'otomotif',
            // 'entertainment',
            // 'bisnis',
            // 'sport',
            // 'jateng',
            // 'jogja',
            // 'teknologi',
            // 'video',
            // 'writing-contest',
            // 'loker',
            // 'cekfakta',
            // 'jagad-jawa',
            // 'foto',
            // 'videos',
            // 'espospedia',
            // 'foto',
            // 'pojokbisnis',
            // 'kolom',
            // 'fiksi'
        ];

        if(!in_array($subdomain, $regionalList)) {
            abort(404);
        }
        return $next($request);
    }
}
