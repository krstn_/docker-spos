<!-- sidebar start -->
<div class="col-lg-4">
	<div class="sidebar">
		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
			<!--<a target="_blank" href="https://rs-jih.co.id/rsjihsolo">
                <img src="{{ url('/images/bugar/sidebar-banner.gif') }}">
            </a>-->
				<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
				<script>
				window.googletag = window.googletag || {cmd: []};
				googletag.cmd.push(function() {
					googletag.defineSlot('/54058497/RSJIH-DESKTOP_SIDEBAR', [[300, 250], [300, 300]], 'div-gpt-ad-1640664658669-0').addService(googletag.pubads());
					googletag.pubads().enableSingleRequest();
					googletag.enableServices();
				});
				</script>
				<!-- /54058497/RSJIH-DESKTOP_SIDEBAR -->
				<div id='div-gpt-ad-1640664658669-0' style='min-width: 300px; min-height: 250px;'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1640664658669-0'); });
				</script>
				</div>
			</div>
			<!--div class="ads-image">
				<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						style="display:block"
						data-ad-client="ca-pub-4969077794908710"
						data-ad-slot="2921244965"
						data-ad-format="rectangle"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			</div-->
		</div><!-- widget end -->
				
		@include('includes.widget-popular-all')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Artikel </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $an_loop = 1; @endphp
		          @foreach($artikel as $an) @if($an_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$an['slug']}-{$an['id']}") }}" title="{{ $an['title'] }}">
									<img class="img-fluid" src="{{ $an['featured_image']['thumbnail'] }}" alt="{{ $an['title'] }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									<a href="{{ url("/{$an['slug']}-{$an['id']}") }}" title="{{ $an['title'] }}">{{ $an['title'] }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($an['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $an_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Lifestyle </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $bn_loop = 1; @endphp
		          @foreach($lifestyle as $bn) @if($bn_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}" title="{{ $bn['title'] }}">
									<img class="img-fluid" src="{{ $bn['images']['thumbnail'] }}" alt="{{ $bn['title'] }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}" title="{{ $bn['title'] }}">{{ $bn['title'] }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($bn['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $bn_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Foto </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $fn_loop = 1; @endphp
		          @foreach($foto as $fn) @if($fn_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$fn['slug']}-{$fn['id']}") }}" title="{{ $fn['title'] }}">
									<img class="img-fluid" src="{{ $fn['featured_image']['thumbnail'] }}" alt="{{ $fn['title'] }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									<a href="{{ url("/{$fn['slug']}-{$fn['id']}") }}" title="{{ $fn['title'] }}">{{ $fn['title'] }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($fn['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $fn_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Infografis </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $gn_loop = 1; @endphp
		          @foreach($grafis as $gn) @if($gn_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$gn['slug']}-{$gn['id']}") }}" title="{{ $gn['title'] }}">
									<img class="img-fluid" src="{{ $gn['featured_image']['thumbnail'] }}" alt="{{ $gn['title'] }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									<a href="{{ url("/{$gn['slug']}-{$gn['id']}") }}" title="{{ $gn['title'] }}">{{ $gn['title'] }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($gn['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $gn_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
				<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						style="display:block"
						data-ad-client="ca-pub-4969077794908710"
						data-ad-slot="2921244965"
						data-ad-format="rectangle"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			</div>
		</div><!-- widget end -->

	</div>
</div><!-- Sidebar Col end -->
