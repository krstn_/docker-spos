<!-- sidebar start -->
<div class="col-lg-4">
	<div class="sidebar">
		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
				<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- Iklan Responsif -->
				<ins class="adsbygoogle"
					style="display:block"
					data-ad-client="ca-pub-4969077794908710"
					data-ad-slot="2921244965"
					data-ad-format="rectangle"></ins>
				<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		</div><!-- widget end -->
		
		@include('includes.widget-popular-all')
		
		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
				<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- Iklan Responsif -->
				<ins class="adsbygoogle"
					style="display:block"
					data-ad-client="ca-pub-4969077794908710"
					data-ad-slot="2921244965"
					data-ad-format="rectangle"></ins>
				<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		</div><!-- widget end -->
		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Jateng </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $bn_loop = 1; @endphp
		          @foreach($jateng as $bn) @if($bn_loop <= 5)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}?utm_source=sidebar_desktop" title="{{ $bn['title'] }}">
									<img class="img-fluid" src="{{ $bn['images']['thumbnail'] }}" alt="{{ $bn['title'] }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}?utm_source=sidebar_desktop" title="{{ $bn['title'] }}">{{ $bn['title'] }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($bn['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif
                    @php $bn_loop++; @endphp
                    @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
				<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- Iklan Responsif -->
				<ins class="adsbygoogle"
					style="display:block"
					data-ad-client="ca-pub-4969077794908710"
					data-ad-slot="2921244965"
					data-ad-format="rectangle"></ins>
				<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		</div><!-- widget end -->

	</div>
</div><!-- Sidebar Col end -->
