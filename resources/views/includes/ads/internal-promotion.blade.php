<blockquote class="wp-block-quote pt-5 p-3">
    <p style="font-size:15px !important;font-weight:500;">Daftar dan berlangganan <a href="https://www.solopos.com/plus" title="Espos Plus" target="_blank" rel="noreferrer noopener"><strong>Espos Plus</strong></a> sekarang. Cukup dengan <strong>Rp99.000/tahun</strong>, Anda bisa menikmati berita yang lebih mendalam dan bebas dari iklan dan berkesempatan mendapatkan hadiah utama mobil Daihatsu Rocky, sepeda motor NMax, dan hadiah menarik lainnya. Daftar Espos Plus <a href="https://id.solopos.com/register" title="Espos Plus" target="_blank" rel="noreferrer noopener"><strong> di sini</strong></a>.</p>
    <p style="font-size:15px;font-weight:500;">Cek Berita dan Artikel yang lain di <a href="https://news.google.com/publications/CAAiEAdvb9cTDzY80fy_TN99HBAqFAgKIhAHb2_XEw82PNH8v0zffRwQ/sections/CAQqKggAIhAHb2_XEw82PNH8v0zffRwQKhQICiIQB29v1xMPNjzR_L9M330cEDDCrp0H?hl=en-ID&gl=ID&ceid=ID%3Aen" rel="noopener noreferrer" title="Solopos Google News" target="_blank"><strong>Google News</strong></a></p>
    {{-- <p style="font-size:15px;font-weight:500;">Simak berbagai berita pilihan dan terkini dari Solopos.com di Grup Telegram "Solopos.com Berita Terkini". Klik link <a href="https://t.me/soloposdotcom" title="Grup Telegram Solopos" target="_blank" rel="noreferrer noopener">https://t.me/soloposdotcom</a> kemudian join/bergabung. Pastikan Anda sudah menginstall aplikasi Telegram di ponsel.</p> --}}
    <cite><em style="display: inline !important;">Solopos.com - Panduan Informasi dan Inspirasi</em></cite>
</blockquote>

@if( date('Y-m-d H:i:s') >= '2021-12-23 00:00:01' && date('Y-m-d H:i:s') <= '2021-12-28 23:59:59')
<video width="100%" autoplay loop controls muted>
	<source src="https://cdn.solopos.com/iklan/ONLINE_SASA_SANTAN.mp4" type="video/mp4">
	Your browser does not support the video.
</video>
<br>
							
@elseif( date('Y-m-d H:i:s') >= '2021-11-12 00:00:01' && date('Y-m-d H:i:s') <= '2021-12-12 23:59:59')
<a href="https://linktr.ee/EsportArena" target="_blank">
    <video width="100%" autoplay loop controls muted>
	<source src="{{ url('images/banner/ESPORT_JOGJA.mp4') }}" type="video/mp4">
	Your browser does not support the video.
	</video></a><br>
@endif