@if( date('Y-m-d H:i:s') >= '2022-03-16 22:00:01' && date('Y-m-d H:i:s') <= '2022-03-17 23:59:59')

<div class="item post-overaly-style post-md" style="background-image:url(https://images.solopos.com/2022/03/Cover-RLPPD-Klaten-2021.png)">
	<div class="featured-post">
		<a class="image-link" href="https://www.solopos.com/ringkasan-laporan-penyelenggaraan-pemerintahan-daerah-klaten-2021-1274582?utm_source=terkini_desktop" title="Ringkasan Laporan Penyelenggaraan Pemerintahan Daerah Klaten 2021">&nbsp;</a>
		<div class="overlay-post-content">
			<div class="post-content">
				<div class="grid-category">
					<a class="post-cat bisnis" href="{{ url("/soloraya/klaten") }}">Klaten</a>
				</div>

				<h2 class="post-title title-md">
					<a href="https://www.solopos.com/ringkasan-laporan-penyelenggaraan-pemerintahan-daerah-klaten-2021-1274582?utm_source=terkini_desktop" title="Ringkasan Laporan Penyelenggaraan Pemerintahan Daerah Klaten 2021">Ringkasan Laporan Penyelenggaraan Pemerintahan Daerah Klaten 2021</a>
				</h2>
				<div class="post-meta">
					<ul>
						<li><a href="#"><i class="fa fa-user"></i> BC</a></li>
						<li><a href="#"><i class="icon icon-clock"></i> 17 Maret 2022</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div><!--/ Featured post end -->
</div><!-- Item 1 end -->

@endif