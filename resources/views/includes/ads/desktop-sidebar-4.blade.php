<div class="sidebar-widget ads-widget">
    @if( date('Y-m-d H:i:s') >= '2022-08-25 00:00:01' && date('Y-m-d H:i:s') <= '2022-08-27 23:59:59')

	<div class="ads-image">
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/Sidebar-Desktop-4', [[300, 250], [300, 330], [300, 300], [300, 600]], 'div-gpt-ad-1648704676555-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/Sidebar-Desktop-4 -->
        <div id='div-gpt-ad-1648704676555-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1648704676555-0'); });
        </script>
        </div>
    </div>
@elseif ( date('Y-m-d H:i:s') >= '2022-06-20 00:00:01' && date('Y-m-d H:i:s') <= '2022-06-29 23:59:59')
    <a href="https://www.solopos.com/tag/Ekspedisi-Energi-2022" target="_blank" title="Ekspedisi Energi 2022"><img src="https://cdn.solopos.com/banner/Ekspedisi-Energi-2022-MR.gif" width="300px" height="250px" alt="Ekspedisi Energi 2022"></a>

@else
	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Iklan Responsif -->
	<ins class="adsbygoogle"
		style="display:block"
		data-ad-client="ca-pub-4969077794908710"
		data-ad-slot="2921244965"
		data-ad-format="rectangle"></ins>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
@endif

</div>