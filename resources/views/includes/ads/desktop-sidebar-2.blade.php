<div class="sidebar-widget ads-widget">
    <div class="ads-image">
        @if( date('Y-m-d H:i:s') >= '2022-08-24 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-21 23:59:59')
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/Sidebar-desktop-2', [[300, 600], [300, 300], [300, 250]], 'div-gpt-ad-1650347758198-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/Sidebar-desktop-2 -->
        <div id='div-gpt-ad-1650347758198-0' style='min-width: 300px; min-height: 250px;'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1650347758198-0'); });
        </script>
        </div>
        @elseif ( date('Y-m-d H:i:s') >= '2022-06-22 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-15 23:59:59')
        <a href="https://wa.me/6285974110958" target="_blank" title="Commuter RUN 2022"><img src="https://cdn.solopos.com/banner/KAI300x300.jpg?v={{time()}}" width="300px" height="300px" alt="Commuter RUN 2022"></a>
        @else
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Iklan Responsif -->
            <ins class="adsbygoogle"
                style="display:block"
                data-ad-client="ca-pub-4969077794908710"
                data-ad-slot="2921244965"
                data-ad-format="rectangle"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        @endif
    </div>
</div><!-- widget end -->