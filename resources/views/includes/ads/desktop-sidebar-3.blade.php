<div class="sidebar-widget ads-widget">
    <div class="ads-image">
        @if( date('Y-m-d H:i:s') >= '2022-09-19 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-25 23:59:59')
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/54058497/Desktop-Sidebar-3', [[300, 300], [300, 250]], 'div-gpt-ad-1638957629047-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
        </script>
        <!-- /54058497/Desktop-Sidebar-3 -->
        <div id='div-gpt-ad-1638957629047-0' style='min-width: 300px; min-height: 250px;'>
            <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1638957629047-0'); });
            </script>
        </div>
        @else
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Iklan Responsif -->
        <ins class="adsbygoogle"
            style="display:block"
            data-ad-client="ca-pub-4969077794908710"
            data-ad-slot="2921244965"
            data-ad-format="rectangle"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
        @endif
    </div>
</div><!-- widget end -->