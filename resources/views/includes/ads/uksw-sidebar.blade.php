    @if( date('Y-m-d H:i:s') <= '2024-05-31 23:59:59')
	<div class="sidebar-widget ads-widget">
		<div class="ads-image">
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
		<script>
		window.googletag = window.googletag || {cmd: []};
		googletag.cmd.push(function() {
			googletag.defineSlot('/54058497/UKSW-MR-DESKTOP', [300, 250], 'div-gpt-ad-1634116572610-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		});
		</script>
		<!-- /54058497/UKSW-MR-DESKTOP -->
		<div id='div-gpt-ad-1634116572610-0' style='min-width: 300px; min-height: 250px;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1634116572610-0'); });
			</script>
		</div><br>
        </div>
	</div><!-- widget end -->
    @endif