<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>{{ $header['title'] }}</title>
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="https://m.solopos.com/images/favicon.ico">
  <!-- Dependency Styles -->
  <link rel="stylesheet" href="https://cdn.solopos.com/tematik/assets/wow/animate.css">
  <link rel="stylesheet" href="https://cdn.solopos.com/tematik/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
  <link rel="stylesheet" href="https://cdn.solopos.com/tematik/assets/magnific-popup/css/magnific-popup.css">
  <link rel="stylesheet" href="https://cdn.solopos.com/tematik/assets/swiper/css/swiper.min.css">
  <link rel="stylesheet" href="https://cdn.solopos.com/tematik/css/app.css">
  <!-- <link rel="stylesheet" href="css/pricing.css"> -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
  <!-- Google Web Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
</head>

<body>

  <div id="wrapper">
    <div id="main_content">
      <!--=====================================-->
      <!--=            Navbar Start           =-->
      <!--=====================================-->
      <header class="header header1 sticky-on trheader">
        <div id="sticky-placeholder"></div>
        <div id="navbar-wrap" class="navbar-wrap">
          <div class="header-menu">
            <div class="header-width">
              <div class="container-fluid">
                <div class="inner-wrap">
                  <div class="d-flex align-items-center justify-content-between">
                    <div class="site-branding">
                      <a href="{{ url('/solorayakeren') }}" class="logo logo-light"><img src="https://cdn.solopos.com/logo/logo-white.png" alt="Logo" width="200" height="37"></a>
                      <a href="{{ url('/solorayakeren') }}" class="logo logo-dark"><img src="https://cdn.solopos.com/logo/logo.png" alt="Logo" width="200" height="37"></a>
                    </div>
                    <nav id="dropdown" class="template-main-menu menu-text-light">
                      <ul class="menu">
                        <li class="menu-item">
                          <a href="{{ url('/solorayakeren') }}">HOME</a>
                        </li>
                        <li class="menu-item">
                          <a href="{{ url('/solorayakeren/tentang') }}">TENTANG</a>
                        </li>
                        <li class="menu-item">
                          <a href="{{ url('/solorayakeren#pembicara') }}">PEMBICARA</a>
                        </li>
                        <li class="menu-item">
                          <a href="{{ url('/solorayakeren#event') }}">EVENTS</a>
                        </li>
                        <li class="menu-item">
                          <a href="{{ url('/solorayakeren/kontak') }}">KONTAK</a>
                        </li>
                        <li class="menu-item">
                          <a href="{{ url('/solorayakeren/news') }}">NEWS</a>
                        </li>
                      </ul>
                    </nav>
                    <ul class="header-action-items">
                      <li class="search-wrap">
                        <a href="#template-search" title="Search">
                          <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20 20L15.514 15.506L20 20ZM18 9.5C18 11.7543 17.1045 13.9163 15.5104 15.5104C13.9163 17.1045 11.7543 18 9.5 18C7.24566 18 5.08365 17.1045 3.48959 15.5104C1.89553 13.9163 1 11.7543 1 9.5C1 7.24566 1.89553 5.08365 3.48959 3.48959C5.08365 1.89553 7.24566 1 9.5 1C11.7543 1 13.9163 1.89553 15.5104 3.48959C17.1045 5.08365 18 7.24566 18 9.5V9.5Z" stroke-width="2" stroke-linecap="round" />
                          </svg>
                        </a>
                      </li>
                      <li class="header-action-item d-none d-xl-block">
                        <a href="#" class="item-btn btn-fill style-one">DAFTAR</a>
                      </li>
                      <li class="header-action-item">
                        <button type="button" class="offcanvas-menu-btn style-one menu-status-open">
                          <span class="menu-btn-icon">
                          <span></span>
                          <span></span>
                          <span></span>
                          </span>
                        </button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div class="rt-header-menu mean-container" id="meanmenu">
        <div class="mean-bar">
          <a href="{{ url('/solorayakeren') }}">
            <img src="https://cdn.solopos.com/logo/logo.png" alt="Logo" width="130" height="29">
          </a>
          <span class="sidebarBtn">
            <span class="bar"></span>
          <span class="bar"></span>
          <span class="bar"></span>
          <span class="bar"></span>
          </span>
        </div>
        <div class="rt-slide-nav">
          <div class="offscreen-navigation">
            <nav class="menu-main-primary-container">
              <ul class="menu">
                <li class="list">
                  <a class="animation" href="{{ url('/solorayakeren') }}">HOME</a>
                </li>
                <li class="list">
                  <a class="animation" href="{{ url('/solorayakeren/tentang') }}">TENTANG</a>
                </li>
                <li class="list">
                  <a class="animation" href="{{ url('/solorayakeren#pembicara') }}">PEMBICARA</a>
                </li>
                <li class="listn">
                  <a class="animation" href="{{ url('/solorayakeren#event') }}">EVENTS</a>
                </li>
                <li class="list">
                  <a class="animation" href="{{ url('/solorayakeren/kontak') }}">KONTAK</a>
                </li>
                <li class="list">
                  <a class="animation" href="{{ url('/solorayakeren/news') }}">NEWS</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <!--=====================================-->
      <!--=              Navbar End           =-->
      <!--=====================================-->