      <!--=====================================-->
      <!--=   Newsletter Section Area Start   =-->
      <!--=====================================-->
      {{-- <section class="newsletter-wrap-layout1">
        <div class="container">
          <div class="newsletter-box-layout1 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="0.75s">
            <div class="shape-box">
              <svg class="wow fadeInDown animated" data-wow-delay="1.2s" data-wow-duration="0.5s" width="201" height="191" viewBox="0 0 201 191" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M152.65 60.0388C148.702 53.2008 140.282 50.5247 133.196 53.5571L113.382 19.2374C112.168 17.1361 109.778 16.1437 107.433 16.7683C105.089 17.3932 103.509 19.4425 103.502 21.8685C103.454 38.5268 96.9571 53.6446 84.1936 66.8024C74.542 76.7519 64.7388 81.7774 64.6436 81.8249C64.4992 81.8976 64.364 81.9845 64.2343 82.0779L39.8186 96.1743C27.5532 103.256 23.3357 118.996 30.4171 131.261C37.0871 142.814 51.4374 147.223 63.3317 141.779L88.1796 167.81C91.4696 171.257 96.6494 172 100.776 169.618L105.887 166.667C108.641 165.077 110.479 162.36 110.931 159.212C111.383 156.065 110.382 152.94 108.187 150.64L86.8812 128.32L89.915 126.569C90.0614 126.503 90.2048 126.429 90.341 126.34C90.4326 126.28 99.6858 120.302 113.128 116.919C130.905 112.444 147.245 114.377 161.696 122.664C163.377 123.628 165.35 123.603 166.949 122.68C167.354 122.446 167.733 122.156 168.079 121.81C169.792 120.091 170.128 117.525 168.915 115.424L149.1 81.1036C155.27 76.4834 157.162 67.8538 153.214 61.0159L152.65 60.0388ZM100.913 151.91L103.746 154.879C104.633 155.808 105.037 157.069 104.854 158.341C104.672 159.611 103.93 160.709 102.818 161.351L97.7067 164.301C96.0403 165.264 93.9488 164.963 92.6204 163.572L68.8876 138.709L81.41 131.479L87.7451 138.116L86.4745 138.85C85.0063 139.697 84.5034 141.574 85.351 143.043C86.1986 144.511 88.0757 145.014 89.5439 144.166L92.1071 142.686L96.5502 147.34L95.7171 147.821C94.249 148.669 93.746 150.546 94.5936 152.014C95.4413 153.482 97.3184 153.985 98.7865 153.138L100.913 151.91ZM77.9453 111.351L61.4257 120.888C59.9576 121.736 59.4546 123.613 60.3022 125.081C61.1499 126.549 63.027 127.052 64.4951 126.205L81.0147 116.667L84.4579 122.631L80.5746 124.873C80.5244 124.899 80.474 124.923 80.4246 124.952L62.4311 135.34C62.4058 135.355 62.383 135.373 62.358 135.388C53.0351 140.716 41.1079 137.5 35.7335 128.192C30.3445 118.858 33.5541 106.88 42.888 101.491L64.9113 88.7755L77.9453 111.351ZM129.357 59.1862L142.306 81.6143L162.099 115.897C146.98 108.107 129.527 106.414 111.421 111.019C101.396 113.568 93.6676 117.383 89.7678 119.551L84.7963 110.94L70.2341 85.7172C74.0617 83.4234 81.2297 78.6378 88.4495 71.231C101.49 57.8527 108.751 41.8918 109.564 24.9038L129.357 59.1862ZM147.897 64.0856C150.149 67.9862 149.25 72.8555 145.995 75.7272L136.3 58.9344C140.414 57.5515 145.081 59.2079 147.333 63.1085L147.897 64.0856Z" />
                <path d="M144.774 44.5595C145.412 44.1907 145.921 43.5918 146.163 42.8339L149.798 31.4248C150.313 29.8096 149.421 28.0832 147.806 27.5683C146.19 27.0538 144.464 27.9459 143.949 29.561L140.314 40.9696C139.799 42.5848 140.691 44.3113 142.306 44.8261C143.164 45.0992 144.052 44.9763 144.774 44.5595Z" />
                <path d="M164.871 75.237C163.214 74.8754 161.579 75.9244 161.217 77.5805C160.855 79.2366 161.904 80.8725 163.56 81.2344L175.258 83.7903C176.036 83.96 176.809 83.819 177.448 83.4502C178.17 83.0334 178.72 82.3256 178.912 81.4468C179.274 79.7907 178.225 78.1548 176.569 77.7928L164.871 75.237Z" />
                <path d="M162.323 51.5605L156.436 54.9594C154.968 55.8071 154.465 57.6842 155.313 59.1523C156.16 60.6204 158.037 61.1234 159.506 60.2758L165.393 56.8769C166.861 56.0293 167.364 54.1521 166.516 52.684C165.669 51.2159 163.791 50.7129 162.323 51.5605Z" />
                <path d="M53.8765 127.437C53.7164 127.313 53.545 127.206 53.3664 127.118C53.1837 127.029 52.9964 126.956 52.8035 126.904C52.608 126.854 52.408 126.821 52.2065 126.809C52.0077 126.797 51.806 126.803 51.6081 126.829C51.4076 126.856 51.211 126.902 51.0236 126.964C50.6408 127.093 50.2872 127.297 49.9842 127.564C49.834 127.697 49.6957 127.844 49.5746 128.003C49.4535 128.161 49.3442 128.334 49.2559 128.513C49.165 128.693 49.0939 128.883 49.0422 129.076C48.9905 129.269 48.9571 129.469 48.9459 129.67C48.933 129.869 48.9397 130.071 48.9668 130.271C48.9924 130.469 49.0386 130.666 49.1023 130.856C49.1659 131.046 49.2486 131.232 49.3484 131.405C49.4481 131.578 49.568 131.742 49.7024 131.895C49.8336 132.043 49.9808 132.181 50.1409 132.305C50.2994 132.426 50.4708 132.533 50.6494 132.621C50.8295 132.712 51.0194 132.783 51.2123 132.835C51.4051 132.886 51.6052 132.92 51.8081 132.934C52.0069 132.946 52.2113 132.938 52.4092 132.913C52.6071 132.887 52.8037 132.841 52.9938 132.777C53.1865 132.712 53.3685 132.628 53.5413 132.528C53.7167 132.427 53.8803 132.311 54.0331 132.177C54.1833 132.044 54.3189 131.899 54.4427 131.739C54.5638 131.58 54.6704 131.409 54.7587 131.23C54.8481 131.047 54.9193 130.857 54.9736 130.663C55.0253 130.47 55.056 130.272 55.0699 130.069C55.0828 129.87 55.0761 129.668 55.0505 129.47C55.0234 129.27 54.9772 129.073 54.915 128.886C54.8498 128.693 54.7645 128.508 54.6648 128.335C54.565 128.163 54.4478 127.996 54.315 127.846C54.1822 127.696 54.035 127.558 53.8765 127.437Z" />
              </svg>
            </div>
            <div class="sub-title">Don’t Miss Our Future Updates!</div>
            <h2 class="title">Get Subscribed Today!</h2>
            <form class="newsletter-form">
              <div class="input-group stylish-input-group">
                <input type="text" class="form-control" placeholder="Email address">
                <span class="input-group-addon">
                        <button type="submit"><span>SUBSCRIBE</span></button>
                </span>
              </div>
            </form>
          </div>
        </div>
      </section> --}}
      <!--=====================================-->
      <!--=    Newsletter Section Area End    =-->
      <!--=====================================-->

      <!--=====================================-->
      <!--=     Sponsorship Section Area Start      =-->
      <!--=====================================-->
      <section class="process-wrap-layout1" data-bg-image="https://coolbackgrounds.io/images/backgrounds/white/white-unsplash-9d0375d2.jpg" >
        <div class="process-inner-wrap container">
          <div class="container-fluid row row-cols-xl-5 row-cols-md-3 row-cols-2">
            <div class="col wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-ojk.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-ovo.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.7s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-candi-elektronik.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.9s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-bni.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="1.1s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-shipper.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-telkom.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-blesscon.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.7s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-jne.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="0.9s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
                <a href=""><img src="https://cdn.solopos.com/logo/logo-alila.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
            <div class="col wow fadeInLeft animated" data-wow-delay="1.1s" data-wow-duration="0.5s">
              <div class="brand-box-layout1">
               <a href=""> <img src="https://cdn.solopos.com/logo/logo-indriyati.png" alt="Sponsorship" width="130" height="112"></a>
              </div>
            </div>
          </div>
          <div class="text-center">
            <a href="#" class="btn-fill style-four">BECOME A SPONSOR</a>
          </div>
          </div>
        </div>
      </section>      
      <!--=====================================-->
      <!--=      Sponsorship Section Area End       =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=     Footer Section Area Start     =-->
      <!--=====================================-->
      <footer class="footer-wrap-layout3" data-bg-image="https://cdn.solopos.com/tematik/image/banner/footer-bg1.jpg">
        <div class="container">
          <div class="footer3 footer-middle">
            <div class="row">
              <div class="col-lg-5 wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-duration="1s">
                <div class="footer-widgets">
                  <h3 class="widget-title">Get Subscribed Today!</h3>
                  <p>Lorem ipsum consectetur adipiscing venenatis magna fringilla.</p>
                  <div class="footer-newsletter">
                    <form>
                      <input type="email" class="form-control" placeholder="Your Email Address">
                      <button type="submit" class="btn-fill style-three">SUBSCRIBE</button>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-duration="1s">
                <div class="footer-widgets">
                  <h3 class="widget-title">Services</h3>
                  <div class="footer-menu">
                    <ul>
                      <li><a href="#">Log In</a></li>
                      <li><a href="#">Register</a></li>
                      <li><a href="#">About</a></li>
                      <li><a href="#">Contact Support</a></li>
                      <li><a href="#">How It Works</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-2 wow fadeInLeft animated" data-wow-delay="0.7s" data-wow-duration="1s">
                <div class="footer-widgets">
                  <h3 class="widget-title">Useful Links</h3>
                  <div class="footer-menu">
                    <ul>
                      <li><a href="#">Sponsors</a></li>
                      <li><a href="#">Speakers</a></li>
                      <li><a href="#">Registration</a></li>
                      <li><a href="#">Schedule</a></li>
                      <li><a href="#">Event Gallery</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 wow fadeInLeft animated" data-wow-delay="0.9s" data-wow-duration="1s">
                <div class="footer-widgets">
                  <h3 class="widget-title">Contact Info</h3>
                  <div class="footer-address">
                    <ul>
                      <li>
                        <div class="icon-box">
                          <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14.4988 19.2499C14.7312 19.2512 14.9615 19.2063 15.1763 19.1176C15.3912 19.029 15.5862 18.8985 15.75 18.7336L18.1213 16.3624C18.2843 16.1984 18.3757 15.9767 18.3757 15.7455C18.3757 15.5143 18.2843 15.2926 18.1213 15.1286L14.6213 11.6286C14.4573 11.4657 14.2356 11.3742 14.0044 11.3742C13.7732 11.3742 13.5515 11.4657 13.3875 11.6286L11.9875 13.0199C11.0183 12.7614 10.1206 12.2857 9.36253 11.6286C8.70726 10.8694 8.23171 9.97213 7.97128 9.00363L9.36253 7.60363C9.5255 7.43969 9.61697 7.21792 9.61697 6.98675C9.61697 6.75559 9.5255 6.53382 9.36253 6.36988L5.86253 2.86988C5.69859 2.70691 5.47682 2.61544 5.24565 2.61544C5.01449 2.61544 4.79272 2.70691 4.62878 2.86988L2.26628 5.24988C2.10146 5.41372 1.97092 5.60874 1.88228 5.82357C1.79365 6.0384 1.74869 6.26874 1.75003 6.50113C1.82941 9.86314 3.17284 13.0718 5.51253 15.4874C7.92816 17.8271 11.1368 19.1705 14.4988 19.2499ZM5.25003 4.73363L7.51628 6.99988L6.38753 8.12863C6.28062 8.22877 6.20057 8.35415 6.1547 8.49327C6.10884 8.63239 6.09864 8.78079 6.12503 8.92488C6.45208 10.3865 7.1445 11.7412 8.13753 12.8624C9.25784 13.8567 10.6129 14.5493 12.075 14.8749C12.2169 14.9045 12.364 14.8985 12.503 14.8572C12.642 14.816 12.7685 14.7409 12.8713 14.6386L14 13.4836L16.2663 15.7499L14.5163 17.4999C11.6145 17.4252 8.84585 16.2671 6.75503 14.2536C4.73633 12.162 3.57494 9.3896 3.50003 6.48363L5.25003 4.73363ZM17.5 9.62488H19.25C19.2727 8.5845 19.0845 7.5503 18.6968 6.58461C18.3091 5.61892 17.7299 4.74172 16.994 4.00589C16.2582 3.27006 15.381 2.69082 14.4153 2.3031C13.4496 1.91538 12.4154 1.72719 11.375 1.74988V3.49988C12.1869 3.47179 12.996 3.61101 13.7518 3.90888C14.5076 4.20675 15.1941 4.65691 15.7685 5.23136C16.343 5.80582 16.7932 6.4923 17.091 7.24812C17.3889 8.00395 17.5281 8.81296 17.5 9.62488Z" />
                            <path d="M11.375 7C13.2125 7 14 7.7875 14 9.625H15.75C15.75 6.8075 14.1925 5.25 11.375 5.25V7Z" />
                          </svg>
                        </div>
                        <span class="text-box">(0271)724-811</span>
                      </li>
                      <li>
                        <div class="icon-box">
                          <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_424:102)">
                              <path d="M14.4235 10.479L19.8056 5.803V15.1016L14.4235 10.479ZM7.48825 11.2709L9.37825 12.9115C9.67487 13.1635 10.0616 13.3158 10.4843 13.3158H10.4991H10.4982H10.5105C10.934 13.3158 11.3207 13.1626 11.6209 12.9089L11.6182 12.9106L13.5082 11.27L19.2535 16.2041H1.74562L7.48825 11.2709ZM1.73775 4.69437H19.264L10.8456 12.0059C10.751 12.0807 10.6338 12.1214 10.5131 12.1214H10.5009H10.5017H10.4895C10.3684 12.1215 10.2508 12.0805 10.1561 12.005L10.157 12.0059L1.73775 4.69437ZM1.19437 5.80213L6.57562 10.4781L1.19437 15.0981V5.80213ZM20.0944 3.66625C19.8844 3.56125 19.6376 3.5 19.376 3.5H1.62662C1.37302 3.50006 1.12289 3.55907 0.896 3.67238L0.905625 3.668C0.634002 3.80197 0.405254 4.00923 0.245222 4.26636C0.0851906 4.5235 0.00025143 4.82026 0 5.12313L0 15.7736C0.000463228 16.2047 0.171896 16.6179 0.476683 16.9227C0.78147 17.2275 1.19472 17.3989 1.62575 17.3994H19.3734C19.8044 17.3989 20.2177 17.2275 20.5224 16.9227C20.8272 16.6179 20.9987 16.2047 20.9991 15.7736V5.12663V5.12313C20.9991 4.487 20.6325 3.93575 20.0987 3.67063L20.0891 3.66625H20.0944Z" />
                            </g>
                            <defs>
                              <clipPath id="clip0_424:102">
                                <rect width="21" height="21" fill="white" />
                              </clipPath>
                            </defs>
                          </svg>
                        </div>
                        <span class="text-box">redaksi@solopos.co.id</span>
                      </li>
                      <li>
                        <div class="icon-box">
                          <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.32987 4.94839C8.09889 3.17937 10.4982 2.18555 13 2.18555C15.5017 2.18555 17.901 3.17937 19.67 4.94839C21.4391 6.7174 22.4329 9.1167 22.4329 11.6185C22.4329 14.1202 21.4391 16.5195 19.67 18.2886L18.3841 19.5604C17.4362 20.4899 16.2066 21.6859 14.6943 23.1484C14.2398 23.5879 13.6322 23.8336 13 23.8336C12.3677 23.8336 11.7602 23.5879 11.3056 23.1484L7.52371 19.4694C7.04812 19.0025 6.65054 18.6092 6.32987 18.2886C5.45391 17.4126 4.75905 16.3728 4.28497 15.2283C3.8109 14.0839 3.56689 12.8572 3.56689 11.6185C3.56689 10.3797 3.8109 9.15308 4.28497 8.00862C4.75905 6.86416 5.45391 5.82429 6.32987 4.94839ZM18.5206 6.09672C17.0562 4.63255 15.07 3.8101 12.9992 3.8103C10.9283 3.8105 8.94238 4.63334 7.47821 6.0978C6.01404 7.56226 5.19159 9.54838 5.19179 11.6192C5.19199 13.6901 6.01483 15.676 7.47929 17.1402L9.08912 18.7306C10.2011 19.8171 11.3163 20.9004 12.4345 21.9806C12.586 22.1272 12.7886 22.2091 12.9994 22.2091C13.2103 22.2091 13.4128 22.1272 13.5644 21.9806L17.2412 18.4056C17.7504 17.9061 18.1761 17.4847 18.5195 17.1402C19.9837 15.676 20.8062 13.6902 20.8062 11.6196C20.8062 9.54892 19.9837 7.56308 18.5195 6.09889L18.5206 6.09672ZM13 8.6653C13.427 8.6653 13.8499 8.74942 14.2445 8.91286C14.6391 9.0763 14.9976 9.31585 15.2996 9.61784C15.6016 9.91983 15.8411 10.2783 16.0046 10.6729C16.168 11.0675 16.2521 11.4904 16.2521 11.9175C16.2521 12.3446 16.168 12.7674 16.0046 13.162C15.8411 13.5566 15.6016 13.9151 15.2996 14.2171C14.9976 14.5191 14.6391 14.7586 14.2445 14.9221C13.8499 15.0855 13.427 15.1696 13 15.1696C12.1479 15.1541 11.3359 14.8047 10.7388 14.1966C10.1417 13.5885 9.80714 12.7703 9.80714 11.918C9.80714 11.0658 10.1417 10.2476 10.7388 9.63946C11.3359 9.03136 12.1479 8.68194 13 8.66639V8.6653ZM13 10.2903C12.7863 10.2903 12.5747 10.3324 12.3773 10.4142C12.1798 10.4959 12.0005 10.6158 11.8494 10.7669C11.6983 10.918 11.5784 11.0974 11.4967 11.2948C11.4149 11.4922 11.3728 11.7038 11.3728 11.9175C11.3728 12.1312 11.4149 12.3427 11.4967 12.5402C11.5784 12.7376 11.6983 12.917 11.8494 13.0681C12.0005 13.2191 12.1798 13.339 12.3773 13.4208C12.5747 13.5025 12.7863 13.5446 13 13.5446C13.4314 13.5446 13.8451 13.3733 14.1502 13.0682C14.4552 12.7632 14.6266 12.3494 14.6266 11.918C14.6266 11.4866 14.4552 11.0729 14.1502 10.7678C13.8451 10.4628 13.4314 10.2914 13 10.2914V10.2903Z" />
                          </svg>
                        </div>
                        <span class="text-box">Jl. Adisucipto 190 Solo, 57145</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="footer3 footer-bottom">
            <div class="d-flex flex-wrap align-items-center justify-content-between">
              <a href="https://www.solopos.com/solorayakeren" class="footer-logo me-4"><img src="https://cdn.solopos.com/logo/logo-white.png" alt="Footer Logo" width="200" height="37"></a>
              <div class="copyright-text">&copy;<span id="currentYear"></span><a href="https://www.solopos.com/" class="link-text" rel="nofollow">SOLOPOS.</a>All Rights Reserved.</div>
            </div>
          </div>
        </div>
      </footer>
      <!--=====================================-->
      <!--=      Footer Section Area End      =-->
      <!--=====================================-->
    </div>
    <!-- Offcanvas Menu Start -->
    <div class="offcanvas-menu-wrap" id="offcanvas-wrap" data-position="right">
      <div class="offcanvas-header">
        <span class="header-text">Close</span>
        <button type="button" class="offcanvas-menu-btn menu-status-close offcanvas-close">
          <span class="menu-btn-icon">
							<span></span>
          <span></span>
          <span></span>
          </span>
        </button>
      </div>
      <div class="offcanvas-content">
        <div class="top-content">
          <div class="offcanvas-logo">
            <a href="#"><img src="https://cdn.solopos.com/logo/logo-white.png" alt="logo" width="200" height="37"></a>
          </div>
          {{--<p>Lorem ipsum dolor sit, consectetur iscing sed diam nonummy nibh euismo Lorem ipsum dolor sit,
            consectetur</p>--}}
        </div>
        <h2>SolorayaKeren</h2>
        <div class="sub-title">Event Details</div>
        <ul class="info">
          <li>Rabu, 23 Februari 2022</li>
          <li>2:00 PM – 3:30 PM</li>
          <li>Alila Solo,</li>
          <li>Jl. Slamet Riyadi No.562, Jajar, Kec. Laweyan, Kota Surakarta, Jawa Tengah 57144</li>
          <li>+62 271 6770888</li>
        </ul>
        <a href="{{ url('/solorayakeren/kontak') }}" class="text-btn">View map</a>
      </div>
      <div class="offcanvas-footer">
        <ul>
          <li>
            <a target="_blank" href="https://www.facebook.com/soloposcom">Facebook</a>
          </li>
          <li>
            <a target="_blank" href="https://twitter.com/soloposdotcom">Twitter</a>
          </li>
          <li>
            <a target="_blank" href="https://www.pinterest.com/solopos/">Pinterest</a>
          </li>
          <li>
            <a target="_blank" href="https://www.instagram.com/koransolopos/">Instagram</a>
          </li>
        </ul>
      </div>
    </div>
    <!-- Offcanvas Menu End -->
    <!-- Search Start -->
    <div id="template-search" class="template-search">
      <button type="button" class="close">×</button>
      <form class="search-form">
        <input type="search" value="" placeholder="Search" />
        <button type="submit" class="search-btn btn-ghost style-1">
          <i class="fas fa-search"></i>
        </button>
      </form>
    </div>
    <!-- Search End -->
  </div>
  <!-- Dependency Scripts -->
  <script src="https://cdn.solopos.com/tematik/assets/jquery/jquery.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/magnific-popup/js/magnific-popup.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/countdown/js/jquery.countdown.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/wow/wow.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/swiper/js/swiper.min.js"></script>
  <script src="https://cdn.solopos.com/tematik/assets/validator/validator.min.js"></script>
  <!-- Custom Script -->
  <script src="https://cdn.solopos.com/tematik/js/app.js"></script>
  <script src="https://www.solopos.com/js/custom.js"></script>
</body>

</html>