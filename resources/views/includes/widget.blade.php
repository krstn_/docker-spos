@if( date('Y-m-d H:i:s') >= '2022-03-28 09:00:00' && date('Y-m-d H:i:s') <= '2022-04-10 23:59:59')
    <!-- Widget EKSPEDISI KULINER SOLO-->
        <a href="https://www.solopos.com/tag/ekspedisi-kuliner-solo?utm_source=widget_solopos.com" title="EKSPEDISI KULINER SOLO" target="_blank" style="display: inline-block; width: 100%;">
            <img loading="lazy" src="https://cdn.solopos.com/banner/widget/widget-kuliner-solo.jpg" class="visible animated" width="100%" alt="EKSPEDISI KULINER SOLO">
        </a>
        <div class="energi-widget">
            <ul>
            @php $eu_loop = 1; @endphp
            @foreach ($widget as $item)
            @if($eu_loop <=10) 
            <li> 
                <a href="{{ url("/{$item['slug']}-{$item['id']}") }}?utm_source=widget_solopos.com" style="color: rgb(143 3 57);"> 
                    <img loading="lazy" src="{{ $item['featured_image']['thumbnail'] }}" style="object-fit: none; object-position: center; height: 60px; width: 80px;" alt="{{ $item['title'] }}"> 
                    {{ $item['title'] }}
                </a> 
                <div style="clear:both;"></div>
            </li>
            @endif
            @php $eu_loop++ @endphp
            @endforeach 
            </ul>                                   
        </div>
        <div class="mb-3"></div>
        <div class="energi-copy">
            <a href="https://www.solopos.com/tag/ekspedisi-kuliner-solo?utm_source=widget_solopos.com" title="EKSPEDISI KULINER SOLO" target="_blank">
                <img loading="lazy" src="https://cdn.solopos.com/banner/widget/widget-kuliner-solo.jpg" class="visible animated" width="100%" alt="EKSPEDISI KULINER SOLO">
            </a>
        </div>
    
    
          <style type="text/css">
            .energi-widget ul {
                list-style: none;
                margin: 0;
                padding: 0;
                max-height: 270px;
                overflow-y: scroll;
                overflow-x: hidden;        
            }
            .energi-widget ul li {
                list-style: none;
                /*margin-bottom: 10px;*/
                display: block;
                color: blue;
                font-weight: bold;
                font-family: arial;
                padding: 10px;
                line-height: 17px;
            }
            .energi-widget ul li:last-child {
                border: none;
            }
            .energi-widget ul li a {
                text-decoration: none;
                color: #1EBAC4;
                font-size: 12px;
            }
            .energi-widget ul li a:hover {
                text-decoration: none;
                color: #FFA500;     
            }
            .energi-widget ul li img {
                width: 80px;
                height: 60px;
                float: left;
                margin-right: 10px;
                vertical-align: center;
            }
            .energi-widget-title {
                height: 60px;
                background: #FFA500;
            }
            .energi-widget-title img {
                height: 50px;
                padding: 5px 20px;
                float: left;
            }
            .energi-copy {
            border-top: none;
            }     
          </style>
@endif