<!-- sidebar start -->
<div class="col-lg-4">
	<div class="sidebar">
		@include('includes.ads.desktop-sidebar-1')

		@include('includes.widget-popular-all')
		
		<style>
			.ads-image iframe {width: 300px !important;}
		</style>
		@include('includes.ads.desktop-sidebar-2')

		@include('includes.widget')

		@if( date('Y-m-d H:i:s') <= '2024-05-31 23:59:59')
		<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
		<script>
		window.googletag = window.googletag || {cmd: []};
		googletag.cmd.push(function() {
			googletag.defineSlot('/54058497/UKSW-MR-DESKTOP', [300, 250], 'div-gpt-ad-1634116572610-0').addService(googletag.pubads());
			googletag.pubads().enableSingleRequest();
			googletag.enableServices();
		});
		</script>
		<!-- /54058497/UKSW-MR-DESKTOP -->
		<div id='div-gpt-ad-1634116572610-0' style='min-width: 300px; min-height: 250px;'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1634116572610-0'); });
			</script>
		</div><br>
		@endif

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> News </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $bn_loop = 1; @endphp
		          @foreach($news as $bn) @if($bn_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($bn['title']) }}">
									<img loading="lazy" class="img-fluid" src="{{ $bn['images']['url_thumb'] }}" alt="{{ html_entity_decode($bn['title']) }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									{{-- @if($bn['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif --}}
									<a href="{{ url("/{$bn['slug']}-{$bn['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($bn['title']) }}">{{ html_entity_decode($bn['title']) }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($bn['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif @php $bn_loop++; @endphp @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		@include('includes.ads.desktop-sidebar-3')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Lifestyle </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $ls_loop = 1; @endphp
		          @foreach($lifestyle as $ls) @if($ls_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$ls['slug']}-{$ls['id']}") }}?utm_source=sidebar_desktop" title="{{ $ls['title'] }}">
									<img loading="lazy" class="img-fluid" src="{{ $ls['images']['url_thumb'] }}" alt="{{ html_entity_decode($ls['title']) }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									{{-- @if($ls['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif --}}
									<a href="{{ url("/{$ls['slug']}-{$ls['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($ls['title']) }}">{{ html_entity_decode($ls['title']) }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($ls['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif @php $ls_loop++; @endphp @endforeach
				</ul><!-- List post end -->
			</div>
		</div>

		@include('includes.ads.desktop-sidebar-4')

		<div class="sidebar-widget">
			<h2 class="block-title block-title-dark">
				<span class="title-angle-shap"> Kolom </span>
			</h2>
			<div class="list-post-block">
				<ul class="list-post">
		          @php $kl_loop = 1; @endphp
		          @foreach($kolom as $kl) @if($kl_loop <= 3)
					<li>
						<div class="post-block-style media">
							<div class="post-thumb">
								<a href="{{ url("/{$kl['slug']}-{$kl['id']}") }}?utm_source=sidebar_desktop" title="{{ $kl['title'] }}">
									<img loading="lazy" class="img-fluid" src="{{ $kl['images']['url_thumb'] }}" alt="{{ html_entity_decode($kl['title']) }}" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
								</a>
							</div><!-- Post thumb end -->

							<div class="post-content media-body">
								<h2 class="post-title">
									{{-- @if($kl['konten_premium'] == 'premium')
									<span class="espos-plus">+ PLUS</span>
									@endif --}}
									<a href="{{ url("/{$kl['slug']}-{$kl['id']}") }}?utm_source=sidebar_desktop" title="{{ html_entity_decode($kl['title']) }}">{{ html_entity_decode($kl['title']) }}</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($kl['date']) }}</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post block style end -->
					</li><!-- Li 1 end -->
					@endif @php $kl_loop++; @endphp @endforeach
				</ul><!-- List post end -->
			</div>
		</div>
		<div class="sidebar-widget ads-widget">
			<div class="ads-image">
				<!-- PubMatic ad tag (Javascript) : solopos.com_Desktop_300x600 | TADEX_Web-PT_Aksara_Solopos- | 300 x 600 Filmstrip -->
				<script type="text/javascript">
					var pubId=157566;
					var siteId=841452;
					var kadId=4434918;
					var kadwidth=300;
					var kadheight=600;
					var kadschain="SUPPLYCHAIN_GOES_HERE";
					var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
					var kadtype=1;
					var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
					var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
					var kadexpdir = '1,2,3,4,5';
					var kadbattr = '8,9,10,11,14';
					var kadifb = 'Dc';
					var kadpageurl= "%%PATTERN:url%%";
				</script>
				<script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script>
			</div>
		</div>

	</div>
</div><!-- Sidebar Col end -->
