<div class="sidebar-widget featured-tab post-tab"><!-- style="position: -webkit-sticky; position: sticky; top: 130px;" -->
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link animated active fadeIn" href="{{ url("/terpopuler") }}">
                <span class="tab-head">
                    <span class="tab-text-title">Terpopuler</span>
                </span>
            </a>
        </li>
    </ul>
    <div class="gap-50 d-none d-md-block"></div>
    <div class="row">
        <div class="col-12">
            <div class="tab-content">
                <div class="tab-pane active animated fadeInRight" id="post_tab_b">
                    <div class="list-post-block">
                        <ul class="list-post" id="populars">
                        </ul><!-- List post end -->
                    </div>
                </div><!-- Tab pane 2 end -->
            </div><!-- tab content -->
        </div>
    </div>
</div><!-- widget end -->
@push('custom-scripts')
<script>
    function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);

    var interval = seconds / 31536000;

    if (interval > 1) {
    return Math.floor(interval) + " tahun yang lalu";
    }
    interval = seconds / 2592000;
    if (interval > 1) {
    return Math.floor(interval) + " bulan yang lalu";
    }
    interval = seconds / 86400;
    if (interval > 1) {
    return Math.floor(interval) + " hari yang lalu";
    }
    interval = seconds / 3600;
    if (interval > 1) {
    return Math.floor(interval) + " jam yang lalu";
    }
    interval = seconds / 60;
    if (interval > 1) {
    return Math.floor(interval) + " menit yang lalu";
    }
    return Math.floor(seconds) + " detik yang lalu";
}

$(document).ready(function() {
        $.ajax({ //create an ajax request related post
        type: "GET",
        url: "https://tf.solopos.com/api/v1/stats/popular/all/", //72325
        dataType: "JSON",
        success: function(data) {
            // console.log(data.data);
            var populars = $("#populars");

            $.each(data.data, function(i, item) {
                if(i < 5) {
                    // populars.append("<li>"+ item['post_title'] + "</li>");

                    populars.append("<li><div class=\"post-block-style media\"><div class=\"post-thumb\"><a href=\"https://www.solopos.com/" + item['post_slug'] + "-" + item['post_id'] + "?utm_source=bacajuga_desktop\" title=\"" + item['post_title'] + "\"><img loading=\"lazy\" class=\"img-fluid\" src=\"" + item['post_thumb'] +"\" alt=\"" + item['post_title'] + "\" style=\"object-fit: cover; object-position: center; height: 85px; width: 85px;\"></a></div><div class=\"post-content media-body\"><h2 class=\"post-title\"><a href=\"https://www.solopos.com/" + item['post_slug'] + "-" + item['post_id'] + "?utm_source=bacajuga_desktop\" title=\"" + item['post_title'] + "\">" + item['post_title'] + "</a></h2><div class=\"post-meta mb-7\"><span class=\"post-date\"><i class=\"fa fa-clock-o\"></i> " + timeSince(new Date(item['post_date'])) + "</span></div></div></div></li>");
                }
            });
        }
    });
});
</script>
@endpush