@extends('layouts.app')
@section('content')
	<!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li>
							<a href="https://www.solopos.com/"><i class="fa fa-home"></i></a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li><a href="https://www.solopos.com/page/help">Pusat Bantuan</a></li>
					</ol>		
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-10 mx-auto">	
					<div class="post-media mb-20" style="max-height:400px;">
							<img src="https://www.solopos.com/images/background-espos-plus.jpg" alt="Solopos Digital Media" class="img-fluid" style="object-fit: cover; object-position: center; width: 811px; height: 400px;">
					</div>
					<div class="post-header-area" align="center">
						<h1 class="post-title title-lg">Pusat Bantuan</h1>
					</div>
          <div class="gap-30"></div>
          <h2 class="block-title">
              <span class="title-angle-shap"> Frequently Asked Questions Espos Plus </span>
          </h2>                    
          <div class="accordion" id="faqubahlaku">
              <div class="card">
                <div class="card-header" id="heading1">
                    <a href="#" class="faq-title" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                      <i class="fa fa-question-circle"></i> Apa itu Espos Plus?
                    </a>
                </div>

                <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#faqubahlaku">
                  <div class="card-body">
                      Espos Plus adalah artikel yang menyajikan informasi dari fakta dan peristiwa yang diolah serta dipaparkan dengan cara kreatif dan lebih visual. Artikel ditulis dengan bahasa yang lebih sederhana, lugas, tetapi lebih mendalam. Espos Plus dapat diakses dengan sistem berlangganan pada situs dalam jaringan (online).<br><br>

                      Konten Espos Plus di Solopos.com berbeda dengan free content Solopos.com. Espos Plus menyajikan informasi dengan insight dan perspektif yang berbeda dan lebih mendalam. Pembaca akan lebih nyaman membaca Espos Plus tanpa harus terganggu dengan iklan yang muncul.
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="heading2">
                  
                    <a href="#" class="faq-title" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                      <i class="fa fa-question-circle"></i> Bagaimana cara mengakses Espos Plus?
                    </a>
                  
                </div>
                <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#faqubahlaku">
                  <div class="card-body">
                      Espos Plus adalah konten yang bisa diakses melalui smartphone, tablet, laptop, maupun desktop melalui <a href="https://www.solopos.com/plus">https://www.solopos.com/plus</a>.
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="heading3">
                  
                    <a href="#" class="faq-title" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                      <i class="fa fa-question-circle"></i> Bagaimanacara berlangganan Espos Plus?
                    </a>
                  
                </div>
                <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#faqubahlaku">
                  <div class="card-body">
                      Untuk pembaca setia Solopos.com, Anda bisa login terlebih dahulu di <a href="https://id.solopos.com/login">https://id.solopos.com/login</a>. Setelah Anda login dan memiliki akun Solopos ID, Anda bisa memiliki paket langganan yang tersedia.<br><br>
              
                      Mulai 1 Januari 2022, pembaca bisa menikmati Espos Plus dengan memilih paket langganan yang beragam yaitu langganan durasi 7 hari, langganan durasi 1 bulan, langganan durasi 6 bulan, dan langganan durasi 1 tahun.<br><br>
                      
                      Selama 2022, Espos Plus menawarkan program khusus Espos Vaganza bagi pelanggan Espos Plus dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya.
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="heading4">
                  
                    <a href="#" class="faq-title" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
                      <i class="fa fa-question-circle"></i> Apa keunggulan berlangganan Espos Plus?
                    </a>
                  
                </div>

                <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#faqubahlaku">
                  <div class="card-body">
                      Dengan berlangganan Espos Plus, Anda mendapatkan beritayang lebih mendalam dan bebas dari iklan.Sebuah karya jurnalistik adalah sumber informasi sahih. Untuk mendapatkan informasi tersebut, butuh biaya tinggi.<br><br>

                      Dengan berlangganan Espos Plus, berarti Anda mendukung keberlangsungan informasi yang akan menjadi sumber kepercayaan diri ketika membuat keputusan. Anda juga bisa mengikuti program Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya.
                  </div>
                </div>
              </div>

              <div class="card">
                <div class="card-header" id="heading5">
                  
                    <a href="#" class="faq-title" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
                      <i class="fa fa-question-circle"></i> Apa itu Espos Vaganza?
                    </a>
                  
                </div>

                <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#faqubahlaku">
                  <div class="card-body">
                      Espos Vaganza adalah program khusus bagi pelanggan Espos Plus selama 2022. Program khusus ini menawarkan berbagai promo khusus untuk pelanggan Espos Plus seperti Espos Plus E-Money dengan hadiah e-money yang diundi tiap bulan, Espos Plus Emas dengan  hadiah emas batangan yang diundi tiap bulan, Super Espos Plus yang memberikan hadiah emas batangan untuk pemenang tiap bulan.<br><br>

                      Kemudian Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya khusus bagi pelanggan Espos Plus yang memilih paket langganan 1 tahun.<br><br>
                      
                      Lebih lengkap mengenai syarat dan ketentuan tentang promo Espos Vaganza bisa klik <a href="#" data-toggle="modal" data-target="#syarat-ketentuan-vaganza">klik di sini</a>.
                  </div>
                </div>
              </div> 


              <div class="card">
                <div class="card-header" id="heading6">
                    <a href="#" class="faq-title" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
                      <i class="fa fa-question-circle"></i> Siapa yang bisa ikut Espos Vaganza?
                    </a>
                  
                </div>

                <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#faqubahlaku">
                  <div class="card-body">
                      Pelanggan yang mendaftar dan berlangganan Espos Plus di periode 1 Januari 2022-31 Desember 2022 bisa mengikuti program Espos Vaganza sesuai syarat dan ketentuan yang berlaku.<br><br>

                      Promo Espos Vaganza dengan hadiah utama 1 unit mobil, sepeda motor, sepeda, emas batangan, televisi, dan hadiah menarik lainnya hanya berlaku untuk pelanggan yang memilih paket langganan 1 tahun.<br><br>
                      
                      Lebih lengkap mengenai syarat dan ketentuan tentang promo Espos Vaganza bisa <a href="#" data-toggle="modal" data-target="#syarat-ketentuan-vaganza">klik di sini</a>.
                  </div>
                </div>
              </div>

          </div>	<!--end faq -->	

					{{-- <div id="syarat-ketentuan" class="single-post">
            <div class="gap-30"></div>
            <h2 class="block-title">
                <span class="title-angle-shap"> Syarat & Ketentuan Espos Vaganza </span>
            </h2> 
            <div class="post-content-area">
              <p>                       
              <ol>
                  <li>Promo Espos Vaganza bagi pelanggan Espos Plus dilaksanakan selama 1 tahun mulai 1 Januari 2022 – 31 Desember 2022 dengan hadiah utama 1 unit mobil, motor, emas batangan, sepeda, dan televisi yang akan diundi setelah promo berakhir.</li>
                  <li>Promo Espos Vaganza hanya berlaku untuk pelanggan Espos Plus yang memilih paket langganan 1 tahun dengan periode pendaftaran 1 Januari 2022 sampai 31 Desember 2022.</li>
                  <li>Promo Super Espos Plus bagi pelanggan Espos Plus dilaksanakan selama 1 tahun mulai 1 Januari 2022-31 Desember 2022 dengan hadiah emas batangan yang akan diundi tiap bulan.</li>
                  <li>Promo Super Espos Plus hanya berlaku untuk pelanggan Espos Plus yang memilih paket berlangganan 6 bulan dan 1 tahun dengan periode pendaftaran di bulan tersebut.</li>
                  <li>Promo Espos Plus Emoney bagi pelanggan Espos Plus dilaksanakan tiap bulan selama tahun 2022 dengan hadiah emoney yang akan diundi tiap bulan.</li>
                  <li>Promo Espos Plus Emoney hanya berlaku bagi pelanggan Espos Plus yang memilih paket langganan 1 bulan, 6 bulan, dan 1 tahun dengan periode pendaftaran tanggal 1-10 tiap bulannya.</li>
                  <li>Promo Espos Plus Emas bagi pelanggan Espos Plus dilaksanakan tiap bulan selama 2022 dengan hadiah emas yang akan diundi tiap bulan.</li>
                  <li>Promo Espos Plus Emas hanya berlaku bagi pelanggan Espos Plus yang berlangganan 6 bulan dan 1 tahun dengan periode pendaftaran tanggal 15-25 tiap bulannya.</li>
                  <li>Promo Espos Plus Survei bagi pelanggan Espos Plus dilaksanakan selama 2022 dengan hadiah alat elektronik yang akan diumumkan tiap bulan.</li>
                  <li>Promo Espos Plus Survei berlaku bagi semua pelanggan Espos Plus yang mengisi survei secara lengkap dan mengirimkan ke Solopos.</li>
                  <li>Pengundian dan pengumuman pemenang Super Espos Plus, Espos Plus Emoney, dan Espos Plus Emas, serta pemenang Espos Plus Survei akan dilakukan dalam acara Espos Vaganza Live yang disiarkan secara langsung di Facebook Solopos.com tiap awal bulan.</li>
                  <li>Pengundian dan pengumuman pemenang Espos Vaganza akan dilakukan dalam acara Puncak Espos Vaganza yang disiarkan secara langsung di Youtube SoloposTV, Facebook Solopos.com, dan Instagram @koransolopos.</li>
                  <li>Program promo ini berlaku bagi masyarakat umum yang menjadi pelanggan Espos Plus dan tidak berlaku bagi karyawan dan keluarga karyawan Solopos Media Group.</li>
                  <li>Pajak hadiah ditanggung pemenang.</li>
                  <li>Pengundian dan pengumuman pemenang dilakukan secara transparan dan hasilnya tidak dapat diganggu gugat.</li>	
              </ol>
              </p> 
            </div>												
					</div> --}}
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#syarat-ketentuan-vaganza">
            Syarat & Ketentuan Espos Vaganza
          </button>

				</div>
			</div>
		</div>
	</section><!-- category-layout end -->


  <div class="modal fade" id="syarat-ketentuan-vaganza" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Syarat & Ketentuan Espos Vaganza</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="single-post">
            <div class="post-content-area">
              <p>                       
              <ol style="padding-left:25px;">
                  <li>Promo Espos Vaganza bagi pelanggan Espos Plus dilaksanakan selama satu tahun mulai 1 Januari 2022-31 Desember 2022.</li>
                  <li>Undian Tahunan Espos Vaganza dengan hadiah utama 1 unit mobil Daihatsu Rocky, sepeda motor Yamaha NMax, emas batangan, sepeda, dan beragam alat elektronik hanya berlaku bagi <strong>pelanggan Espos Plus yang memilih paket 1 tahun</strong> yang mendaftar pada periode 1 Januari 2022-31 Desember 2022.</li>
                  <li>Undian Tahunan Espos Vaganza dan pengumuman pemenang akan dilakukan dalam Puncak Espos Vaganza yang disiarkan secara langsung di Youtube SoloposTV, Facebook Solopos.com, dan Instagram <a href="https://instagram.com/koransolopos" target="_blank"><i>@koransolopos</i></a> setelah periode promosi berakhir.</li>
                  <li>Promo Espos Vaganza juga menghadirkan Undian Bulanan dengan hadiah e-money dan emas batangan.</li>
                  <li>Undian Bulanan Espos Vaganza hanya berlaku untuk <strong>pelanggan Espos Plus yang memilih paket 1 bulan, 6 bulan, dan 1 tahun</strong> pada periode pendaftaran di bulan tersebut.</li>
                  <li>Hadiah Undian Bulanan Espos Vaganza yaitu satu e-money Rp30.000 untuk pelanggan 1 bulan, satu e-money Rp150.000 untuk pelanggan 6 bulan, dan satu e-money Rp240.000, satu emas 0,2 gram, satu emas 0,5 gram untuk pelanggan 1 tahun.</li>
                  <li>Pelanggan 1 tahun yang sudah menang dalam Undian Bulanan Espos Vaganza tetap diikutkan dalam Undian Tahunan Espos Vaganza.</li>
                  <li>Undian Bulanan Espos Vaganza dan pengumuman pemenang disiarkan secara langsung melalui  Instagram <a href="https://instagram.com/koransolopos" target="_blank"><i>@koransolopos</i></a> pada awal bulan berikutnya.</li>
                  <li>Promo Espos Vaganza terbuka untuk masyarakat umum yang menjadi pelanggan Espos Plus dan tidak berlaku bagi karyawan dan keluarga karyawan Solopos Media Group.</li>
                  <li>Pajak hadiah ditanggung pemenang.</li>
                  <li>Pengundian dan pengumuman pemenang dilakukan secara transparan dan hasilnya tidak dapat diganggu gugat.</li>
              </ol>
              </p> 
            </div>												
          </div>
        </div>
      </div>
    </div>
  </div>

  <style type="text/css">
      .faq-title {font-weight: 700; font-size: 14px; color: #272c76;}
      .accordion li {list-style: decimal; padding-left: 10px;}
      .accordion i {padding-right: 5px; color: #28a745; font-size: 18px;}
      .card .card-header {padding-left: 10px 5px !important;}
      .accordion {margin-bottom: 60px;}
  </style>

@endsection