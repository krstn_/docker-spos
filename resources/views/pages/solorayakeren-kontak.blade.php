@extends('layouts.app-solorayakeren')
@section('content')	      
      <!--=====================================-->
      <!--=    Inne Page Banner Area Start    =-->
      <!--=====================================-->
      <!-- start inner-banner -->
      <section data-bg-image="https://cdn.solopos.com/tematik/image/banner/inner-banner10.jpg" style="padding-top: 70px;padding-bottom: 50px;">
      </section>      
      <!--=====================================-->
      <!--=     Inne Page Banner Area End     =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=    Contact Section Area Start     =-->
      <!--=====================================-->
      <section class="contact-wrap-layout2">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="contact-box-layout2">
                <div class="address-box color-one">
                  <div class="icon-box">
                    <svg width="55" height="63" viewBox="0 0 55 63" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M38.8756 27.2136C38.8756 30.3066 37.6469 33.273 35.4598 35.4602C33.2727 37.6473 30.3063 38.876 27.2132 38.876C24.1201 38.876 21.1538 37.6473 18.9666 35.4602C16.7795 33.273 15.5508 30.3066 15.5508 27.2136C15.5508 24.1205 16.7795 21.1541 18.9666 18.967C21.1538 16.7799 24.1201 15.5511 27.2132 15.5511C30.3063 15.5511 33.2727 16.7799 35.4598 18.967C37.6469 21.1541 38.8756 24.1205 38.8756 27.2136ZM34.9882 27.2136C34.9882 25.1515 34.169 23.1739 32.7109 21.7159C31.2528 20.2578 29.2753 19.4386 27.2132 19.4386C25.1512 19.4386 23.1736 20.2578 21.7155 21.7159C20.2574 23.1739 19.4383 25.1515 19.4383 27.2136C19.4383 29.2756 20.2574 31.2532 21.7155 32.7113C23.1736 34.1694 25.1512 34.9885 27.2132 34.9885C29.2753 34.9885 31.2528 34.1694 32.7109 32.7113C34.169 31.2532 34.9882 29.2756 34.9882 27.2136Z" />
                      <path d="M46.457 46.4761C51.5609 41.3695 54.428 34.4452 54.428 27.2253C54.428 20.0054 51.5609 13.081 46.457 7.97451C43.9306 5.44639 40.9308 3.44088 37.629 2.07258C34.3272 0.704273 30.7881 0 27.214 0C23.6399 0 20.1009 0.704273 16.799 2.07258C13.4972 3.44088 10.4974 5.44639 7.97101 7.97451C2.86712 13.081 0 20.0054 0 27.2253C0 34.4452 2.86712 41.3695 7.97101 46.4761L13.8839 52.3034L21.826 60.02L22.343 60.4787C25.3558 62.9201 29.7681 62.7646 32.6059 60.02L42.0719 50.8067L46.457 46.4761ZM10.7117 10.7152C12.8785 8.54744 15.4511 6.82785 18.2826 5.65462C21.1141 4.4814 24.1491 3.87754 27.214 3.87754C30.279 3.87754 33.3139 4.4814 36.1454 5.65462C38.9769 6.82785 41.5496 8.54744 43.7163 10.7152C47.972 14.9737 50.4189 20.7111 50.5464 26.7302C50.6738 32.7493 48.4721 38.5852 44.4005 43.0201L43.7163 43.7354L38.581 48.8047L29.9119 57.2327L29.5465 57.5437C28.8744 58.0502 28.0556 58.3241 27.214 58.3241C26.3724 58.3241 25.5537 58.0502 24.8815 57.5437L24.52 57.2327L12.9353 45.9396L10.7117 43.7354L10.0275 43.024C5.95593 38.5891 3.75419 32.7532 3.88167 26.7341C4.00915 20.715 6.456 14.9776 10.7117 10.7191V10.7152Z" />
                    </svg>
                  </div>
                  <h3 class="title">Venue Info</h3>
                  <ul class="address-details">
                    <li>Alila Hotel, Jl. Slamet Riyadi No.562</li>
                    <li>Jajar, Laweyan, Surakarta, Jateng 57144</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4 wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="contact-box-layout2">
                <div class="address-box color-two">
                  <div class="icon-box">
                    <svg width="72" height="54" viewBox="0 0 72 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M67.5 0H4.5C3.30653 0 2.16193 0.474106 1.31802 1.31802C0.474106 2.16193 0 3.30653 0 4.5V49.5C0 50.6935 0.474106 51.8381 1.31802 52.682C2.16193 53.5259 3.30653 54 4.5 54H67.5C68.6935 54 69.8381 53.5259 70.682 52.682C71.5259 51.8381 72 50.6935 72 49.5V4.5C72 3.30653 71.5259 2.16193 70.682 1.31802C69.8381 0.474106 68.6935 0 67.5 0ZM64.035 49.5H8.235L23.985 33.21L20.745 30.0825L4.5 46.89V7.92L32.4675 35.7525C33.3106 36.5906 34.4512 37.0611 35.64 37.0611C36.8288 37.0611 37.9694 36.5906 38.8125 35.7525L67.5 7.2225V46.5975L50.94 30.0375L47.7675 33.21L64.035 49.5ZM7.4475 4.5H63.855L35.64 32.5575L7.4475 4.5Z" />
                    </svg>
                  </div>
                  <h3 class="title">Email Address</h3>
                  <ul class="address-details">
                    <li>redaksi@solopos.co.id</li>
                    <li>admin@solopos.com</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4 wow fadeInLeft animated" data-wow-delay="0.7s" data-wow-duration="1s">
              <div class="contact-box-layout2">
                <div class="address-box color-three">
                  <div class="icon-box">
                    <svg width="51" height="51" viewBox="0 0 51 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M28.05 15.3001C30.0789 15.3001 32.0247 16.1061 33.4594 17.5407C34.894 18.9754 35.7 20.9212 35.7 22.9501C35.7 23.6264 35.9687 24.275 36.4469 24.7532C36.9251 25.2314 37.5737 25.5001 38.25 25.5001C38.9263 25.5001 39.5749 25.2314 40.0531 24.7532C40.5313 24.275 40.8 23.6264 40.8 22.9501C40.8 19.5686 39.4567 16.3255 37.0656 13.9345C34.6745 11.5434 31.4315 10.2001 28.05 10.2001C27.3737 10.2001 26.7251 10.4687 26.2469 10.947C25.7687 11.4252 25.5 12.0738 25.5 12.7501C25.5 13.4264 25.7687 14.075 26.2469 14.5532C26.7251 15.0314 27.3737 15.3001 28.05 15.3001Z" />
                      <path d="M28.05 5.1C32.7841 5.1 37.3243 6.98062 40.6718 10.3281C44.0194 13.6757 45.9 18.2159 45.9 22.95C45.9 23.6263 46.1686 24.2749 46.6469 24.7531C47.1251 25.2313 47.7737 25.5 48.45 25.5C49.1263 25.5 49.7749 25.2313 50.2531 24.7531C50.7313 24.2749 51 23.6263 51 22.95C51 16.8633 48.5821 11.0259 44.2781 6.7219C39.9741 2.41794 34.1367 0 28.05 0C27.3737 0 26.7251 0.26866 26.2469 0.746877C25.7687 1.2251 25.5 1.8737 25.5 2.55C25.5 3.2263 25.7687 3.8749 26.2469 4.35312C26.7251 4.83134 27.3737 5.1 28.05 5.1ZM50.3625 35.4705C50.2222 35.0611 49.9801 34.6942 49.6589 34.4042C49.3377 34.1143 48.948 33.9108 48.5265 33.813L33.2265 30.3195C32.8112 30.2253 32.3789 30.2367 31.9691 30.3525C31.5593 30.4683 31.1851 30.6849 30.8805 30.9825C30.5235 31.314 30.498 31.3395 28.8405 34.5015C23.3407 31.9672 18.9337 27.5422 16.422 22.032C19.6605 20.4 19.686 20.4 20.0175 20.0175C20.3151 19.7129 20.5317 19.3387 20.6475 18.9289C20.7633 18.5191 20.7747 18.0868 20.6805 17.6715L17.187 2.55C17.0892 2.12847 16.8857 1.73877 16.5958 1.41757C16.3058 1.09638 15.9389 0.854256 15.5295 0.714C14.934 0.501303 14.319 0.347566 13.6935 0.255C13.049 0.105558 12.3913 0.0201433 11.73 0C8.61901 0 5.63544 1.23584 3.43564 3.43564C1.23584 5.63544 0 8.61901 0 11.73C0.0134944 22.1409 4.15519 32.1215 11.5168 39.4832C18.8784 46.8448 28.8591 50.9865 39.27 51C40.8104 51 42.3357 50.6966 43.7589 50.1071C45.182 49.5176 46.4751 48.6536 47.5644 47.5644C48.6536 46.4751 49.5176 45.182 50.1071 43.7589C50.6966 42.3357 51 40.8104 51 39.27C51.0008 38.6209 50.9496 37.9729 50.847 37.332C50.7398 36.6986 50.5777 36.0758 50.3625 35.4705ZM39.27 45.9C30.2096 45.8932 21.5223 42.291 15.1156 35.8844C8.70896 29.4777 5.10675 20.7904 5.1 11.73C5.10672 9.97368 5.80739 8.29121 7.0493 7.0493C8.29121 5.80739 9.97368 5.10672 11.73 5.1H12.5715L15.3 16.932L13.923 17.646C11.73 18.7935 9.996 19.7115 10.914 21.7005C12.4089 25.9324 14.8277 29.7782 17.9947 32.9584C21.1617 36.1387 24.9973 38.5735 29.223 40.086C31.365 40.953 32.2065 39.3465 33.354 37.128L34.0935 35.7255L45.9 38.4285V39.27C45.8933 41.0263 45.1926 42.7088 43.9507 43.9507C42.7088 45.1926 41.0263 45.8933 39.27 45.9Z" />
                    </svg>
                  </div>
                  <h3 class="title">Contact No</h3>
                  <ul class="address-details">
                    <li>(+62) 271 724-811</li>
                    <li>(+62) 271 724-833</li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-lg-5 wow fadeInLeft animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="contact-box-layout2">
                <div class="content-box">
                  <div class="sub-title">Contact with us</div>
                  <h2 class="title">Write a Message to Experts</h2>
                  <p class="description">Lorem ipsum is simply free text available in the as market dolor sit amet
                    consectetur notted
                    dummy adipisicing elit sed do.</p>
                  <ul class="social">
                    <li>
                      <a target="_blank" href="https://www.facebook.com/soloposcom" class="facebook"><i
                                        class="fab fa-facebook-f"></i></a>
                    </li>
                    <li>
                      <a target="_blank" href="https://twitter.com/soloposdotcom" class="twitter"><i
                                        class="fab fa-twitter"></i></a>
                    </li>
                    <li>
                      <a target="_blank" href="https://www.linkedin.com/soloposcom" class="linkedin"><i
                                        class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                      <a target="_blank" href="https://www.instagram.com/koransolopos" class="instagram"><i
                                        class="fab fa-instagram"></i></a>
                    </li>
                    <li>
                      <a target="_blank" href="https://www.pinterest.com/solopos/" class="pinterest"><i
                                        class="fab fa-pinterest-p"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-lg-7 wow fadeInLeft animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="contact-box-layout2">
                <div class="contact-form">
                  <form id="contact-form">
                    <div class="row">
                      <div class="col-md-6 form-group">
                        <input type="text" placeholder="Your name" class="form-control" name="name" data-error="Name field is required" required>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="col-md-6 form-group">
                        <input type="email" placeholder="Email address" class="form-control" name="email" data-error="Email field is required" required>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="col-md-6 form-group">
                        <input type="text" placeholder="Phone number" class="form-control" name="phone" data-error="Email field is required" required>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="col-md-6 form-group">
                        <input type="text" placeholder="Subject" class="form-control" name="subject" data-error="Subject field is required" required>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="col-12 form-group">
                        <textarea placeholder="Write a message" class="textarea form-control" name="message" rows="4" cols="20" data-error="Message field is required" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="col-12 form-group mb-0">
                        <button type="submit" class="btn-fill style-one">Send Message</button>
                      </div>
                    </div>
                    <div class="form-response"></div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=     Contact Section Area End      =-->
      <!--=====================================-->
@endsection       