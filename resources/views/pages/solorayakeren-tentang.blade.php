@extends('layouts.app-solorayakeren')
@section('content')	
      <!--=====================================-->
      <!--=    Inne Page Banner Area Start    =-->
      <!--=====================================-->
      <!-- start inner-banner -->
      <section data-bg-image="https://cdn.solopos.com/tematik/image/banner/inner-banner10.jpg" style="padding-top: 70px;padding-bottom: 50px;">
      </section>
      <!--=====================================-->
      <!--=     Inne Page Banner Area End     =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=   Blog Grid Section Area Start    =-->
      <!--=====================================-->
      <section class="blog-list-wrap">
        <div class="container">
          <div class="row">
            <div class="col-lg-8">
              <div class="section-heading style-two">
                <div class="sub-title">TENTANG</div>
                <h2 class="title">SOLORAYA <span>KEREN</span></h2>
              </div>
              <div class="figure-box wow fadeInUp animated" data-wow-delay="0.2s" data-wow-duration="1s">
                <img src="https://cdn.solopos.com/tematik/image/tentang-soloraya-keren.jpg" alt="Tentang Soloraya Keren">
              </div>              
            </div>
            @include('includes.solorayakeren.sidebar')
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=    Blog Grid Section Area End     =-->
      <!--=====================================-->
@endsection 