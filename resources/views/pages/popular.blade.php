@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
	<div class="gap-20"></div>
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-9 mx-auto">
					<div  align="center">
						@include('includes.ads.leaderboard-1')
					</div>

					<div class="post-header-area mt-3" align="center">
						<h1 class="post-title title-md">{{ $header['title'] }}</h1>
					</div>
					<div class="gap-30"></div>
					<div class="row ts-gutter-20 align-items-center loadmore-frame">
						@php
	                        $loop_no = 1;
	                    @endphp
                        @foreach ($popular as $posts)
						@php
						$thumb = $posts['post_thumb'];//$posts['images']['url_thumb'] ?? 'https://www.solopos.com/images/no-thumb.jpg';
            			// $medium = $posts['images']['medium'] ?? 'https://www.solopos.com/images/no-medium.jpg';
						$title = html_entity_decode($posts['post_title']);
						@endphp

                            <div class="col-12 mb-10 content-box">
                                <div class="post-block-style">
                                    <div class="row">
										<div class="col-md-1">
										<h1 class="align-middle">{{ $loop_no }}</h1>
										</div>
                                        <div class="col-md-3">
                                            <div class="post-thumb post-list_feed">
                                                <img src="{{ $thumb }}" alt="{{ $title }}" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
                                                <a class="post-cat-box {{ $posts['post_category'] }}" href="">{{ $posts['post_category'] }}</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8 pl-0">
                                            <div class="post-content">
												@if($loop_no==1)
												<h1 class="post-title title-md">
													<a href="{{ url("/{$posts['post_slug']}-{$posts['post_id']}") }}?utm_source=terpopuler_desktop" title="{{ $title }}">{{ $title }}</a>
												</h1>
												@else
												<h2 class="post-title title-md">
													<a href="{{ url("/{$posts['post_slug']}-{$posts['post_id']}") }}?utm_source=terpopuler_desktop" title="{{ $title }}">{{ $title }}</a>
												</h2>
												@endif
                                                <div class="post-meta mb-7">
												{{-- <span class="post-author"><a href="#"><i class="fa fa-user"></i> {{ $posts['author'] }} </a></span> --}}
												<span class="post-date"><i class="fa fa-clock-o"></i> {{ Carbon\Carbon::parse($posts['post_date'])->translatedFormat('l, j F Y H:i') }} WIB</span>
                                                </div>
                                                {{-- <p>@if($posts['summary']) {!! $posts['summary'] !!} @endif</p> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							@php $loop_no++; @endphp
						@endforeach
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Lihat Berita Lainnya</a>
				            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>
					<div align="center">
						<!-- PubMatic ad tag (Javascript) : solopos.com_desktop_728x90 | TADEX_Web-PT_Aksara_Solopos- | 728 x 90 Leaderboard -->
						<script type="text/javascript">
							var pubId=157566;
							var siteId=841452;
							var kadId=3732072;
							var kadwidth=728;
							var kadheight=90;
							var kadschain="SUPPLYCHAIN_GOES_HERE";
							var kadUsPrivacy=""; <!-- Insert user CCPA consent string here for CCPA compliant inventory -->
							var kadtype=1;
							var kadGdpr="0"; <!-- set to 1 if inventory is GDPR compliant -->
							var kadGdprConsent=""; <!-- Insert user GDPR consent string here for GDPR compliant inventory -->
							var kadexpdir = '1,2,3,4,5';
							var kadbattr = '8,9,10,11,14';
							var kadifb = 'Dc';
							var kadpageurl= "%%PATTERN:url%%";
						</script>
						<script type="text/javascript" src="https://ads.pubmatic.com/AdServer/js/showad.js"></script>
					</div>
				</div><!-- col-lg-12 -->

				<!-- sidebar start -->
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->

	<style>
		@import url('https://fonts.googleapis.com/css2?family=Orbitron:wght@600&display=swap');
	</style>

@endsection
