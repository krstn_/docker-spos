<html>
<head>
    <title>Ads Inventory Solopos.com Homepage</title>
    <meta content="Ads Inventory Solopos.com Homepage" itemprop="headline" />
    <meta name="title" content="Ads Inventory Solopos.com Homepage" />
    <meta name="description" content="Ads Inventory Solopos.com Homepage" itemprop="description" />
    <meta name="keywords" content="Ads Inventory Solopos.com Homepage" itemprop="keywords" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1200, initial-scale=1">
    <meta name="copyright" content="Solopos Media Group" /> 
    <meta name="googlebot-news" content="index,follow" />    
    <meta name="googlebot" content="index,follow,all" />
    <meta name="Bingbot" content="index,follow">
    <meta name="robots" content="index,follow,max-image-preview:large" />
    <meta name="google-site-verification" content="AA4UjbZywyFmhoSKLELl4RA451drENKllt5Sbq9uINw" />
    <meta name="yandex-verification" content="7966bb082003f8ae" />
    <meta name="msvalidate.01" content="F2320220951CEFB78E7527ECC232031C" />
    <meta name='dailymotion-domain-verification' content='dm0w2nbrkrxkno2q2' />
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="showus-verification" content="pub-2627" />

	<!--Favicon-->
	<link rel="shortcut icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">

    <!-- Stylesheet -->
	<link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/iconfonts.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />
	<link rel="stylesheet" href="{{ url('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ url('css/animate.css') }}">
	<link rel="stylesheet" href="{{ url('css/style.css') }}?v={{time()}}">
	<link rel="stylesheet" href="{{ url('css/responsive.css') }}">
	<link rel="stylesheet" href="{{ url('css/colorbox.css') }}">
</head>

<div id="mega-billboard-container" class="smooth" data-height="400px">
    <div id="div-small" class="smooth">
        <a href="" data-toggle="modal" data-target="#ModalWraptTop">
            <img src="{{ url('images/iklan-img/desktop-wrapttop.png') }}">
        </a>
    </div>
</div>
<div id="skinad-left">            
    <div id="left-lk">
        <a href="" data-toggle="modal" data-target="#ModalWraptSide">
            <img src="{{ url('images/iklan-img/desktop-wraptleft.png') }}">
        </a>
        
    </div>
</div>

<div id="skinad-right">
    <div id="right-lk">
        <a href="" data-toggle="modal" data-target="#ModalWraptSide">
            <img src="{{ url('images/iklan-img/desktop-wraptright.png') }}">
        </a>
    </div>
</div>

<!-- Header start -->
<header id="header" class="header">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-3 col-sm-12">
                <div class="header-nav">
                    <a href="{{ url('/espospedia') }}">Espospedia</a> | <a href="{{ url('/cekfakta') }}">Cek Fakta</a>
                    
                    <div class="top-social">
                        <ul class="social list-unstyled" style="padding:0;">
                            <li><a href="https://www.facebook.com/soloposcom" target="_blank" title="Facebook FP"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.twitter.com/soloposdotcom" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/koransolopos" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="mailto:redaksi@solopos.co.id" target="_blank" title="Emal"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>
                                        
                </div>
            </div><!-- logo col end -->		

            <div class="col-md-6 col-sm-12">
                <div class="logo">
                    <a href="{{ url('/mediakit') }}"> 
                      <img class="logo-image" src="{{ url('images/logo.png') }}" style="width:300px;" alt="Solopos.com">
                    </a>
                </div>
            </div><!-- logo col end -->
            <div class="col-md-3 col-sm-12">
                <div class="header-nav" style="text-align: right;">
                    <div style="height: 20px;display:inline;float: right; margin-bottom:10px;">
                        <a href="{{ url('/premium') }}">
                            <img class="logo-image" src="{{ url('images/premium.png') }}" height="23" alt="Espos Premium">
                        </a> 
                    </div>
                    <div style="clear: right;font-weight:500;font-size:13px;">
                    @if(Cookie::get('is_login') == 'true')
                    <a href="https://id.solopos.com/profile" target="_blank">PROFIL</a> | <a href="https://id.solopos.com" target="_blank">LOGOUT</a>
                    @else
                    <a href="https://id.solopos.com/login" target="_blank">LOGIN</a> | <a href="https://id.solopos.com/register" target="_blank">DAFTAR</a>
                    @endif
                    </div>
                </div>
            </div><!-- logo col end -->

        </div><!-- Row end -->
    </div><!-- Logo and banner area end -->
</header><!--/ Header end -->

<div class="main-nav clearfix is-ts-sticky">
    <div class="container">
        <div class="row justify-content-between">
            <nav class="navbar navbar-expand-lg col-lg-12">
                <div class="site-nav-inner float-left">
                       <!-- End of Navbar toggler -->
                       <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="sticky">
                                                   
                                {{-- <img src="/images/logo-.png" alt="Logo"> --}}
                                
                                <img src="{{ url('/images/logo.png') }}" alt="Logo">
                                
                            </li>							
                            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='news'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">News<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-submenu">
                                        <a href="{{ url('news/pendidikan') }}" class="menu-dropdown" data-toggle="dropdown">Pendidikan</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('/news/uns') }}">UNS</a></li>
                                            <li><a href="{{ url('uksw') }}">UKSW</a></li>
                                        </ul>
                                    </li>										
                                    <li><a href="{{ url('/news/nasional') }}">Nasional</a></li>
                                    <li><a href="{{ url('/news/internasional') }}">Internasional</a></li>
                                </ul>
                            </li><!-- Features menu end -->									
                            <li><a href="{{ url('/bisnis') }}">Ekbis</a></li>
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='soloraya'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Soloraya<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/soloraya/solo') }}">Solo</a></li>
                                    <li><a href="{{ url('/soloraya/sukoharjo') }}">Sukoharjo</a></li>
                                    <li><a href="{{ url('/soloraya/karanganyar') }}">Karanganyar</a></li>
                                    <li><a href="{{ url('/soloraya/sragen') }}">Sragen</a></li>
                                    <li><a href="{{ url('/soloraya/wonogiri') }}">Wonogiri</a></li>
                                    <li><a href="{{ url('/soloraya/klaten') }}">Klaten</a></li>
                                    <li><a href="{{ url('/soloraya/boyolali') }}">Boyolali</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='sport'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Sport<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/sport/bola') }}">Bola</a></li>
                                    <li><a href="{{ url('/sport/arena') }}">Arena</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='lifestyle'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Lifestyle<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ ('/lifestyle/leisure') }}">Leisure</a></li>
                                    <li><a href="{{ ('/lifestyle/viral') }}">Viral</a></li>
                                    <li><a href="{{ ('/lifestyle/anak') }}">Anak</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Regional<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-submenu">
                                        <a href="#" onclick="window.location.href='jateng'" class="menu-dropdown" data-toggle="dropdown">Jateng</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('/jateng/semarang') }}">Semarang</a></li>
                                            <li><a href="{{ url('/jateng/magelang') }}">Magelang</a></li>
                                            <li><a href="{{ url('/jateng/kudus') }}">Kudus</a></li>
                                            <li><a href="{{ url('/jateng/grobogan') }}">Grobogan</a></li>
                                            <li><a href="{{ url('/jateng/pemalang') }}">Pemalang</a></li>
                                            <li><a href="{{ url('/jateng/salatiga') }}">Salatiga</a></li>
                                            <li><a href="{{ url('/jateng/blora') }}">blora</a></li>
                                            <li><a href="{{ url('/jateng/pati') }}">Pati</a></li>
                                            <li><a href="{{ url('/jateng/banyumas') }}">Banyumas</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" onclick="window.location.href='jogja'" class="menu-dropdown" data-toggle="dropdown">Jogja</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('jogja/bantul') }}">Bantul</a></li>
                                            <li><a href="{{ url('jogja/gunungkidul') }}">Gunung Kidul</a></li>
                                            <li><a href="{{ url('jogja/kota-jogja') }}">Kota Jogja</a></li>
                                            <li><a href="{{ url('jogja/kulonprogo') }}">Kulon Progo</a></li>
                                            <li><a href="{{ url('jogja/sleman') }}">Sleman</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('/jatim') }}">Jatim</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li><a href="{{ url('/otomotif') }}">Otomotif</a></li>
                            <li><a href="{{ url('/teknologi') }}">Teknologi</a></li>
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='entertainment'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Entertainment<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/entertainment/artis') }}">Artis</a></li>
                                    <li><a href="{{ url('/entertainment/hiburan') }}">Hiburan</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li><a href="{{ url('/rsjihsolo') }}">Bugar</a></li>
                            <li><a href="{{ url('/arsip') }}">Indeks</a></li>
                            							
                        </ul><!--/ Nav ul end -->
                    </div><!--/ Collapse end -->
                </div><!-- Site Navbar inner end -->
                <div class="text-right" style="min-width: 65px; ">
                    <ul class="nav navbar-nav" style="float: left;">				
                        <li class="dropdown mega-dropdown">
                            <a style="font-size:18px;" href="#" class="dropdown-toggle menu-dropdown" data-toggle="dropdown"><i class="icon icon-menu"></i></a>
                            <ul class="dropdown-menu" role="menu">
                            <!-- responsive dropdown end -->
                            <div class="ts-footer">
                                <div class="container">
                                    <div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
                                        <div class="col-lg-7 col-md-7">
                                            <div class="footer-widget">
                                                <div class="footer-logo">
                                                    <img src="{{ url('/images/logo.png') }}" alt="Footer Logo">
                                                </div>
                                                <div class="widget-content">
                                                    <ul class="ts-footer-nav">
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/kontak">KONTAK</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/copyright">COPYRIGHT</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="writing-contest">WRITING CONTEST</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/about-us">REDAKSI</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/about-us">TENTANG KAMI</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="video">VIDEO</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="loker">KARIR</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/kode-etik">KODE ETIK</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="cekfakta">CEK FAKTA</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/privacy-policy">PRIVACY POLICY</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/code-of-conduct">PEDOMAN MEDIA SIBER</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!-- col end -->
                                        <div class="col-lg-5 col-md-5">
                                            <div class="footer-widtet post-widget">
                                                <h3 class="widget-title"><span>Jaringan</span></h3>
                                                <div class="widget-content">

                                                    <ul class="ts-footer-nav">
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://bisnis.com">BISNIS.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://rumah190.com">RUMAH190.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://griya190.com">GRIYA190.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://iklan.solopos.com">IKLAN</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://semarangpos.com">SEMARANGPOS.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://madiunpos.com">MADIUNPOS.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://jeda.id">JEDA.ID</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://harianjogja.com">HARIANJOGJA.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://pasangbaliho.com">PASANGBALIHO.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://tokosolopos.com">TOKO</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://ibukotakita.com">IBUKOTAKITA.COM</a></li>
                                                    </ul>							

                                                </div>
                                            </div>
                                        </div><!-- col end -->
                                    </div><!-- row end -->
                                </div><!-- container end -->
                            </div>										
                            </ul><!-- End dropdown -->
                        </li><!-- Features menu end -->
                    </ul><!--/ Nav ul end -->
                    <div class="nav-search">
                        <a href="#search-popup" class="xs-modal-popup">
                            <i class="icon icon-search1"></i>
                        </a>
                    </div>
                    <div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="xs-search-panel">
                                    <form action="{{ url('/') }}" method="get" class="ts-search-group">
                                        <div class="input-group">
                                            <input type="search" class="form-control" name="s" placeholder="Search" value="">
                                            <button class="input-group-btn search-button">
                                                <i class="icon icon-search1"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>						
                </div>					
            </nav><!--/ Navigation end -->
            
        </div><!--/ Row end -->
    </div><!--/ Container end -->	
    <div class="trending-bar trending-light d-md-block">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-9 text-center text-md-left">
                    <p class="trending-title"><i class="tsicon fa fa-bolt"></i> STORY </p>
                    <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
                        <div class="item">
                           <div class="post-content">
                              <h2 class="post-title title-small">
                                 
                              </h2>
                           </div>
                        </div>
                    </div>
                </div><!-- Col end -->
                <div class="col-md-3 text-md-right text-center">
                    <div class="ts-date">
                        <i class="fa fa-calendar-check-o"></i>{{ date("j F Y") }}
                    </div>
                </div><!-- Col end -->
            </div><!--/ Row end -->
        </div><!--/ Container end -->
    </div><!--/ Trending end -->			
</div><!-- Menu wrapper end -->

<div class="gap-50"></div>
			<section class="featured-post-area no-padding">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-20">
				<div class="col-lg-8 col-md-12 pad-r">
					<div class="owl-carousel owl-theme featured-slider h2-feature-slider">					
			          	
						<div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/office-solopos.jpg') }})">
							<div class="featured-post">
								<a class="image-link" href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat news" href="#">news</a>
										</div>
	
										<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
												<li><a href="#"><i class="icon icon-clock"></i> 4 jam yang lalu</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--/ Featured post end -->
						</div><!-- Item 1 end -->
                        
                       	<div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/office-solopos.jpg') }})">
							<div class="featured-post">
								<a class="image-link" href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat news" href="#">news</a>
										</div>
	
										<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
												<li><a href="#"><i class="icon icon-clock"></i> 4 jam yang lalu</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--/ Featured post end -->
						</div><!-- Item 1 end -->
                        
                       	<div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/office-solopos.jpg') }})">
							<div class="featured-post">
								<a class="image-link" href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat news" href="#">news</a>
										</div>
	
										<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
												<li><a href="#"><i class="icon icon-clock"></i> 4 jam yang lalu</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--/ Featured post end -->
						</div><!-- Item 1 end -->
                        
                       	<div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/office-solopos.jpg') }})">
							<div class="featured-post">
								<a class="image-link" href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat news" href="#">news</a>
										</div>
	
										<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
												<li><a href="#"><i class="icon icon-clock"></i> 4 jam yang lalu</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--/ Featured post end -->
						</div><!-- Item 1 end -->
                        
                       	<div class="item post-overaly-style post-md" style="background-image:url(./iklan-img/office-solopos.jpg)">
							<div class="featured-post">
								<a class="image-link" href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">&nbsp;</a>
								<div class="overlay-post-content">
									<div class="post-content">
										<div class="grid-category">
											<a class="post-cat news" href="#">news</a>
										</div>
	
										<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam">Merapi 3 Kali Luncurkan Awan Panas Guguran dalam 6 Jam</a>
										</h2>
										<div class="post-meta">
											<ul>
												<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
												<li><a href="#"><i class="icon icon-clock"></i> 4 jam yang lalu</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--/ Featured post end -->
						</div><!-- Item 1 end -->
                        
                           
					</div>
				</div><!-- Col 8 end -->
	            
				<div class="col-lg-4 col-md-12">
					<div class="post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/post-thumb-editor.png') }})">
						<div class="featured-post">
							<a class="image-link" href="{{ url('mediakit/single') }}" title="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua">&nbsp;</a>
							<div class="overlay-post-content">
								<div class="post-content">
									<div class="grid-category">
									<a class="post-cat premium" href="#">Espos Premium</a>
									</div>

									<h2 class="post-title title-md">
										<a href="{{ url('mediakit/single') }}" title="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua">Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua</a>
									</h2>
									<div class="post-meta">
										<ul>
											<li><a href="#"><i class="fa fa-user"></i>Cahyadi Kurniawan</a></li>
											<li><a href="#"><i class="icon icon-clock"></i> 1 jam lalu</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div><!--/ Featured post end -->
					</div><!-- Item 2 end -->
				</div><!-- Col 4 end -->
				

			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- Feature post end -->

	<!-- Section Trending start-->
	<section class="trending-slider pb-0">
		<div class="container pl-0 pr-0">
			<div class="ts-grid-box">
				<h2 class="block-title">
					 <span class="title-angle-shap"> Fokus </span>
				</h2>
				<div class="owl-carousel dot-style2" id="trending-slider">
					
					<div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/post-thumb-editor.png') }})">
						<a class="image-link" href="{{ url('mediakit/single') }}" title="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content">
								<div class="grid-category">
									<a class="post-cat news" href="#">news</a>
								</div>

								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua">Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua</a>
								</h2>
								<div class="post-meta">
									<ul>
										<li><a href="#"><i class="fa fa-user"></i>Cahyadi Kurniawan</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
					
                    <div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/post-thumb-editor.png') }})">
						<a class="image-link" href="{{ url('mediakit/single') }}" title="Tabrak Motor Sampah di Sragen, Pemotor Asal Gondangrejo Terpental Lalu Meninggal Seketika">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content">
								<div class="grid-category">
									<a class="post-cat soloraya" href="#">soloraya</a>
								</div>

								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Tabrak Motor Sampah di Sragen, Pemotor Asal Gondangrejo Terpental Lalu Meninggal Seketika">Tabrak Motor Sampah di Sragen, Pemotor Asal Gondangrejo Terpental Lalu Meninggal Seketika</a>
								</h2>
								<div class="post-meta">
									<ul>
										<li><a href="#"><i class="fa fa-user"></i>Muh Khodiq Duhri</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
					
                    <div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/post-thumb-editor.png') }})">
						<a class="image-link" href="{{ url('mediakit/single') }}" title="Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content">
								<div class="grid-category">
									<a class="post-cat soloraya" href="#">soloraya</a>
								</div>

								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai">Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai</a>
								</h2>
								<div class="post-meta">
									<ul>
										<li><a href="#"><i class="fa fa-user"></i>Taufiq Sidik Prakoso</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
					
                    <div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/post-thumb-editor.png') }})">
						<a class="image-link" href="{{ url('mediakit/single') }}" title="Setelah 11 Tahun Inter Milan Rebut Scudetto ke-19, Ini Daftar Juara Liga Italia ">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content">
								<div class="grid-category">
									<a class="post-cat sport" href="#">sport</a>
								</div>

								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Setelah 11 Tahun Inter Milan Rebut Scudetto ke-19, Ini Daftar Juara Liga Italia ">Setelah 11 Tahun Inter Milan Rebut Scudetto ke-19, Ini Daftar Juara Liga Italia </a>
								</h2>
								<div class="post-meta">
									<ul>
										<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
                    					
					<div class="item post-overaly-style post-md" style="background-image:url({{ url('images/iklan-img/post-thumb-editor.png') }})">
						<a class="image-link" href="{{ url('mediakit/single') }}" title="Guru Lumpuh Seusai Vaksin Diduga Alami Guillain Barre Syndrome, Apa Itu?">&nbsp;</a>
						<div class="overlay-post-content">
							<div class="post-content">
								<div class="grid-category">
									<a class="post-cat news" href="#">news</a>
								</div>

								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Guru Lumpuh Seusai Vaksin Diduga Alami Guillain Barre Syndrome, Apa Itu?">Guru Lumpuh Seusai Vaksin Diduga Alami Guillain Barre Syndrome, Apa Itu?</a>
								</h2>
								<div class="post-meta">
									<ul>
										<li><a href="#"><i class="fa fa-user"></i>Newswire</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div><!-- Item 1 end -->
                </div>
				<!-- most-populers end-->
			</div>
			<!-- ts-populer-post-box end-->
		</div>
		<!-- container end-->
	</section>
	<!-- section trending End -->

	<!-- Section Terkini Start -->
	<section class="block-wrapper">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8 col-md-12">
					<h2 class="block-title">
						<span class="title-angle-shap"> Berita Terkini </span>
					</h2>
					<div class="row ts-gutter-20 align-items-center">
			          			          			          
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
			          	<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" data-toggle="modal" data-target="#ModalAdvetorial" title="Iklan Advetorial">Iklan Advetorial</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Newswire</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Summary Iklan Advetorial </p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Block Konten Premium -->
					<div class="block style2 text-light mb-20 mt-10">
						<h2 class="block-title">
							<span class="title-angle-shap"> Espos Premium</span>
						</h2>

						<div class="row">
				            <div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua" style="object-fit: cover; width: 266px; height: 178px;">
										</a>
										<div class="grid-cat">
											<a class="post-cat premium" href="#">Premium</a>
										</div>
									</div>
									
									<div class="post-content mt-3">
										<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua">Keberhasilan PTM, Perilaku dan Kejujuran Orang Tua</a>
										</h2>
										<p>Keberhasilan pembelajaran tatap muka (PTM) memerlukan kejujuran orang tua perihal kondisi keluarga hingga keteladan perilaku. Yang anak-anak lihat di rumah akan mereka bawa ke sekolah.</p>
										<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i>Cahyadi Kurniawan</a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i> 1 jam lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">								
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai">Tak Ada Lonjakan Penumpang, Kecil Kemungkinan Terminal Klaten Ramai</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
														
				            							
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="#" title="Gelombang Pemudik Berkejaran dengan Larangan Mudik">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Gelombang Pemudik Berkejaran dengan Larangan Mudik" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="#" title="Gelombang Pemudik Berkejaran dengan Larangan Mudik">Gelombang Pemudik Berkejaran dengan Larangan Mudik</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
														
				            							
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="7 Tahun Hari Buruh Wonogiri Tanpa Demonstrasi, Apa Kuncinya?">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="7 Tahun Hari Buruh Wonogiri Tanpa Demonstrasi, Apa Kuncinya?" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="7 Tahun Hari Buruh Wonogiri Tanpa Demonstrasi, Apa Kuncinya?">7 Tahun Hari Buruh Wonogiri Tanpa Demonstrasi, Apa Kuncinya?</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
														
				            							
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="Jaket Rian D’Masiv pun Masuk Museum Titik Nol Pasoepati">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Jaket Rian D’Masiv pun Masuk Museum Titik Nol Pasoepati" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="Jaket Rian D’Masiv pun Masuk Museum Titik Nol Pasoepati">Jaket Rian D’Masiv pun Masuk Museum Titik Nol Pasoepati</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
								</div><!-- .row -->
							</div><!-- Col 2 end -->
												</div><!-- Row end -->
					</div><!-- Block Konten Premium end -->	

					<div class="row ts-gutter-20 loadmore-frame">
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mb-10">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i> Nugroho Meidinata</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 31 menit lalu</span>
											</div>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Id interdum velit laoreet id donec ultrices tincidunt. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
				            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Espos Premium Lainnya</a>
				            <a href="https://m.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>									
				</div><!-- Content Col end -->

				
				
				<!-- sidebar start -->
                <div class="col-lg-4">
                    <div class="sidebar">
                        <div class="sidebar-widget ads-widget">
                            <div class="ads-image">
                            
                            <div align="center">
                            <a href="" data-toggle="modal" data-target="#ModalSidebar1">
                            <img class="img-fluid" src="{{ url('images/iklan-img/desktop-sidebar-1.png') }}" >
                            </a>
                            </div>
                
                                 
                            </div>
                        </div><!-- widget end -->						
                        <div class="sidebar-widget featured-tab post-tab"><!-- style="position: -webkit-sticky; position: sticky; top: 130px;" -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link animated active fadeIn" href="#post_tab_b" data-toggle="tab">
                                        <span class="tab-head">
                                            <span class="tab-text-title">Terpopuler</span>					
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="gap-50 d-none d-md-block"></div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="tab-content">
                                        <div class="tab-pane active animated fadeInRight" id="post_tab_b">
                                            <div class="list-post-block">
                                                <ul class="list-post">
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 22 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 17 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 20 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 22 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 23 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                       
                                                </ul><!-- List post end -->
                                            </div>
                                        </div><!-- Tab pane 2 end -->
                                    </div><!-- tab content -->
                                </div>
                            </div>
                        </div><!-- widget end -->	
                                    
                            
                        <div class="sidebar-widget ads-widget">
                            <div class="ads-image">
                            
                            <div align="center">
                            <a href="#" data-toggle="modal" data-target="#ModalSidebar2">
                            <img class="img-fluid" src="{{ url('images/iklan-img/desktop-sidebar-2.png') }}" style="object-fit: cover; object-position: center; height: 250px; width: 300px;">
                            </a>
                            </div>
                  
                            </div>
                        </div><!-- widget end -->	
                        <div class="sidebar-widget">
                            <h2 class="block-title block-title-dark">
                                <span class="title-angle-shap"> News </span>
                            </h2>
                            <div class="list-post-block">
                                <ul class="list-post">
                                                                          
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 3 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->	          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 4 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->	          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 5 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->	          				
                                </ul><!-- List post end -->
                            </div>
                        </div>
                
                        <div class="sidebar-widget ads-widget">
                            <div class="ads-image">
                                <div align="center">
                                <a href="#" data-toggle="modal" data-target="#ModalSidebar3">
                                <img class="img-fluid" src="{{ url('images/iklan-img/desktop-sidebar-3.png') }}" style="object-fit: cover; object-position: center; height: 250px; width: 300px;">
                                </a>
                                </div>
                            </div>
                        </div><!-- widget end -->	
                
                        <div class="sidebar-widget">
                            <h2 class="block-title block-title-dark">
                                <span class="title-angle-shap"> Lifestyle </span>
                            </h2>
                            <div class="list-post-block">
                                <ul class="list-post">
                                                                          
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 3 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->			          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 5 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->		          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 15 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->			          				
                                </ul><!-- List post end -->
                            </div>
                        </div>
                        
                        <div class="sidebar-widget">
                            <h2 class="block-title block-title-dark">
                                <span class="title-angle-shap"> Ekbis </span>
                            </h2>
                            <div class="list-post-block">
                                <ul class="list-post">
                                                                          
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 4 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->			          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 20 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->		          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 1 hari lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->		          				
                                </ul><!-- List post end -->
                            </div>
                        </div>
                
                    </div>					
                </div><!-- Sidebar Col end -->
				<!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- Section Terkini End -->

	<!-- ad banner start-->
	<div class="block-wrapper no-padding">
		<div class="container pl-0 pr-0">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="banner-img">
						<div style="margin: 20px auto;" align="center">
						<a href="#" data-toggle="modal" data-target="#ModalLB1" title="Desktop Leaderboard Banner 1" target="_blank"><img src="{{ url('images/iklan-img/desktop-leaderboard-1.png') }}"></a>
						</div>						
					</div>
				</div>
				<!-- col end -->
			</div>
			<!-- row  end -->
		</div>
		<!-- container end -->
	</div>
	<!-- ad banner end-->

	<!-- block slider start -->
	<section class="block-slider">
		<div class="container pl-0 pr-0">
			<div class="ts-grid-box">
				<div class="owl-carousel dot-style2" id="post-block-slider">
							          			          						
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Retno-Winarni-2-555x370.jpg" alt="Sinergi Mencegah Perkawinan Anak" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Sinergi Mencegah Perkawinan Anak">Sinergi Mencegah Perkawinan Anak</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 18 jam lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Nadia-Aliya-Azki-555x370.jpg" alt="Bisnis Monyet Tanaman Hias" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Bisnis Monyet Tanaman Hias">Bisnis Monyet Tanaman Hias</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 2 hari lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Siti-Farida-2-555x370.jpg" alt="(Andai) Demokrasi Tanpa Oligarki" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="(Andai) Demokrasi Tanpa Oligarki">(Andai) Demokrasi Tanpa Oligarki</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 3 hari lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Syifaul-Arifin-3-555x370.jpg" alt="Perlawanan Perempuan" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Perlawanan Perempuan">Perlawanan Perempuan</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 4 hari lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Maria-Y.-Benyamin-3-554x370.jpg" alt="Singkong Goreng dan Segelas Wine" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Singkong Goreng dan Segelas Wine">Singkong Goreng dan Segelas Wine</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 5 hari lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Albertus-Rusputranto-PA-2-555x370.jpg" alt="Punden-Punden Kota Solo" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Punden-Punden Kota Solo">Punden-Punden Kota Solo</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 6 hari lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Ayu-Prawitasari-2-555x370.jpg" alt="Membaca Perempuan Kini" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Membaca Perempuan Kini">Membaca Perempuan Kini</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 7 hari lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
										
					<div class="item">
						<div class="post-block-style">
							<div class="post-thumb post-list_slides">
								<a href="{{ url('mediakit/single') }}">
									<img class="img-fluid" src="https://images.solopos.com/2021/04/Elly-Jauharah-Asriani-1-555x370.jpg" alt="Bukan Cuma Kebaya dan Sanggul" style="object-fit: cover; height: 148px; width: 222px;">
								</a>
								<div class="grid-cat">
									<a class="post-cat kolom" href="#">kolom</a>
								</div>
							</div>
							
							<div class="post-content">
								<h2 class="post-title">
									<a href="{{ url('mediakit/single') }}" title="Bukan Cuma Kebaya dan Sanggul">Bukan Cuma Kebaya dan Sanggul</a>
								</h2>
								<div class="post-meta mb-7">
									<span class="post-date"><i class="fa fa-clock-o"></i> 1 minggu lalu</span>
								</div>
							</div><!-- Post content end -->
						</div><!-- Post Block style end -->
					</div><!-- slide-item end -->
				</div>
			</div>
			<!-- ts-populer-post-box end-->
		</div>
		<!-- container end-->
	</section>
	<!-- block slider end -->

	<!-- black section start -->
	<section class="block-wrapper">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8 col-md-12">

					<div class="block style1 text-light mb-20 mt-10">
						<h2 class="block-title">
							<span class="title-angle-shap"> Jawa Tengah </span>
						</h2>

						<div class="row">
				            <div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="New Tim Elang Bergerak, Sejumlah Pemuda Pemicu Tawuran di Semarang Ditangkap">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="New Tim Elang Bergerak, Sejumlah Pemuda Pemicu Tawuran di Semarang Ditangkap" style="object-fit: cover; width: 266px; height: 178px;">
										</a>
										<div class="grid-cat">
											<a class="post-cat premium" href="">Premium</a>
										</div>
									</div>
									
									<div class="post-content mt-3">
										<h2 class="post-title title-md">
											<a href="#" title="New Tim Elang Bergerak, Sejumlah Pemuda Pemicu Tawuran di Semarang Ditangkap">New Tim Elang Bergerak, Sejumlah Pemuda Pemicu Tawuran di Semarang Ditangkap</a>
										</h2>
										<p></p>
										<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i></a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i> 6 jam lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">	
								
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="Jembatan Layang Bandara Ahmad Yani Ditarget Rampung Agustus 2021">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Jembatan Layang Bandara Ahmad Yani Ditarget Rampung Agustus 2021" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="Jembatan Layang Bandara Ahmad Yani Ditarget Rampung Agustus 2021">Jembatan Layang Bandara Ahmad Yani Ditarget Rampung Agustus 2021</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
														
				            							
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="Rampung Agustus 2021, Ini Penampakan Pembangunan Jalan Layang Bandara Ahmad Yani Semarang">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Rampung Agustus 2021, Ini Penampakan Pembangunan Jalan Layang Bandara Ahmad Yani Semarang" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="Rampung Agustus 2021, Ini Penampakan Pembangunan Jalan Layang Bandara Ahmad Yani Semarang">Rampung Agustus 2021, Ini Penampakan Pembangunan Jalan Layang Bandara Ahmad Yani Semarang</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
														
				            							
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="Tabrak Markah Jalan di Depan Banaran Cafe Salatiga, Mahasiswa asal Papua Meninggal">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Tabrak Markah Jalan di Depan Banaran Cafe Salatiga, Mahasiswa asal Papua Meninggal" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="Tabrak Markah Jalan di Depan Banaran Cafe Salatiga, Mahasiswa asal Papua Meninggal">Tabrak Markah Jalan di Depan Banaran Cafe Salatiga, Mahasiswa asal Papua Meninggal</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								
														
				            							
									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url('mediakit/single') }}" title="Fasilitas Kota Tegal Mampu Hadirkan Sister City Perikanan">
													<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Fasilitas Kota Tegal Mampu Hadirkan Sister City Perikanan" style="object-fit: cover; width: 118px; height: 84px;">
												</a>
											</div>
											
											<div class="post-content">
												<h2 class="post-title mb-2">
													<a href="{{ url('mediakit/single') }}" title="Fasilitas Kota Tegal Mampu Hadirkan Sister City Perikanan">Fasilitas Kota Tegal Mampu Hadirkan Sister City Perikanan</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->
								</div><!-- .row -->
							</div><!-- Col 2 end -->
												</div><!-- Row end -->
					</div><!-- Block Lifestyle end -->


				</div><!-- Content Col end -->
				<div class="col-lg-4 col-md-12">
					<h2 class="block-title block-title-dark">
						<span class="title-angle-shap"> Otomotif </span>
					</h2>
					<div class="list-post-block">
						<ul class="list-post">
				          				          				          	
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Bakal Ada 15 Juta Kendaraan Listrik yang Mengaspal di Jalanan Indonesia 2030">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bakal Ada 15 Juta Kendaraan Listrik yang Mengaspal di Jalanan Indonesia 2030" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Bakal Ada 15 Juta Kendaraan Listrik yang Mengaspal di Jalanan Indonesia 2030">Bakal Ada 15 Juta Kendaraan Listrik yang Mengaspal di Jalanan Indonesia 2030</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 5 hari lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
								
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Pakai Mobil Listrik Bisa Hemat Hingga Ratusan Ribu, Yakin Enggak Mau Coba?">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Pakai Mobil Listrik Bisa Hemat Hingga Ratusan Ribu, Yakin Enggak Mau Coba?" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Pakai Mobil Listrik Bisa Hemat Hingga Ratusan Ribu, Yakin Enggak Mau Coba?">Pakai Mobil Listrik Bisa Hemat Hingga Ratusan Ribu, Yakin Enggak Mau Coba?</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 5 hari lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
								
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Bosan Honda HR-V Standar? Pilih Varian dengan Sentuhan Mugen">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bosan Honda HR-V Standar? Pilih Varian dengan Sentuhan Mugen" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Bosan Honda HR-V Standar? Pilih Varian dengan Sentuhan Mugen">Bosan Honda HR-V Standar? Pilih Varian dengan Sentuhan Mugen</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 6 hari lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
								
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Mobil Terbang Ehang 216 Resmi Bakal Masuk Indonesia Medio 2021">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Mobil Terbang Ehang 216 Resmi Bakal Masuk Indonesia Medio 2021" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Mobil Terbang Ehang 216 Resmi Bakal Masuk Indonesia Medio 2021">Mobil Terbang Ehang 216 Resmi Bakal Masuk Indonesia Medio 2021</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 6 hari lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
						</ul><!-- List post end -->
					</div>
				</div><!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- black section end -->

	<!-- ad banner start-->
	<div class="block-wrapper no-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="banner-img text-center">
						<div style="margin: 20px auto;" align="center">
						<a href="" data-toggle="modal" data-target="#ModalLB2" title="Desktop Leaderboard Banner 2" target="_blank"><img src="{{ url('images/iklan-img/desktop-leaderboard-2.png') }}"></a>
						</div>	
					</div>
				</div>
				<!-- col end -->
			</div>
			<!-- row  end -->
		</div>
		<!-- container end -->
	</div>
	<!-- ad banner end-->

	<!-- card block start -->
	<section class="block-wrapper">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8 col-md-12">
					<h2 class="block-title">
						<span class="title-angle-shap"> #Espospedia / Info Grafis  </span>
					</h2>
						<div class="row text-light ts-gutter-30">
				          				          				          							
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url(https://images.solopos.com/2021/04/230421_KAPALSELAM_HEADER-560x281.jpg)">
								<a href="{{ url('mediakit/single') }}" title="Kapal Selam Indonesia">&nbsp;</a>
							</div><!-- post end -->
						</div><!-- end col -->
													
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url(https://images.solopos.com/2021/04/190421_ANJING_HEADER-560x280.jpg)">
								<a href="{{ url('mediakit/single') }}" title="Bahaya Mengonsumsi Daging Anjing">&nbsp;</a>
							</div><!-- post end -->
						</div><!-- end col -->
													
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url(https://images.solopos.com/2021/04/140421_TAKJIL_HEADER-560x280.jpg)">
								<a href="{{ url('mediakit/single') }}" title="7 Lokasi Penjual Takjil di Solo">&nbsp;</a>
							</div><!-- post end -->
						</div><!-- end col -->
													
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url(https://images.solopos.com/2021/04/150421_MAAG_HEADER-560x281.jpg)">
								<a href="{{ url('mediakit/single') }}" title="Cegah Maag Saat Berpuasa">&nbsp;</a>
							</div><!-- post end -->
						</div><!-- end col -->
													
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url(https://images.solopos.com/2021/04/150421_BANGJO_HEADER-560x280.jpg)">
								<a href="{{ url('mediakit/single') }}" title="Tahukah Anda? Begini Sejarah Bangjo">&nbsp;</a>
							</div><!-- post end -->
						</div><!-- end col -->
													
						<div class="col-md-4">
							<div class="post-overaly-style post-sm" style="background-image:url(https://images.solopos.com/2021/04/080421_INTERNET_HEADER-560x280.jpg)">
								<a href="{{ url('mediakit/single') }}" title="Wusss! Berikut Deretan Kota di Indonesia yang Punya Internet Cepat">&nbsp;</a>
							</div><!-- post end -->
						</div><!-- end col -->
											</div><!-- end row -->
					
				</div><!-- Content Col end -->
				<div class="col-lg-4 col-md-12">
					<h2 class="block-title block-title-dark">
						<span class="title-angle-shap"> Sepakbola </span>
					</h2>
					<div class="list-post-block">
						<ul class="list-post">
				          				          				          	
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Prokes Di Stadion Manahan Jadi Contoh, Kesuksesan Piala Menpora Modal Utama Menuju Liga">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Prokes Di Stadion Manahan Jadi Contoh, Kesuksesan Piala Menpora Modal Utama Menuju Liga" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Prokes Di Stadion Manahan Jadi Contoh, Kesuksesan Piala Menpora Modal Utama Menuju Liga">Prokes Di Stadion Manahan Jadi Contoh, Kesuksesan Piala Menpora Modal Utama Menuju Liga</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 1 minggu lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
								
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Laga Leeds Versus MU Berakhir Tanpa Gol di Saat Gema Protes #GlazersOut">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Laga Leeds Versus MU Berakhir Tanpa Gol di Saat Gema Protes #GlazersOut" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Laga Leeds Versus MU Berakhir Tanpa Gol di Saat Gema Protes #GlazersOut">Laga Leeds Versus MU Berakhir Tanpa Gol di Saat Gema Protes #GlazersOut</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 1 minggu lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
								
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Juarai Piala Liga Inggris, Manchester City Samai Rekor Liverpool">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Juarai Piala Liga Inggris, Manchester City Samai Rekor Liverpool" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Juarai Piala Liga Inggris, Manchester City Samai Rekor Liverpool">Juarai Piala Liga Inggris, Manchester City Samai Rekor Liverpool</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 1 minggu lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
								
							<li>
								<div class="post-block-style media">
									<div class="post-thumb">
										<a href="{{ url('mediakit/single') }}" title="Tak Hanya Hadiah Rp2 Miliar, Persija bakal Diguyur Bonus Seusai Menang Piala Menpora">
											<img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Tak Hanya Hadiah Rp2 Miliar, Persija bakal Diguyur Bonus Seusai Menang Piala Menpora" style="object-fit: cover; width: 130px; height: 96px;">
										</a>
									</div><!-- Post thumb end -->

									<div class="post-content media-body">
										<h2 class="post-title">
											<a href="{{ url('mediakit/single') }}" title="Tak Hanya Hadiah Rp2 Miliar, Persija bakal Diguyur Bonus Seusai Menang Piala Menpora">Tak Hanya Hadiah Rp2 Miliar, Persija bakal Diguyur Bonus Seusai Menang Piala Menpora</a>
										</h2>
										<div class="post-meta mb-7">
											<span class="post-date"><i class="fa fa-clock-o"></i> 1 minggu lalu</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block style end -->
							</li><!-- Li 1 end -->		          				
						</ul><!-- List post end -->
					</div>
				</div><!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- card block end -->
	
<!-- Footer start -->
<div class="ts-footer">
    <div class="container">
        <div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
            <div class="col-lg-7 col-md-7">
                <div class="footer-widget">
                    <div class="footer-logo">
                        <img src="{{ url('images/logo.png') }}" alt="Footer Logo">
                    </div>
                    <div class="widget-content">
                        <ul class="ts-footer-nav">
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">KONTAK</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">COPYRIGHT</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">WRITING CONTEST</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">REDAKSI</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">TENTANG KAMI</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">VIDEO</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">KARIR</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">KODE ETIK</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">CEK FAKTA</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">PRIVACY POLICY</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">PEDOMAN MEDIA SIBER</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- col end -->
            <div class="col-lg-5 col-md-5">
                <div class="footer-widtet post-widget">
                    <h3 class="widget-title"><span>Jaringan</span></h3>
                    <div class="widget-content">

                        <ul class="ts-footer-nav">
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">BISNIS.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">RUMAH190.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">GRIYA190.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">IKLAN</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">SEMARANGPOS.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">MADIUNPOS.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">JEDA.ID</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">HARIANJOGJA.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">PASANGBALIHO.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">TOKO</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">IBUKOTAKITA.COM</a></li>
                        </ul>							

                    </div>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- container end -->
</div>
<!-- Footer End-->

<!-- ts-copyright start -->
<div class="ts-copyright">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 text-center">
                <div class="copyright">
                    <p>Copyright &copy; 2007-2021, Solopos Digital Media - Panduan Informasi & Inspirasi. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ts-copyright end-->

<!-- backto -->
<div class="top-up-btn">
    <div class="backto" style="display: block;"> 
        <a href="#" class="icon icon-arrow-up" aria-hidden="true"></a>
    </div>
</div>
<!-- backto end-->

<!-- Javascript Files
================================================== -->

<!-- initialize jQuery Library -->
<script src="{{ asset('js/jquery.js') }}"></script>
<!-- Popper Jquery -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootstrap jQuery -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- magnific-popup -->
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<!-- Color box -->
<script src="{{ asset('js/jquery.colorbox.js') }}"></script>
<!-- Template custom -->
<script src="{{ asset('js/custom.js') }}"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script>
function showHide(d)
{
var onediv = document.getElementById(d);
var divs=['div-small','div-big'];
for (var i=0;i<divs.length;i++)
  {
  if (onediv != document.getElementById(divs[i]))
    {
    document.getElementById(divs[i]).style.display='none';
    }
  }
onediv.style.display = 'block';
} 

</script>


<!-- Modal Sidebar 1-->
<div class="modal fade" id="ModalSidebar1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Sidebar 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>Seluruh Halaman Desktop (Kecuali Halaman Premium) <br><br>
          Rekomendasi Ukuran : 300 x 250 pixel, 300 x 300 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Sidebar 2-->
  <div class="modal fade" id="ModalSidebar2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Sidebar 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>Seluruh Halaman Desktop (Kecuali Halaman Premium) <br><br>
          Rekomendasi Ukuran : 300 x 250 pixel, 300 x 300 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Sidebar 3-->
  <div class="modal fade" id="ModalSidebar3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Sidebar 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>Seluruh Halaman Desktop (Kecuali Halaman Premium) <br><br>
          Rekomendasi Ukuran : 300 x 250 pixel, 300 x 300 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <!-- Modal Widget-->
  <div class="modal fade" id="ModalWidget" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Widget Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Iklan dalam bentuk berita 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Advetorial-->
  <div class="modal fade" id="ModalAdvetorial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Advetorial</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Iklan berita yang disisipkan selalu tampil di halaman awal (HOMEPAGE) selama renntang waktu tertentu.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Leaderboard 1-->
  <div class="modal fade" id="ModalLB1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Leaderboard 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Homepage<br>
          <b>Rekomendasi Ukuran : </b>728 x 90 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Leaderboard 2-->
  <div class="modal fade" id="ModalLB2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Leaderboard 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Homepage<br>
          <b>Rekomendasi Ukuran : </b>728 x 90 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Artikel 1-->
  <div class="modal fade" id="ModalArtikel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Artikel 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Artikel/Single Desktop Setelah Paragraf ke-5<br>
          <b>Rekomendasi Ukuran : </b>640 x 100 pixel, 640 x 150 pixel, 640 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Artikel 2-->
  <div class="modal fade" id="ModalArtikel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Artikel 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Artikel/Single Desktop Setelah Paragraf ke-9<br>
          <b>Rekomendasi Ukuran : </b>640 x 100 pixel, 640 x 150 pixel, 640 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Artikel 3-->
  <div class="modal fade" id="ModalArtikel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Artikel 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Artikel/Single Desktop di akhir isi Konten/Artikel<br>
          <b>Rekomendasi Ukuran : </b>640 x 100 pixel, 640 x 150 pixel, 640 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Modal Wrapt Side Kanan Kiri-->
  <div class="modal fade" id="ModalWraptSide" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Wrapt Side Left-Right Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Seluruh Halaman Desktop Sebelah Kanan & Kiri (Desain Bisa sama ataupun berbeda di kanan & kiri)<br>
          <b>Rekomendasi Ukuran : </b>250 x 750 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Modal Wrapt Top-->
  <div class="modal fade" id="ModalWraptTop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Wrapt Top Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Seluruh Halaman Desktop Paling Atas (Diatas Logo/Navigasi)<br>
          <b>Rekomendasi Ukuran : </b>996 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
</body>
</html>
