<html>
<head>
    <title>Ads Inventory Solopos.com Single Page</title>
    <meta content="Ads Inventory Solopos.com Single Page" itemprop="headline" />
    <meta name="title" content="Ads Inventory Solopos.com Single Page" />
    <meta name="description" content="Ads Inventory Solopos.com Single Page" itemprop="description" />
    <meta name="keywords" content="Ads Inventory Solopos.com Single Page" itemprop="keywords" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1200, initial-scale=1">
    <meta name="copyright" content="Solopos Media Group" /> 
    <meta name="googlebot-news" content="index,follow" />    
    <meta name="googlebot" content="index,follow,all" />
    <meta name="Bingbot" content="index,follow">
    <meta name="robots" content="index,follow,max-image-preview:large" />
    <meta name="google-site-verification" content="AA4UjbZywyFmhoSKLELl4RA451drENKllt5Sbq9uINw" />
    <meta name="yandex-verification" content="7966bb082003f8ae" />
    <meta name="msvalidate.01" content="F2320220951CEFB78E7527ECC232031C" />
    <meta name='dailymotion-domain-verification' content='dm0w2nbrkrxkno2q2' />
    <meta name="language" content="id" />
    <meta name="geo.country" content="id" />
    <meta http-equiv="content-language" content="In-Id" />
    <meta name="geo.placename" content="Indonesia" />
    <meta name="showus-verification" content="pub-2627" />

	<!--Favicon-->
	<link rel="shortcut icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon">

    <!-- Stylesheet -->
	<link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/iconfonts.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" />
	<link rel="stylesheet" href="{{ url('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/owl.theme.default.min.css') }}">
	<link rel="stylesheet" href="{{ url('css/magnific-popup.css') }}">
	<link rel="stylesheet" href="{{ url('css/animate.css') }}">
	<link rel="stylesheet" href="{{ url('css/style.css') }}?v={{time()}}">
	<link rel="stylesheet" href="{{ url('css/responsive.css') }}">
	<link rel="stylesheet" href="{{ url('css/colorbox.css') }}">
</head>

<div id="mega-billboard-container" class="smooth" data-height="400px">
    <div id="div-small" class="smooth">
        <a href="" data-toggle="modal" data-target="#ModalWraptTop">
            <img src="{{ url('images/iklan-img/desktop-wrapttop.png') }}">
        </a>
    </div>
</div>
<div id="skinad-left">            
    <div id="left-lk">
        <a href="" data-toggle="modal" data-target="#ModalWraptSide">
            <img src="{{ url('images/iklan-img/desktop-wraptleft.png') }}">
        </a>
        
    </div>
</div>

<div id="skinad-right">
    <div id="right-lk">
        <a href="" data-toggle="modal" data-target="#ModalWraptSide">
            <img src="{{ url('images/iklan-img/desktop-wraptright.png') }}">
        </a>
    </div>
</div>

<!-- Header start -->
<header id="header" class="header">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-md-3 col-sm-12">
                <div class="header-nav">
                    <a href="{{ url('/espospedia') }}">Espospedia</a> | <a href="{{ url('/cekfakta') }}">Cek Fakta</a>
                    
                    <div class="top-social">
                        <ul class="social list-unstyled" style="padding:0;">
                            <li><a href="https://www.facebook.com/soloposcom" target="_blank" title="Facebook FP"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.twitter.com/soloposdotcom" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/koransolopos" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="mailto:redaksi@solopos.co.id" target="_blank" title="Emal"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>
                                        
                </div>
            </div><!-- logo col end -->		

            <div class="col-md-6 col-sm-12">
                <div class="logo">
                    <a href="{{ url('/mediakit') }}"> 
                      <img class="logo-image" src="{{ url('images/logo.png') }}" style="width:300px;" alt="Solopos.com">
                    </a>
                </div>
            </div><!-- logo col end -->
            <div class="col-md-3 col-sm-12">
                <div class="header-nav" style="text-align: right;">
                    <div style="height: 20px;display:inline;float: right; margin-bottom:10px;">
                        <a href="{{ url('/premium') }}">
                            <img class="logo-image" src="{{ url('images/premium.png') }}" height="23" alt="Espos Premium">
                        </a> 
                    </div>
                    <div style="clear: right;font-weight:500;font-size:13px;">
                    @if(Cookie::get('is_login') == 'true')
                    <a href="https://id.solopos.com/profile" target="_blank">PROFIL</a> | <a href="https://id.solopos.com" target="_blank">LOGOUT</a>
                    @else
                    <a href="https://id.solopos.com/login" target="_blank">LOGIN</a> | <a href="https://id.solopos.com/register" target="_blank">DAFTAR</a>
                    @endif
                    </div>
                </div>
            </div><!-- logo col end -->

        </div><!-- Row end -->
    </div><!-- Logo and banner area end -->
</header><!--/ Header end -->

<div class="main-nav clearfix is-ts-sticky">
    <div class="container">
        <div class="row justify-content-between">
            <nav class="navbar navbar-expand-lg col-lg-12">
                <div class="site-nav-inner float-left">
                       <!-- End of Navbar toggler -->
                       <div id="navbarSupportedContent" class="collapse navbar-collapse navbar-responsive-collapse">
                        <ul class="nav navbar-nav">
                            <li class="sticky">
                                                   
                                {{-- <img src="/images/logo-.png" alt="Logo"> --}}
                                
                                <img src="{{ url('/images/logo.png') }}" alt="Logo">
                                
                            </li>							
                            <li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='news'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">News<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-submenu">
                                        <a href="{{ url('news/pendidikan') }}" class="menu-dropdown" data-toggle="dropdown">Pendidikan</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('/news/uns') }}">UNS</a></li>
                                            <li><a href="{{ url('uksw') }}">UKSW</a></li>
                                        </ul>
                                    </li>										
                                    <li><a href="{{ url('/news/nasional') }}">Nasional</a></li>
                                    <li><a href="{{ url('/news/internasional') }}">Internasional</a></li>
                                </ul>
                            </li><!-- Features menu end -->									
                            <li><a href="{{ url('/bisnis') }}">Ekbis</a></li>
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='soloraya'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Soloraya<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/soloraya/solo') }}">Solo</a></li>
                                    <li><a href="{{ url('/soloraya/sukoharjo') }}">Sukoharjo</a></li>
                                    <li><a href="{{ url('/soloraya/karanganyar') }}">Karanganyar</a></li>
                                    <li><a href="{{ url('/soloraya/sragen') }}">Sragen</a></li>
                                    <li><a href="{{ url('/soloraya/wonogiri') }}">Wonogiri</a></li>
                                    <li><a href="{{ url('/soloraya/klaten') }}">Klaten</a></li>
                                    <li><a href="{{ url('/soloraya/boyolali') }}">Boyolali</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='sport'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Sport<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/sport/bola') }}">Bola</a></li>
                                    <li><a href="{{ url('/sport/arena') }}">Arena</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='lifestyle'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Lifestyle<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ ('/lifestyle/leisure') }}">Leisure</a></li>
                                    <li><a href="{{ ('/lifestyle/viral') }}">Viral</a></li>
                                    <li><a href="{{ ('/lifestyle/anak') }}">Anak</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Regional<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown-submenu">
                                        <a href="#" onclick="window.location.href='jateng'" class="menu-dropdown" data-toggle="dropdown">Jateng</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('/jateng/semarang') }}">Semarang</a></li>
                                            <li><a href="{{ url('/jateng/magelang') }}">Magelang</a></li>
                                            <li><a href="{{ url('/jateng/kudus') }}">Kudus</a></li>
                                            <li><a href="{{ url('/jateng/grobogan') }}">Grobogan</a></li>
                                            <li><a href="{{ url('/jateng/pemalang') }}">Pemalang</a></li>
                                            <li><a href="{{ url('/jateng/salatiga') }}">Salatiga</a></li>
                                            <li><a href="{{ url('/jateng/blora') }}">blora</a></li>
                                            <li><a href="{{ url('/jateng/pati') }}">Pati</a></li>
                                            <li><a href="{{ url('/jateng/banyumas') }}">Banyumas</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" onclick="window.location.href='jogja'" class="menu-dropdown" data-toggle="dropdown">Jogja</a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('jogja/bantul') }}">Bantul</a></li>
                                            <li><a href="{{ url('jogja/gunungkidul') }}">Gunung Kidul</a></li>
                                            <li><a href="{{ url('jogja/kota-jogja') }}">Kota Jogja</a></li>
                                            <li><a href="{{ url('jogja/kulonprogo') }}">Kulon Progo</a></li>
                                            <li><a href="{{ url('jogja/sleman') }}">Sleman</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('/jatim') }}">Jatim</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li><a href="{{ url('/otomotif') }}">Otomotif</a></li>
                            <li><a href="{{ url('/teknologi') }}">Teknologi</a></li>
                            <li class="dropdown">
                                <a href="#" onclick="window.location.href='entertainment'" class="dropdown-toggle menu-dropdown" data-toggle="dropdown">Entertainment<i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/entertainment/artis') }}">Artis</a></li>
                                    <li><a href="{{ url('/entertainment/hiburan') }}">Hiburan</a></li>
                                </ul>
                            </li><!-- Features menu end -->	
                            <li><a href="{{ url('/rsjihsolo') }}">Bugar</a></li>
                            <li><a href="{{ url('/arsip') }}">Indeks</a></li>
                            							
                        </ul><!--/ Nav ul end -->
                    </div><!--/ Collapse end -->
                </div><!-- Site Navbar inner end -->
                <div class="text-right" style="min-width: 65px; ">
                    <ul class="nav navbar-nav" style="float: left;">				
                        <li class="dropdown mega-dropdown">
                            <a style="font-size:18px;" href="#" class="dropdown-toggle menu-dropdown" data-toggle="dropdown"><i class="icon icon-menu"></i></a>
                            <ul class="dropdown-menu" role="menu">
                            <!-- responsive dropdown end -->
                            <div class="ts-footer">
                                <div class="container">
                                    <div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
                                        <div class="col-lg-7 col-md-7">
                                            <div class="footer-widget">
                                                <div class="footer-logo">
                                                    <img src="{{ url('/images/logo.png') }}" alt="Footer Logo">
                                                </div>
                                                <div class="widget-content">
                                                    <ul class="ts-footer-nav">
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/kontak">KONTAK</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/copyright">COPYRIGHT</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="writing-contest">WRITING CONTEST</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/about-us">REDAKSI</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/about-us">TENTANG KAMI</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="video">VIDEO</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="loker">KARIR</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/kode-etik">KODE ETIK</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="cekfakta">CEK FAKTA</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/privacy-policy">PRIVACY POLICY</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="page/code-of-conduct">PEDOMAN MEDIA SIBER</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div><!-- col end -->
                                        <div class="col-lg-5 col-md-5">
                                            <div class="footer-widtet post-widget">
                                                <h3 class="widget-title"><span>Jaringan</span></h3>
                                                <div class="widget-content">

                                                    <ul class="ts-footer-nav">
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://bisnis.com">BISNIS.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://rumah190.com">RUMAH190.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://griya190.com">GRIYA190.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://iklan.solopos.com">IKLAN</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://semarangpos.com">SEMARANGPOS.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://madiunpos.com">MADIUNPOS.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://jeda.id">JEDA.ID</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://harianjogja.com">HARIANJOGJA.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://pasangbaliho.com">PASANGBALIHO.COM</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://tokosolopos.com">TOKO</a></li>
                                                        <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="https://ibukotakita.com">IBUKOTAKITA.COM</a></li>
                                                    </ul>							

                                                </div>
                                            </div>
                                        </div><!-- col end -->
                                    </div><!-- row end -->
                                </div><!-- container end -->
                            </div>										
                            </ul><!-- End dropdown -->
                        </li><!-- Features menu end -->
                    </ul><!--/ Nav ul end -->
                    <div class="nav-search">
                        <a href="#search-popup" class="xs-modal-popup">
                            <i class="icon icon-search1"></i>
                        </a>
                    </div>
                    <div class="zoom-anim-dialog mfp-hide modal-searchPanel ts-search-form" id="search-popup">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="xs-search-panel">
                                    <form action="{{ url('/') }}" method="get" class="ts-search-group">
                                        <div class="input-group">
                                            <input type="search" class="form-control" name="s" placeholder="Search" value="">
                                            <button class="input-group-btn search-button">
                                                <i class="icon icon-search1"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>						
                </div>					
            </nav><!--/ Navigation end -->
            
        </div><!--/ Row end -->
    </div><!--/ Container end -->	
    <div class="trending-bar trending-light d-md-block">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-9 text-center text-md-left">
                    <p class="trending-title"><i class="tsicon fa fa-bolt"></i> STORY </p>
                    <div id="trending-slide" class="owl-carousel owl-theme trending-slide">
                        <div class="item">
                           <div class="post-content">
                              <h2 class="post-title title-small">
                                 
                              </h2>
                           </div>
                        </div>
                    </div>
                </div><!-- Col end -->
                <div class="col-md-3 text-md-right text-center">
                    <div class="ts-date">
                        <i class="fa fa-calendar-check-o"></i>{{ date("j F Y") }}
                    </div>
                </div><!-- Col end -->
            </div><!--/ Row end -->
        </div><!--/ Container end -->
    </div><!--/ Trending end -->			
</div><!-- Menu wrapper end -->

<div class="gap-50"></div>
<div class="breadcrumb-section">
	<div class="container ">
		<div class="row">
			<div class="col-12">
				<ol class="breadcrumb">
					<li>
						<a href="{{ url('/mediakit') }}"><i class="fa fa-home"></i></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><a href="#">soloraya</a></li>
					<li><i class="fa fa-angle-right"></i>5 Kuliner Gerobakan yang Lagi Viral di Solo, Sudah Pernah Coba?</li>
				</ol>		
			</div>
		</div><!-- row end -->
	</div><!-- container end -->
</div>
<!-- breadcrumb end -->
<section class="main-content pt-0">
	<div class="container pl-0 pr-0">
		<div class="row ts-gutter-30">
			<div class="col-lg-8">
				<div class="single-post">
					<div class="post-header-area">
						<h1 class="post-title title-lg">5 Kuliner Gerobakan yang Lagi Viral di Solo, Sudah Pernah Coba?</h1>
						<p>Ini dia lima kuliner viral dijual di gerobak di Solo, Jawa Tengah. Kira-kira kamu suka yang mana dari kelima jajanan viral tersebut? </p>
						<ul class="post-meta">
							<li>
								<a class="post-cat soloraya" href="#">soloraya</a>
							</li>
							<li class="post-author">
								<a href="#"><strong>Nugroho Meidinata</strong></a>
							</li>
							<li><a href="#"><i class="fa fa-clock-o"></i>Senin, 3 Mei 2021 - 14:06 WIB</a></li>
							<li><a href="#"><i class="fa fa-eye"></i>3 menit baca</a></li>
							<li class="social-share">
								<i class="shareicon fa fa-share"></i>
								<ul class="social-list">
									<li><a data-social="facebook" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=#" target="_blank" rel="noopener"><i class="fa fa-facebook"></i></a></li>
									<li><a data-social="twitter" class="twitter" href="https://twitter.com/home?status=#" target="_blank" rel="noopener"><i class="fa fa-twitter"></i></a></li>
									<li><a data-social="whatsapp" class="whatsapp" href="whatsapp://send?text=*5 Kuliner Gerobakan yang Lagi Viral di Solo, Sudah Pernah Coba?* | Ini dia lima kuliner viral dijual di gerobak di Solo, Jawa Tengah. Kira-kira kamu suka yang mana dari kelima jajanan viral tersebut?  |  _selengkapnya baca di sini_ #"><i class="fa fa-whatsapp"></i></a></li>
									<li><a data-social="email" class="email" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang 5 Kuliner Gerobakan yang Lagi Viral di Solo, Sudah Pernah Coba?&amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini #"><i class="fa fa-envelope"></i></a></li>
								</ul>
							</li>
						</ul>
					</div><!-- post-header-area end -->
					<div class="post-content-area">
						<div class="post-media mb-20">
							<img src="{{ url('/images/iklan-img/office-solopos.jpg') }}" alt="5 Kuliner Gerobakan yang Lagi Viral di Solo, Sudah Pernah Coba?" class="img-fluid">
							<span><p>SOLOPOS.COM - </p></span>
						</div>
						<p><p><strong>Solopos.com, SOLO</strong> -- Di bawah ini ada kuliner gerobakan yang lagi viral di Solo, Jawa Tengah.</p>
						<p>Berbicara Solo tak bisa lepas dengan beraneka macam kulinernya. Berbagai penganan lezat nan menarik banyak ditemukan di <strong><a href="#">Solo</a></strong>.</p>
						<p><em><strong>Baca Juga: <a href="#"> Mudik Dilarang, Enaknya Ngapain Ya Pas Libur Lebaran?</a></strong></em></p>
						<p>Mulai dari aneka sate, soto, tengkleng, nasi liwet, gudeg, selat dan aneka kuliner lainnya sangat mudah ditemukan di Solo.</p>
						<p>Selain <strong><a href="#">kuliner</a> </strong>di atas, ternyata ada kuliner lain yang lagi viral di Solo dan dijual di gerobak.
						
						<div class="iklan" align="center" style="margin-bottom: 30px;">
							<a href="" data-toggle="modal" data-target="#ModalArtikel1" title="" target="_blank"><img src="{{ url('/images/iklan-img/desktop-artikel-1.png') }}"></a>
						</div>		                    		                    
						
						<p><em><strong>Baca Juga: <a href="#"> Pertama Kali, Paguyuban Lintas Selatan Bagikan Takjil di Gembongan Sukoharjo</a></strong></em></p>
						
						<p>Apa saja kuliner viral yang dijual di gerobakan di Solo itu? Berikut ini di antaranya.</p>
						<h3>5 Kuliner Viral di Solo</h3>
						<h4>1. Martabak Korea</h4>
						<div class="iklan" align="center" style="margin-bottom: 30px;">
							<a href="" data-toggle="modal" data-target="#ModalArtikel2" title="" target="_blank"><img src="{{ url('/images/iklan-img/desktop-artikel-2.png') }}"></a>
						</div>	
						</p>
						<p>Jika biasanya masyarakat mengenal martabak Tegal, di pinggiran Solo, tepatnya di Cemani, Sukoharjo terdapat jajanan yang dijual di gerobak, yakni martabak Korea.</p>
						<p>Berbeda dengan martabak lainnya, martabak Korea ini berukuran lebih kecil dan memiliki tekstur lebih renyah.</p>
						<p><em><strong>Baca Juga: <a href="#"> Kapan Malam Lailatul Qadar? Kenali Tanda-tandanya di Sini</a></strong></em></p>
						<p>Isian dari kuliner viral di Solo ini ada daun bawang dan juga telur.</p>
						<p>Uniknya martabak telur Korea ini dinikmati dengan cocolan sambal yang rasanya pedas manis dan gurih. Karena rasanya yang lezat dan harganya yang murah cuma Rp1.000 per biji, kuliner ini banyak diminati masyarakat.</p>
						<p><em><strong>Baca Juga: <a href="#"> 4 Kuliner Khas Angkringan Solo, Bisa Jadi Menu Buka Puasa Hlo!</a></strong></em></p>
						<h4>2. Sate Kenyil</h4>
													
						<p>Kuliner viral di Solo berikutnya ada sate kenyil. Ada banyak penjual yang menjajakn sate kenyil di Solo.</p>
						<p>Beberapa di antaranya ada di belakang <strong><a href="#">Stadion Manahan</a></strong> dan ada pula di Gentan, Baki, Sukoharjo.</p>
						<p><em><strong>Baca Juga: <a href="#"> Ini Menu Sahur Bersejarah Soekarno-Hatta Saat Malam Penyusunan Teks Proklamasi</a></strong></em></p>
						<p>Sate yang memiliki tekstur kenyal ini sangat murah <em>hlo</em>, cuma Rp500 per tusuk. Bagaimana, mau mencoba?</p>
						<h4>3. Bakso Gongso</h4>
						
						<p>Kuliner viral di Solo selanjutnya ada bakso gongso. Jika biasanya penganan ini terbuat dari jerohan sapi maupun ayam, kali ini berbeda karena berbahan dasar bakso.</p>
						<p>Penjual bakso gongso di Solo mudah ditemukan, salah satunya di Jl Mr Sartono No. 30, Nusukan atau di depan SMAN 6 Solo.</p>
						<p><em><strong>Baca Juga: <a href="#"> Pakai Inhaler Saat Puasa, Bagaimana Hukumnya?</a></strong></em></p>
						<p>Dengan harga seporsi Rp5.000, kamu bakal dapat bakso gonso lengkap berisi bakso ayam, bakso sapi, tahu bakso dan juga telur. Selain itu, ada tambahan pangsit di atasnya.</p>
						
						<h4>4. Takoyaki Hiroshi</h4>
						
						<p>Beberapa waktu lalu sempat viral di Solo kuliner takoyaki yang dimasak langsung oleh orang Jepang langsung, yakni Hiroshi.</p>
						<p>Takoyaki buatan Hiroshi dijual seharga Rp10.000 per porsi dengan isi tujuh bulatan. Meski harganya murah, resep takoyaki itu asli dari Jepang. Isiannya adalah gurita, daun bawang, serta ikan. Sausnya juga diracik sendiri dengan resep otentik.</p>
						<p><em><strong>Baca Juga: <a href="#"> Tsunami Covid-19 India, Ahli: Bisa Jadi karena Euforia Vaksinasi</a></strong></em></p>
						<p>Selain takoyaki, ada juga ramen yang dijual seharga Rp12.00-Rp13.000 per porsi. Serta gyoza seharga Rp5.000 per porsi isi lima.</p>
						<p>Untuk lokasinya sendiri, warung kuliner viral ini berlokasi di dekat SPBU Pucangsawit, Jebres.</p>
						<p><em><strong>Baca Juga: <a href="#"> Yayasan Pendidikan Warga, Hadirkan Sekolah dengan Biaya Terjangkau dan Berkualitas di Solo</a></strong></em></p>
						<h4>5. Dimsum Pasar Gede</h4>
						
						<p>Kuliner viral di Solo terakhir adalah dimsum yang dijual di Pasar Gede bagian barat.</p>
						<p>Dimsum di sini terkenal murah sehingga ramai oleh pembeli. Untuk satu buah dimsum cuma dihargai Rp3.000 saja. Variannya pun beraneka ragam sehingga cocok untuk segala kalangan.</p>
						<p><em><strong>Baca Juga: <a href="#"> Kenangan Netizen di Terminal Kartasura Lama: Tempat Main Gim - Lokasi Bolos Sekolah</a></strong></em></p>
						<p>Tapi kalau ke sini harus sabar antre ya!</p></p>
	   
												
					</div><!-- post-content-area end -->
					<div style="margin: 20px 0;">
					   <a href="" data-toggle="modal" data-target="#ModalArtikel3" title="" target="_blank"><img src="{{ url('/images/iklan-img/desktop-artikel-3.png') }}"></a>
					</div>
					
					<div class="post-footer">							
						<div class="author-box d-flex">
							<div class="author-img">
								  <img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Profile">
							</div>
															   
							<div class="author-info">
								<h3><a href="#" target="_blank">Nugroho Meidinata</a></h3>									
								<p><p>Describe your mind with words.</p></p>
							<div class="post-list">
								<a href="#" target="_blank">Lihat Artikel Saya Lainnya</a>
							</div> 									
							<div class="author-social">
									<span>Follow Me: </span>
									<a href=""><i class="fa fa-twitter"></i></a>
									<a href=""><i class="fa fa-facebook"></i></a>
									<a href=""><i class="fa fa-instagram"></i></a>
									<a href="#"><i class="fa fa-google-plus"></i></a>
								</div>									
							</div>
						</div><!-- author box -->

						<div class="gap-30"></div>
						<div class="tag-lists">
							<span>Tags: </span>
							<a href="#">#Wisata</a> <a href="#">#Kuliner</a> <a href="#">#wisata solo</a> <a href="#">#kuliner solo</a>  
						</div><!-- tag lists -->

					</div>
					
				</div><!-- single-post end -->

				<div class="gap-50 d-none d-md-block"></div>

				<!-- realted post start -->
				<div class="related-post">
					<h2 class="block-title">
						<span class="title-angle-shap"> Berita Terkait </span>
					</h2>
					<div class="row">
												 

						<div class="col-md-4">
							<div class="post-block-style">
								<div class="post-thumb">
									<a class="post-title" href="#" title="Mudik Dilarang, Enaknya Ngapain Ya Pas Libur Lebaran?">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Mudik Dilarang, Enaknya Ngapain Ya Pas Libur Lebaran?" style="object-fit: cover; width: 195px; height: 128px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat lifestyle" href="#">lifestyle</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title">
										<a class="post-title" href="#" title="Mudik Dilarang, Enaknya Ngapain Ya Pas Libur Lebaran?">Mudik Dilarang, Enaknya Ngapain Ya Pas Libur Lebaran?</a>
									</h2>
									<div class="post-meta mb-7 p-0">
										
									</div>
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->	
					   

						<div class="col-md-4">
							<div class="post-block-style">
								<div class="post-thumb">
									<a class="post-title" href="#" title="4 Kuliner Khas Angkringan Solo, Bisa Jadi Menu Buka Puasa Hlo!">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="4 Kuliner Khas Angkringan Solo, Bisa Jadi Menu Buka Puasa Hlo!" style="object-fit: cover; width: 195px; height: 128px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat soloraya" href="#">soloraya</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title">
										<a class="post-title" href="#" title="4 Kuliner Khas Angkringan Solo, Bisa Jadi Menu Buka Puasa Hlo!">4 Kuliner Khas Angkringan Solo, Bisa Jadi Menu Buka Puasa Hlo!</a>
									</h2>
									<div class="post-meta mb-7 p-0">
										
									</div>
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->	
					   

						<div class="col-md-4">
							<div class="post-block-style">
								<div class="post-thumb">
									<a class="post-title" href="#" title="Ini Menu Sahur Bersejarah Soekarno-Hatta Saat Malam Penyusunan Teks Proklamasi">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ini Menu Sahur Bersejarah Soekarno-Hatta Saat Malam Penyusunan Teks Proklamasi" style="object-fit: cover; width: 195px; height: 128px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat lifestyle" href="#">lifestyle</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title">
										<a class="post-title" href="#" title="Ini Menu Sahur Bersejarah Soekarno-Hatta Saat Malam Penyusunan Teks Proklamasi">Ini Menu Sahur Bersejarah Soekarno-Hatta Saat Malam Penyusunan Teks Proklamasi</a>
									</h2>
									<div class="post-meta mb-7 p-0">
										
									</div>
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->	
					   

						<div class="col-md-4">
							<div class="post-block-style">
								<div class="post-thumb">
									<a class="post-title" href="#" title="4 Takjil Favorit Warga Soloraya, Mana Kesukaanmu?">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="4 Takjil Favorit Warga Soloraya, Mana Kesukaanmu?" style="object-fit: cover; width: 195px; height: 128px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat soloraya" href="#">soloraya</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title">
										<a class="post-title" href="#" title="4 Takjil Favorit Warga Soloraya, Mana Kesukaanmu?">4 Takjil Favorit Warga Soloraya, Mana Kesukaanmu?</a>
									</h2>
									<div class="post-meta mb-7 p-0">
										
									</div>
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->	
					   

						<div class="col-md-4">
							<div class="post-block-style">
								<div class="post-thumb">
									<a class="post-title" href="#" title="3 Pentol Lezat &#038; Ngehits di Sragen, Mana Favoritmu?">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="3 Pentol Lezat &#038; Ngehits di Sragen, Mana Favoritmu?" style="object-fit: cover; width: 195px; height: 128px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat soloraya" href="#">soloraya</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title">
										<a class="post-title" href="#" title="3 Pentol Lezat &#038; Ngehits di Sragen, Mana Favoritmu?">3 Pentol Lezat &#038; Ngehits di Sragen, Mana Favoritmu?</a>
									</h2>
									<div class="post-meta mb-7 p-0">
										
									</div>
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->	
					   

						<div class="col-md-4">
							<div class="post-block-style">
								<div class="post-thumb">
									<a class="post-title" href="#" title="Pentol Portugal Bakal Jadi Andalan di Kampung Wisata Pentol Sragen">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Pentol Portugal Bakal Jadi Andalan di Kampung Wisata Pentol Sragen" style="object-fit: cover; width: 195px; height: 128px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat soloraya" href="#">soloraya</a>
									</div>
								</div>
								
								<div class="post-content">
									<h2 class="post-title">
										<a class="post-title" href="#" title="Pentol Portugal Bakal Jadi Andalan di Kampung Wisata Pentol Sragen">Pentol Portugal Bakal Jadi Andalan di Kampung Wisata Pentol Sragen</a>
									</h2>
									<div class="post-meta mb-7 p-0">
										
									</div>
								</div><!-- Post content end -->
							</div>
						</div><!-- col end -->	
																											 
					</div><!-- row end -->
				</div>
				<!-- realted post end -->

				<div class="gap-50 d-none d-md-block"></div>

				<!-- Block Konten Premium -->
				<div class="block style2 text-light mb-20 mt-10">
					<h2 class="block-title">
						<span class="title-angle-shap"> Espos Premium</span>
					</h2>

					<div class="row">
																				
						<div class="col-lg-6 col-md-6">
							<div class="post-block-style">
								<div class="post-thumb">
									<a href="#" title="Klaster Piknik Berdampak Buruk ke Sektor Lain">
										<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Klaster Piknik Berdampak Buruk ke Sektor Lain" style="object-fit: cover; object-position: center; width: 266px; height: 178px;">
									</a>
									<div class="grid-cat">
										<a class="post-cat premium" href="#">Premium</a>
									</div>
								</div>
								
								<div class="post-content mt-3">
									<h2 class="post-title title-md">
										<a href="#" title="Klaster Piknik Berdampak Buruk ke Sektor Lain">Klaster Piknik Berdampak Buruk ke Sektor Lain</a>
									</h2>
									<p>Persebaran Covid-19 klaster piknik di Dusun Gondang, Desa Candi, Kecamatan Ampel, Kabupaten Boyolali berdampak ke warga lain yang berkontak erat dan juga berdampak pada sektor pendidikan.</p>
									<div class="post-meta mb-7">
										<span class="post-author"><a href="#"><i class="fa fa-user"></i>Bayu Jatmiko Adi</a></span>
										<span class="post-date"><i class="fa fa-clock-o"></i> 8 menit lalu</span>
									</div>
								</div><!-- Post content end -->
							</div><!-- Post block a end -->
						</div><!-- Col 1 end -->

						<div class="col-lg-6 col-md-6">
							<div class="row ts-gutter-20">								
																				
													
								<div class="col-md-6">
									<div class="post-block-style">
										<div class="post-thumb">
											<a href="#" title="PTM di Sekolah Swasta Ditunda, Kenapa?">
												<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="PTM di Sekolah Swasta Ditunda, Kenapa?" style="object-fit: cover; object-position: center; width: 118px; height: 84px;">
											</a>
										</div>
										
										<div class="post-content">
											<h2 class="post-title mb-2">
												<a href="#" title="PTM di Sekolah Swasta Ditunda, Kenapa?">PTM di Sekolah Swasta Ditunda, Kenapa?</a>
											</h2>
										</div><!-- Post content end -->
									</div><!-- Post block a end -->
								</div><!-- .col -->
							
													
													
								<div class="col-md-6">
									<div class="post-block-style">
										<div class="post-thumb">
											<a href="#" title="Persis Sedang Menimang-Nimang Sponsor Tunggal">
												<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Persis Sedang Menimang-Nimang Sponsor Tunggal" style="object-fit: cover; object-position: center; width: 118px; height: 84px;">
											</a>
										</div>
										
										<div class="post-content">
											<h2 class="post-title mb-2">
												<a href="./solopos_com_single" title="Persis Sedang Menimang-Nimang Sponsor Tunggal">Persis Sedang Menimang-Nimang Sponsor Tunggal</a>
											</h2>
										</div><!-- Post content end -->
									</div><!-- Post block a end -->
								</div><!-- .col -->
							
													
													
								<div class="col-md-6">
									<div class="post-block-style">
										<div class="post-thumb">
											<a href="#" title="Man City vs PSG: Hidup Mati di Etihad">
												<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Man City vs PSG: Hidup Mati di Etihad" style="object-fit: cover; object-position: center; width: 118px; height: 84px;">
											</a>
										</div>
										
										<div class="post-content">
											<h2 class="post-title mb-2">
												<a href="./solopos_com_single" title="Man City vs PSG: Hidup Mati di Etihad">Man City vs PSG: Hidup Mati di Etihad</a>
											</h2>
										</div><!-- Post content end -->
									</div><!-- Post block a end -->
								</div><!-- .col -->
							
													
													
								<div class="col-md-6">
									<div class="post-block-style">
										<div class="post-thumb">
											<a href="#" title="PDIP Masih Memimpin, Partai Ummat Menyalip PAN">
												<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="PDIP Masih Memimpin, Partai Ummat Menyalip PAN" style="object-fit: cover; object-position: center; width: 118px; height: 84px;">
											</a>
										</div>
										
										<div class="post-content">
											<h2 class="post-title mb-2">
												<a href="./solopos_com_single" title="PDIP Masih Memimpin, Partai Ummat Menyalip PAN">PDIP Masih Memimpin, Partai Ummat Menyalip PAN</a>
											</h2>
										</div><!-- Post content end -->
									</div><!-- Post block a end -->
								</div><!-- .col -->
							</div><!-- .row -->
						</div><!-- Col 2 end -->
											</div><!-- Row end -->
				</div><!-- Block Konten Premium end -->	
				

				<div class="gap-50 d-none d-md-block"></div>
				
				<div>
					<h2 class="block-title">
						<span class="title-angle-shap"> Berita Terkini </span>
					</h2>
					<div class="row ts-gutter-20 loadmore-frame">
					  
																																
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Klaster Piknik Berdampak Buruk ke Sektor Lain" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" title="Klaster Piknik Berdampak Buruk ke Sektor Lain">Klaster Piknik Berdampak Buruk ke Sektor Lain</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>Bayu Jatmiko Adi</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 8 menit lalu</span>
											</div>
											<p>Persebaran Covid-19 klaster piknik di Dusun Gondang, Desa Candi, Kecamatan Ampel, Kabupaten Boyolali berdampak ke warga lain yang berkontak erat dan juga berdampak pada sektor pendidikan.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
																			
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="PTM di Sekolah Swasta Ditunda, Kenapa?" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" title="PTM di Sekolah Swasta Ditunda, Kenapa?">PTM di Sekolah Swasta Ditunda, Kenapa?</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>R Bony Eko Wicaksono</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 14 menit lalu</span>
											</div>
											<p>Berbagai persyaratan uji coba PTM telah dipenuhi seperti infrastruktur protokol kesehatan, vaksinasi tenaga pengajar dan karyawan sekolah dan permintaan persetujuan dari orang tua/wali murid.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
																			
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Gubernur Jateng Tunjuk Tatag Prabawanto Jadi Plh Bupati Sragen" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" title="Gubernur Jateng Tunjuk Tatag Prabawanto Jadi Plh Bupati Sragen">Gubernur Jateng Tunjuk Tatag Prabawanto Jadi Plh Bupati Sragen</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>Tri Rahayu</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 34 menit lalu</span>
											</div>
											<p>Serah terima jabatan dilaksanakan dengan penandatanganan berita acara dan penyerahan buku memori dari Bupati dan Wabup kepada Tatag Prabawanto.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
																			
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Ibu di Wonogiri Ditemukan Meninggal di Dalam Sumur" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" title="Ibu di Wonogiri Ditemukan Meninggal di Dalam Sumur">Ibu di Wonogiri Ditemukan Meninggal di Dalam Sumur</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>Aris Munandar</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 1 jam lalu</span>
											</div>
											<p>Hasil pemeriksaan luar tidak ditemukan adanya tanda-tanda penganiayaan dan keluarga menerima kejadian itu sebagai musibah.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
																			
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Bupati Karanganyar Perbolehkan Salat Id di Lapangan dan Masjid, Tapi Ada Syaratnya" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" title="Bupati Karanganyar Perbolehkan Salat Id di Lapangan dan Masjid, Tapi Ada Syaratnya">Bupati Karanganyar Perbolehkan Salat Id di Lapangan dan Masjid, Tapi Ada Syaratnya</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>Candra Mantovani</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 1 jam lalu</span>
											</div>
											<p>Keputusan terkait Salat Id di Karanganyar mengacu SE Menteri Agama nomor 3/2021 tentang panduan ibadah Ramadan dan Idul Fitri 1442 Hijriyah.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
																			
						<div class="col-12 mb-10 content-box">
							<div class="post-block-style">
								<div class="row">
									<div class="col-md-5">
										<div class="post-thumb post-list_feed">
											<img src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lebaran Kian Dekat, Gibran Imbau Warga Solo Tahan Diri Jangan Mudik" style="object-fit: cover; object-position: center; height: 167px; width: 250px;">
											<a class="post-cat-box soloraya" href="#">soloraya</a>
										</div>
									</div>
									<div class="col-md-7 pl-0">
										<div class="post-content">
											<h2 class="post-title title-md">
											<a href="" title="Lebaran Kian Dekat, Gibran Imbau Warga Solo Tahan Diri Jangan Mudik">Lebaran Kian Dekat, Gibran Imbau Warga Solo Tahan Diri Jangan Mudik</a>
											</h2>
											<div class="post-meta mb-7">
												<span class="post-author"><a href="#"><i class="fa fa-user"></i>Wahyu Prakoso</a></span>
												<span class="post-date"><i class="fa fa-clock-o"></i> 3 jam lalu</span>
											</div>
											<p>Pemkot mengantisipasi lonjakan konsumen di pusat perbelanjaan dan tempat pariwisata dan memberikan ancaman sanksi bagi pelanggar protokol kesehatan.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
																			
						<div class="col-12 mt-3 align-items-center" style="text-align: center;">
							<a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Cek Berita Lainnya</a>
							<a href="#" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
						</div><!-- col end -->
					</div>	
				</div><!-- Content Col end -->					
			</div><!-- col-lg-8 -->

				
				
				<!-- sidebar start -->
                <div class="col-lg-4">
                    <div class="sidebar">
                        <div class="sidebar-widget ads-widget">
                            <div class="ads-image">
                            
                            <div align="center">
                            <a href="" data-toggle="modal" data-target="#ModalSidebar1">
                            <img class="img-fluid" src="{{ url('images/iklan-img/desktop-sidebar-1.png') }}" >
                            </a>
                            </div>
                
                                 
                            </div>
                        </div><!-- widget end -->						
                        <div class="sidebar-widget featured-tab post-tab"><!-- style="position: -webkit-sticky; position: sticky; top: 130px;" -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link animated active fadeIn" href="#post_tab_b" data-toggle="tab">
                                        <span class="tab-head">
                                            <span class="tab-text-title">Terpopuler</span>					
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="gap-50 d-none d-md-block"></div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="tab-content">
                                        <div class="tab-pane active animated fadeInRight" id="post_tab_b">
                                            <div class="list-post-block">
                                                <ul class="list-post">
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 22 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 17 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 20 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 22 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                    <li>
                                                        <div class="post-block-style media">
                                                            <div class="post-thumb">
                                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                                </a>
                                                            </div><!-- Post thumb end -->
                        
                                                            <div class="post-content media-body">
                                                                <h2 class="post-title">
                                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                                </h2>
                                                                <div class="post-meta mb-7">
                                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 23 jam lalu</span>
                                                                </div>
                                                            </div><!-- Post content end -->
                                                        </div><!-- Post block style end -->
                                                    </li><!-- Li 1 end -->
                                                       
                                                </ul><!-- List post end -->
                                            </div>
                                        </div><!-- Tab pane 2 end -->
                                    </div><!-- tab content -->
                                </div>
                            </div>
                        </div><!-- widget end -->	
                                    
                            
                        <div class="sidebar-widget ads-widget">
                            <div class="ads-image">
                            
                            <div align="center">
                            <a href="#" data-toggle="modal" data-target="#ModalSidebar2">
                            <img class="img-fluid" src="{{ url('images/iklan-img/desktop-sidebar-2.png') }}" style="object-fit: cover; object-position: center; height: 250px; width: 300px;">
                            </a>
                            </div>
                  
                            </div>
                        </div><!-- widget end -->	
                        <div class="sidebar-widget">
                            <h2 class="block-title block-title-dark">
                                <span class="title-angle-shap"> News </span>
                            </h2>
                            <div class="list-post-block">
                                <ul class="list-post">
                                                                          
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 3 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->	          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 4 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->	          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 5 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->	          				
                                </ul><!-- List post end -->
                            </div>
                        </div>
                
                        <div class="sidebar-widget ads-widget">
                            <div class="ads-image">
                                <div align="center">
                                <a href="#" data-toggle="modal" data-target="#ModalSidebar3">
                                <img class="img-fluid" src="{{ url('images/iklan-img/desktop-sidebar-3.png') }}" style="object-fit: cover; object-position: center; height: 250px; width: 300px;">
                                </a>
                                </div>
                            </div>
                        </div><!-- widget end -->	
                
                        <div class="sidebar-widget">
                            <h2 class="block-title block-title-dark">
                                <span class="title-angle-shap"> Lifestyle </span>
                            </h2>
                            <div class="list-post-block">
                                <ul class="list-post">
                                                                          
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 3 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->			          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 5 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->		          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 15 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->			          				
                                </ul><!-- List post end -->
                            </div>
                        </div>
                        
                        <div class="sidebar-widget">
                            <h2 class="block-title block-title-dark">
                                <span class="title-angle-shap"> Ekbis </span>
                            </h2>
                            <div class="list-post-block">
                                <ul class="list-post">
                                                                          
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 4 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->			          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 20 jam lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->		          				
                                        
                                    <li>
                                        <div class="post-block-style media">
                                            <div class="post-thumb">
                                                <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">
                                                    <img class="img-fluid" src="{{ url('images/iklan-img/post-thumb.png') }}" alt="Lorem ipsum dolor sit amet" style="object-fit: cover; object-position: center; height: 85px; width: 85px;">
                                                </a>
                                            </div><!-- Post thumb end -->
                
                                            <div class="post-content media-body">
                                                <h2 class="post-title">
                                                    <a href="{{ url('mediakit/single') }}" title="Lorem ipsum dolor sit amet">Lorem ipsum dolor sit amet</a>
                                                </h2>
                                                <div class="post-meta mb-7">
                                                    <span class="post-date"><i class="fa fa-clock-o"></i> 1 hari lalu</span>
                                                </div>
                                            </div><!-- Post content end -->
                                        </div><!-- Post block style end -->
                                    </li><!-- Li 1 end -->		          				
                                </ul><!-- List post end -->
                            </div>
                        </div>
                
                    </div>					
                </div><!-- Sidebar Col end -->
				<!-- Sidebar Col end -->
			</div><!-- Row end -->
		</div><!-- Container end -->
	</section><!-- Section Terkini End -->


<!-- Footer start -->
<div class="ts-footer">
    <div class="container">
        <div class="row ts-gutter-30 justify-content-lg-between justify-content-center">
            <div class="col-lg-7 col-md-7">
                <div class="footer-widget">
                    <div class="footer-logo">
                        <img src="{{ url('images/logo.png') }}" alt="Footer Logo">
                    </div>
                    <div class="widget-content">
                        <ul class="ts-footer-nav">
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">KONTAK</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">COPYRIGHT</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">WRITING CONTEST</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">REDAKSI</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">TENTANG KAMI</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">VIDEO</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">KARIR</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">KODE ETIK</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">CEK FAKTA</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">PRIVACY POLICY</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">PEDOMAN MEDIA SIBER</a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- col end -->
            <div class="col-lg-5 col-md-5">
                <div class="footer-widtet post-widget">
                    <h3 class="widget-title"><span>Jaringan</span></h3>
                    <div class="widget-content">

                        <ul class="ts-footer-nav">
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">BISNIS.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">RUMAH190.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">GRIYA190.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">IKLAN</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">SEMARANGPOS.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">MADIUNPOS.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">JEDA.ID</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">HARIANJOGJA.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">PASANGBALIHO.COM</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">TOKO</a></li>
                            <li class="ts-footer-nav-item"><a class="ts-footer-nav-item-link" href="#">IBUKOTAKITA.COM</a></li>
                        </ul>							

                    </div>
                </div>
            </div><!-- col end -->
        </div><!-- row end -->
    </div><!-- container end -->
</div>
<!-- Footer End-->

<!-- ts-copyright start -->
<div class="ts-copyright">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 text-center">
                <div class="copyright">
                    <p>Copyright &copy; 2007-2021, Solopos Digital Media - Panduan Informasi & Inspirasi. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ts-copyright end-->

<!-- backto -->
<div class="top-up-btn">
    <div class="backto" style="display: block;"> 
        <a href="#" class="icon icon-arrow-up" aria-hidden="true"></a>
    </div>
</div>
<!-- backto end-->

<!-- Javascript Files
================================================== -->

<!-- initialize jQuery Library -->
<script src="{{ asset('js/jquery.js') }}"></script>
<!-- Popper Jquery -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootstrap jQuery -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- magnific-popup -->
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<!-- Color box -->
<script src="{{ asset('js/jquery.colorbox.js') }}"></script>
<!-- Template custom -->
<script src="{{ asset('js/custom.js') }}"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script>
function showHide(d)
{
var onediv = document.getElementById(d);
var divs=['div-small','div-big'];
for (var i=0;i<divs.length;i++)
  {
  if (onediv != document.getElementById(divs[i]))
    {
    document.getElementById(divs[i]).style.display='none';
    }
  }
onediv.style.display = 'block';
} 

</script>


<!-- Modal Sidebar 1-->
<div class="modal fade" id="ModalSidebar1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Sidebar 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>Seluruh Halaman Desktop (Kecuali Halaman Premium) <br><br>
          Rekomendasi Ukuran : 300 x 250 pixel, 300 x 300 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Sidebar 2-->
  <div class="modal fade" id="ModalSidebar2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Sidebar 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>Seluruh Halaman Desktop (Kecuali Halaman Premium) <br><br>
          Rekomendasi Ukuran : 300 x 250 pixel, 300 x 300 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Sidebar 3-->
  <div class="modal fade" id="ModalSidebar3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Sidebar 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>Seluruh Halaman Desktop (Kecuali Halaman Premium) <br><br>
          Rekomendasi Ukuran : 300 x 250 pixel, 300 x 300 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  
  <!-- Modal Widget-->
  <div class="modal fade" id="ModalWidget" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Widget Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Iklan dalam bentuk berita 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Advetorial-->
  <div class="modal fade" id="ModalAdvetorial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Advetorial</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Iklan berita yang disisipkan selalu tampil di halaman awal (HOMEPAGE) selama renntang waktu tertentu.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Leaderboard 1-->
  <div class="modal fade" id="ModalLB1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Leaderboard 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Homepage<br>
          <b>Rekomendasi Ukuran : </b>728 x 90 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Leaderboard 2-->
  <div class="modal fade" id="ModalLB2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Leaderboard 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Homepage<br>
          <b>Rekomendasi Ukuran : </b>728 x 90 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Artikel 1-->
  <div class="modal fade" id="ModalArtikel1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Artikel 1 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Artikel/Single Desktop Setelah Paragraf ke-5<br>
          <b>Rekomendasi Ukuran : </b>640 x 100 pixel, 640 x 150 pixel, 640 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Artikel 2-->
  <div class="modal fade" id="ModalArtikel2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Artikel 2 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Artikel/Single Desktop Setelah Paragraf ke-9<br>
          <b>Rekomendasi Ukuran : </b>640 x 100 pixel, 640 x 150 pixel, 640 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Artikel 3-->
  <div class="modal fade" id="ModalArtikel3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Desktop Artikel 3 Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Halaman Artikel/Single Desktop di akhir isi Konten/Artikel<br>
          <b>Rekomendasi Ukuran : </b>640 x 100 pixel, 640 x 150 pixel, 640 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Modal Wrapt Side Kanan Kiri-->
  <div class="modal fade" id="ModalWraptSide" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Wrapt Side Left-Right Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Seluruh Halaman Desktop Sebelah Kanan & Kiri (Desain Bisa sama ataupun berbeda di kanan & kiri)<br>
          <b>Rekomendasi Ukuran : </b>250 x 750 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal Modal Wrapt Top-->
  <div class="modal fade" id="ModalWraptTop" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLongTitle">Wrapt Top Banner</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <b>Penempatan : </b><br>
          Seluruh Halaman Desktop Paling Atas (Diatas Logo/Navigasi)<br>
          <b>Rekomendasi Ukuran : </b>996 x 200 pixel<br><br>
          File Format: JPG, PNG, GIF (animated or static)<br>
          <br>
          File Size: < 100KB<br>
        </div>
      </div>
    </div>
  </div>
  
</body>
</html>
