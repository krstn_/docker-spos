@extends('layouts.app-solorayakeren')
@section('content')		      
      <!--=====================================-->
      <!--=      Hero Section Area Start      =-->
      <!--=====================================-->
      <section class="hero-wrap-layout1" data-bg-image="https://cdn.solopos.com/tematik/image/main-cover.jpg">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-9">

            </div>
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=       Hero Section Area End       =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=   Countdown Section Area Start   =-->
      <!--=====================================-->
      {{-- <section class="countdown-wrap-layout1 wow fadeInUp animated" data-wow-delay="1.7s" data-wow-duration="1s">
        <div class="countdown-inner-wrap">
          <div class="countdown-container">
            <div class="countdown-heading">
              <div class="sub-title">Tanggal</div>
              <h2 class="title">Hitung mundur...</h2>
            </div>
            <div class="countdown-content">
              <div class="countdown"></div>
            </div>
          </div>
        </div>
      </section> --}}
      <!--=====================================-->
      <!--=    Countdown Section Area End    =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=      About Section Area Start     =-->
      <!--=====================================-->
      {{-- <section class="about-wrap-layout1">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 order-lg-2">
              <div class="about-box-layout1 content-box">
                <div>
                  <div class="sub-title">REMBUG SOLORAYA KEREN</div>
                  <h2 class="title">Diskusi Kerja Pengembangan Terbaik Soloraya 2022...</h2>
                  <p class="sub-description">Kami juga dapat bekerja dengan Anda untuk paket konten yang dipesan lebih dahulu dalam skala global.</p>
                  <p class="description">Lorem ipsum dolor sit amet consectetur adipiscing elit. Congue nisi the faucibus cursus lectus condimentum molestie sit nec. Nibh eturna dui as massa odio quis molestie amet dignissim. Nec vehicula tortor acnibh is ultricies bibendum more text.</p>
                  <p class="description">Lorem ipsum dolor sit amet consectetur adipiscing elit. Congue nisi the faucibus cursus lectus condimentum molestie sit nec. Nibh eturna dui as massa odio quis molestie amet dignissim. Nec vehicula tortor acnibh is ultricies bibendum more text.</p>
                  <p class="description">Nibh et urna dui as massa odio quis molestie amet dignissim. vehicula tortor ac nibh is ultricies bibendum</p>
                  <a href="about.html" class="btn-fill style-one mt-4">SELENGKAPNYA</a>
                </div>
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="about-box-layout1">
                <div class="figure-wrap">
                  <div class="main-figure wow fadeInLeft animated" data-wow-delay="0.1s" data-wow-duration="1s">
                    <img src="https://cdn.solopos.com/tematik/image/about/1.jpg" alt="About" width="729" height="1050">
                  </div>
                  <div class="sub-figure">
                    <ul>
                      <li class="wow zoomIn animated" data-wow-delay="0.3s" data-wow-duration="0.4s"><img src="https://cdn.solopos.com/tematik/image/about/2.jpg" alt="About" width="106" height="106">
                      </li>
                      <li class="wow zoomIn animated" data-wow-delay="0.5s" data-wow-duration="0.4s"><img src="https://cdn.solopos.com/tematik/image/about/3.jpg" alt="About" width="157" height="157">
                      </li>
                      <li class="wow zoomIn animated" data-wow-delay="0.7s" data-wow-duration="0.4s"><img src="https://cdn.solopos.com/tematik/image/about/4.jpg" alt="About" width="218" height="218">
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> --}}
      <!--=====================================-->
      <!--=       About Section Area End      =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=     Process Section Area Start    =-->
      <!--=====================================-->
      <section class="process-wrap-layout1" data-bg-image="https://cdn.solopos.com/tematik/image/banner/banner2.jpg">
        <div class="process-inner-wrap">
          <div class="container-fluid">
            <div class="process-heading-content">
              <a href="https://www.youtube.com/watch?v=xE39TcgNnXs" class="play-btn play-btn-primary">
                <i class="fas fa-play"></i>
              </a>
              <a href="https://www.youtube.com/watch?v=xE39TcgNnXs" class="video-title">Intro Video</a>
              <h2 class="title">Soloraya untuk Indonesia Maju</h2>
              <p class="description">“Recover Together, Recover Stronger”</p>
              <a href="#" class="btn-fill style-one mt-4">SELENGKAPNYA</a>
            </div>
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=     Process Section Area End      =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=    Speaker Section Area Start     =-->
      <!--=====================================-->
      <section class="speaker-wrap-layout3" id="pembicara">
        <div class="container">
          <div class="section-heading style-four">
            <h2 class="title">Nara<span>sumber</span></h2>
          </div>
          <div class="row g-0 child-items-wrap">
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/wimboh-santoso.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Wimboh Santoso</a></h3>
                  <div class="sub-title">Ketua Dewan Komisaris OJK</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/haryadi-sukamdani.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Hariyadi Sukamdani</a></h3>
                  <div class="sub-title">Ketua Apindo</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.7s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/royke-tumilaar.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Royke Tumilaar</a></h3>
                  <div class="sub-title">Dirut BNI</div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/mirza-adityaswara.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Mirza Adityaswara</a></h3>
                  <div class="sub-title">Ketua Indonesia Fintech Society (IFSoc)</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/ganjar-pranowo.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Ganjar Pranowo</a></h3>
                  <div class="sub-title">Gubernur Jawa Tengah</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.7s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/karaniya-dharmasaputra.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Karaniya Dharmasaputra</a></h3>
                  <div class="sub-title">Presiden Direktur OVO</div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/gibran-rakabuming.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Gibran Rakabuming Raka</a></h3>
                  <div class="sub-title">Walikota Solo</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/yuni-sukowati.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Yuni Sukowati</a></h3>
                  <div class="sub-title">Bupati Sragen</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.7s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/sri-mulyani.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Sri Mulyani</a></h3>
                  <div class="sub-title">Bupati Klaten</div>
                </div>
              </div>
            </div>  

            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/etik-suryani.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Etik Suryani</a></h3>
                  <div class="sub-title">Bupati Sukoharjo</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/said-hidayat.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Said Hidayat</a></h3>
                  <div class="sub-title">Bupati Boyolali</div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.7s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/joko-sutopo.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Joko Sutopo</a></h3>
                  <div class="sub-title">Bupati Wonogiri</div>
                </div>
              </div>
            </div>     
            <div class="col-xl-3 col-md-6 col-6 wow fadeInUp animated" data-wow-delay="0.7s" data-wow-duration="1s">
              <div class="speaker-box-layout3 animated-bg-wrap">
                <span class="animated-bg"></span>
                <div class="figure-box">
                  <a href="#"><img src="https://cdn.solopos.com/tematik/image/avatar/juliyatmono.png" alt="Speaker" width="267" height="267"></a>
                </div>
                <div class="content-box">
                  <h3 class="title"><a href="#">Juliyatmono</a></h3>
                  <div class="sub-title">Bupati Karanganyar</div>
                </div>
              </div>
            </div>                              
          </div>

        </div>
      </section>
      <section class="schedule-wrap-layout3" id="event">
        <div class="container">
          <div class="section-heading style-five">
            <div class="sub-title">UPDATE</div>
            <h2 class="title">INFORMASI <span>BERITA</span></h2>
          </div>          
        </div>
      </section>      
      <!--=====================================-->
      <!--=     Speaker Section Area End      =-->
      <!--=====================================-->      
      <!--=====================================-->
      <!--=      Blog Section Area Start      =-->
      <!--=====================================-->
      <section class="blog-wrap-layout1" id="news">
        <div class="container">
          <div class="row">
            @php
	            $loop_no = 1;
	          @endphp
            @foreach ($keren as $posts)
						@php           
						$thumb = $posts['featured_image']['thumbnail'] ?? 'https://www.solopos.com/images/no-thumb.jpg'; 
            $medium = $posts['featured_image']['medium'] ?? 'https://www.solopos.com/images/no-medium.jpg';
						$title = html_entity_decode($posts['title']);
					  @endphp
            @if($loop_no<=6)  
            <div class="col-sm-12 col-lg-4 wow fadeInUp animated" data-wow-delay="0.1s" data-wow-duration="1s">
              <div class="blog-box-layout1">
                <div class="figure-box figure-top">
                  <a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" class="link-wrap"><img src="{{ $medium }}" alt="blog" width="728" height="480"></a>
                  <div class="entry-date-wrap">
                    <svg width="90" height="68" viewBox="0 0 90 68" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M90 68C90 67.888 90 67.7573 90 67.6266C90 67.4959 90 67.3839 90 67.2718L90 34.5228L90 33.4585L90 0.728172C90 0.616145 90 0.485448 90 0.373421C90 0.242724 90 0.112026 90 0C89.0048 0.504119 88.1786 0.858868 87.6153 1.08292C83.5781 2.68863 80.1231 3.04338 78.4707 3.02471C77.7571 3.02471 77.0624 3.02471 76.3488 3.02471L0.131431 3.02471C3.66158 6.53487 5.91487 10.0264 9.42625 13.5365L-1.0569e-06 24.179L8.61881 34L-1.91548e-06 43.821L9.42624 54.4635C5.89609 57.9736 3.6428 61.4651 0.131429 64.9753L76.3676 64.9753C77.0812 64.9753 77.7759 64.9753 78.4895 64.9753C80.1419 64.9753 83.5969 65.3114 87.634 66.9171C88.1974 67.1411 89.0236 67.4959 90 68Z" fill="#EE0034" />
                    </svg>
                    <div class="entry-date">{{ Carbon\Carbon::parse($posts['date'])->translatedFormat('j') }}<span>{{ Carbon\Carbon::parse($posts['date'])->translatedFormat('M') }}</span></div>
                  </div>
                </div>
                <div class="content-box">
                  <div class="entry-meta mb-1">
                    <ul>
                      <li><i class="far fa-user"></i> {{ $posts['author'] }}</li>
                      <li><i class="fas fa-tags"></i> {{ $posts['catsname'] }}</li>
                    </ul>
                  </div>
                  <h3 class="entry-title" style="overflow: hidden; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;"><a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}">{{ $title }}</a></h3>
                  <p class="entry-description" style="overflow: hidden; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{{ html_entity_decode($posts['summary']) }}</p>
                  {{--<a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" class="btn-text style-one">READ MORE</a>--}}
                </div>
              </div>
            </div>      
                        
            @endif
            @php $loop_no++; @endphp
					  @endforeach
            {{--
              <div class="col-sm-12 col-lg-4 wow fadeInUp animated" data-wow-delay="0.1s" data-wow-duration="1s">
              <div class="blog-box-layout1">
                <div class="figure-box figure-top">
                  <a href="blog-single.html" class="link-wrap"><img src="https://cdn.solopos.com/tematik/image/blog/blog-figure-1.jpg" alt="blog" width="728" height="480"></a>
                  <div class="entry-date-wrap">
                    <svg width="90" height="68" viewBox="0 0 90 68" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M90 68C90 67.888 90 67.7573 90 67.6266C90 67.4959 90 67.3839 90 67.2718L90 34.5228L90 33.4585L90 0.728172C90 0.616145 90 0.485448 90 0.373421C90 0.242724 90 0.112026 90 0C89.0048 0.504119 88.1786 0.858868 87.6153 1.08292C83.5781 2.68863 80.1231 3.04338 78.4707 3.02471C77.7571 3.02471 77.0624 3.02471 76.3488 3.02471L0.131431 3.02471C3.66158 6.53487 5.91487 10.0264 9.42625 13.5365L-1.0569e-06 24.179L8.61881 34L-1.91548e-06 43.821L9.42624 54.4635C5.89609 57.9736 3.6428 61.4651 0.131429 64.9753L76.3676 64.9753C77.0812 64.9753 77.7759 64.9753 78.4895 64.9753C80.1419 64.9753 83.5969 65.3114 87.634 66.9171C88.1974 67.1411 89.0236 67.4959 90 68Z" fill="#EE0034" />
                    </svg>
                    <div class="entry-date">16<span>June</span></div>
                  </div>
                </div>
                <div class="content-box">
                  <div class="entry-meta mb-1">
                    <ul>
                      <li><a href="blog-single.html"><i class="far fa-user"></i>Jenny Wilson</a></li>
                      <li><a href="blog-single.html"><i class="fas fa-tags"></i>Technology</a></li>
                    </ul>
                  </div>
                  <h3 class="entry-title"><a href="blog-single.html">Chances are Good that there’s a Cloud
                      Software..</a></h3>
                  <p class="entry-description">Lorem ipsum dolor sit amet consectetur is purus
                    amet luctus venenatis.</p>
                  <a href="blog-single.html" class="btn-text style-one">READ MORE</a>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-lg-4 wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
              <div class="blog-box-layout1 d-flex d-lg-block flex-column-reverse">
                <div class="content-box">
                  <div class="entry-meta mb-1">
                    <ul>
                      <li><a href="blog-single.html"><i class="far fa-user"></i>Kristin Watson</a></li>
                      <li><a href="blog-single.html"><i class="fas fa-tags"></i>Technology</a></li>
                    </ul>
                  </div>
                  <h3 class="entry-title"><a href="blog-single.html">Many of those Products Offer the Potential
                      not just to</a></h3>
                  <p class="entry-description">Lorem ipsum dolor sit amet consectetur is purus
                    amet luctus venenatis.</p>
                  <a href="blog-single.html" class="btn-text style-one">READ MORE</a>
                </div>
                <div class="figure-box figure-bottom">
                  <a href="blog-single.html" class="link-wrap"><img src="https://cdn.solopos.com/tematik/image/blog/blog-figure-2.jpg" alt="blog" width="728" height="480"></a>
                  <div class="entry-date-wrap">
                    <svg width="90" height="68" viewBox="0 0 90 68" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M90 68C90 67.888 90 67.7573 90 67.6266C90 67.4959 90 67.3839 90 67.2718L90 34.5228L90 33.4585L90 0.728172C90 0.616145 90 0.485448 90 0.373421C90 0.242724 90 0.112026 90 0C89.0048 0.504119 88.1786 0.858868 87.6153 1.08292C83.5781 2.68863 80.1231 3.04338 78.4707 3.02471C77.7571 3.02471 77.0624 3.02471 76.3488 3.02471L0.131431 3.02471C3.66158 6.53487 5.91487 10.0264 9.42625 13.5365L-1.0569e-06 24.179L8.61881 34L-1.91548e-06 43.821L9.42624 54.4635C5.89609 57.9736 3.6428 61.4651 0.131429 64.9753L76.3676 64.9753C77.0812 64.9753 77.7759 64.9753 78.4895 64.9753C80.1419 64.9753 83.5969 65.3114 87.634 66.9171C88.1974 67.1411 89.0236 67.4959 90 68Z" fill="#EE0034" />
                    </svg>
                    <div class="entry-date">23<span>June</span></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-lg-4 wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">
              <div class="blog-box-layout1">
                <div class="figure-box figure-top">
                  <a href="blog-single.html" class="link-wrap"><img src="https://cdn.solopos.com/tematik/image/blog/blog-figure-3.jpg" alt="blog" width="728" height="480"></a>
                  <div class="entry-date-wrap">
                    <svg width="90" height="68" viewBox="0 0 90 68" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M90 68C90 67.888 90 67.7573 90 67.6266C90 67.4959 90 67.3839 90 67.2718L90 34.5228L90 33.4585L90 0.728172C90 0.616145 90 0.485448 90 0.373421C90 0.242724 90 0.112026 90 0C89.0048 0.504119 88.1786 0.858868 87.6153 1.08292C83.5781 2.68863 80.1231 3.04338 78.4707 3.02471C77.7571 3.02471 77.0624 3.02471 76.3488 3.02471L0.131431 3.02471C3.66158 6.53487 5.91487 10.0264 9.42625 13.5365L-1.0569e-06 24.179L8.61881 34L-1.91548e-06 43.821L9.42624 54.4635C5.89609 57.9736 3.6428 61.4651 0.131429 64.9753L76.3676 64.9753C77.0812 64.9753 77.7759 64.9753 78.4895 64.9753C80.1419 64.9753 83.5969 65.3114 87.634 66.9171C88.1974 67.1411 89.0236 67.4959 90 68Z" fill="#EE0034" />
                    </svg>
                    <div class="entry-date">29<span>June</span></div>
                  </div>
                </div>
                <div class="content-box">
                  <div class="entry-meta mb-1">
                    <ul>
                      <li><a href="blog-single.html"><i class="far fa-user"></i>Devon Lane</a></li>
                      <li><a href="blog-single.html"><i class="fas fa-tags"></i>Technology</a></li>
                    </ul>
                  </div>
                  <h3 class="entry-title"><a href="blog-single.html">Our Evaluation Services Focus on Providing
                      Actionable</a></h3>
                  <p class="entry-description">Lorem ipsum dolor sit amet consectetur is purus
                    amet luctus venenatis.</p>
                  <a href="blog-single.html" class="btn-text style-one">READ MORE</a>
                </div>
              </div>
            </div>--}}
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=       Blog Section Area End       =-->
      <!--=====================================-->      
      <!--=====================================-->
      <!--=    Schedule Section Area Start    =-->
      <!--=====================================-->
      <section class="schedule-wrap-layout3" id="event">
        <div class="container">
          <div class="section-heading style-five">
            <h2 class="title">Rundown <span>Acara</span></h2>
          </div>
          <div class="row justify-content-center">
            <div class="col-lg-10">
              <div class="schedule-slider-main-wrap">
                <div class="schedule-slider-thumbnail-style-1 swiper-container schedule-box-layout3 schedule-nav">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      Rundown
                    </div>
                  </div>
                </div>
                <span class="slider-btn slider-btn-prev">
                    <i class="fas fa-chevron-left"></i>
                </span>
                <span class="slider-btn slider-btn-next">
                    <i class="fas fa-chevron-right"></i>
                </span>
              </div>
              <div class="shcedule-slider-style-1 swiper-container schedule-box-layout3 schedule-content">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <div class="panel-group" id="accordionExample">
                      <div class="panel panel-default wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">
                        <div class="panel-heading" id="headingOne">
                          <div class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" role="button">
                            <div class="date-time-wrap">
                              <div class="date">23</div>
                              <div>
                                <div class="month">Februari</div>
                                <div class="time">08:00 - 12:30</div>
                              </div>
                            </div>
                            <div class="content-box-wrap">
                              <div class="figure-box">
                                <img src="https://cdn.solopos.com/misc/avatar.png" alt="Speaker" width="60" height="60">
                              </div>
                              <div class="inner-box">
                                <h3 class="title">SARASEHAN SOLORAYA UNTUK INDONESIA MAJU</h3>
                                <div class="sub-title">By <span>SOLOPOS</span> Media Group
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                          <div class="panel-body">
                            <p class="description">SARASEHAN SOLORAYA UNTUK INDONESIA MAJU, “Recover Together, Recover Stronger”</p>
                            <div class="address-wrap">
                              <div class="icon-box">
                                <svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M6.04688 17.8984C6.36328 18.3906 7.10156 18.3906 7.41797 17.8984C12.5508 10.5156 13.5 9.74219 13.5 7C13.5 3.27344 10.4766 0.25 6.75 0.25C2.98828 0.25 0 3.27344 0 7C0 9.74219 0.914062 10.5156 6.04688 17.8984ZM6.75 9.8125C5.16797 9.8125 3.9375 8.58203 3.9375 7C3.9375 5.45312 5.16797 4.1875 6.75 4.1875C8.29688 4.1875 9.5625 5.45312 9.5625 7C9.5625 8.58203 8.29688 9.8125 6.75 9.8125Z" />
                                </svg>
                              </div>
                              <div class="address-text">Hotel Alila Solo</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-center mt-4 wow fadeInUp animated" data-wow-delay="1.1s" data-wow-duration="1s">
                <a href="https://cdn.solopos.com/tematik/rundown.pdf" class="btn-fill style-four">DOWNLOAD RUNDOWN</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=     Schedule Section Area End     =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--=    Location Section Area Start    =-->
      <!--=====================================-->
      <section class="location-wrap-layout1">
        <div class="container">
          <div class="section-heading style-two">
            <div class="sub-title">LOKASI</div>
            <h2 class="title">Peta Lokasi <span>Acara</span></h2>
          </div>
        </div>        
        <div class="container">
          <div class="location-box-layout1 has-animation">
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3955.145177803517!2d110.78615900967772!3d-7.559145263586463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a143836b69543%3A0x5f9e70ee23da6ca4!2sAlila%20Solo!5e0!3m2!1sid!2sid!4v1645181894111!5m2!1sid!2sid" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </iframe>
            <div class="map-content">
              <div class="sub-title">Pastikan Lokasinya</div>
              <h2 class="title">Arahan ke Lokasi</h2>
              <div class="list-info">
                <ul>
                  <li>Tempat
                    <p>Alila Solo</p>
                  </li>
                  <li>Alamat
                    <p>Jl. Slamet Riyadi No.562, Jajar, Kec. Laweyan, Kota Surakarta, Jawa Tengah 57144</p>
                  </li>
                  <li><a href="#">Get Direction<i class="fas fa-arrow-right"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--=====================================-->
      <!--=     Location Section Area End     =-->
      <!--=====================================-->
      <!--=====================================-->
      <!--= Call To Action Section Area Start =-->
      <!--=====================================-->
      {{-- <section class="call-to-action-wrap-layout1 bg-img" data-bg-image="https://cdn.solopos.com/tematik/image/banner/banner1.jpg">
        <div class="container">
          <div class="call-to-action-box-layout1">
            <div class="sub-title wow fadeInUp animated" data-wow-delay="0.3s" data-wow-duration="1s">Let’s Do it Hurry
            </div>
            <h2 class="title wow fadeInUp animated" data-wow-delay="0.5s" data-wow-duration="1s">Haven't booked your
              seat yet? Get ticket now</h2>
            <a href="pricing.html" class="btn-fill style-four wow fadeInUp animated" data-wow-delay="0.7s" data-wow-duration="1s">BUY TICKETS</a>
          </div>
        </div>
      </section> --}}
      <!--=====================================-->
      <!--=  Call To Action Section Area End  =-->
      <!--=====================================-->
@endsection 