@extends('layouts.app')
@section('content')

@include('includes.ads.popup-banner')
@include('includes.ads.wrapt')
    <!-- breadcrumb -->
	<div class="breadcrumb-section">
		<div class="container ">
			<div class="row">
				<div class="col-12">
					<ol class="breadcrumb">
						<li>
							<a href="{{ url('/') }}"><i class="fa fa-home"></i></a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li><a href="{{ url('/') }}/{{ $content['category_parent'] }}">{{ $header['category_parent'] }}</a></li>
						@if($header['category_child'] != '')
						<li>
							<i class="fa fa-angle-right" style="padding-right:10px;"></i>
							{{ $header['category_child'] }}</li>
						@endif
						<li><i class="fa fa-angle-right"></i> {{ $content['title'] }}</li>
					</ol>
				</div>
			</div><!-- row end -->
		</div><!-- container end -->
	</div>
	<!-- breadcrumb end -->
	<!-- mfunc setPostViews(get_the_ID()); --><!--/mfunc-->
	<section class="main-content pt-0">
		<div class="container pl-0 pr-0">
			<div class="row ts-gutter-30">
				<div class="col-lg-8">
					<div class="single-post">
						<div class="post-header-area">
							<h1 class="post-title title-lg title-only">{{ $header['title'] }}</h1>
							<p>{{ $header['ringkasan'] }}</p>
							<ul class="post-meta">
								<li>
									<i class="fa fa-folder-open"></i>  <a href="{{ url('/') }}/{{ $content['category_parent'] }}">{{ $header['category_parent'] }}</a>
								</li>
								<li><i class="fa fa-clock-o"></i> {{ Helper::indo_datetime($content['date']) }} WIB</li><br>
								<li>
									@if($header['author'] != $header['editor'] )
										Penulis:
										@foreach ($author_slug as $author)
										<a href="@if (ucwords(str_replace('_', ' ', $author)) ==  $header['editor'] ) {{ url('/')}}/author/{{ $header['editor_url'] }} @else {{ url('/') }}/penulis/{{$author}} @endif" class="penulis">{{ ucwords(str_replace('_', ' ', $author)) }}</a> <span style="margin-right:3px; margin-left:3px;padding-right:0;">|</span>
										@endforeach
									@endif
									Editor:  <a href="@if(!empty($header['editor_url'])){{ url('/')}}/author/{{ $header['editor_url'] }} @else https://www.solopos.com/arsip @endif" class="penulis" target="_blank">{{ $header['editor'] }}</a>
								</li>

								<li class="social-share">
									<i class="shareicon fa fa-share"></i>
									<ul class="social-list">
										<li><a data-social="facebook" class="facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a></li>
										<li><a data-social="twitter" class="twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a></li>
										<li><a data-social="whatsapp" class="whatsapp" href="https://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="fa fa-whatsapp"></i></a></li>
										{{-- <li><a data-social="email" class="email" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a></li> --}}
									</ul>
								</li>
							</ul>
						</div><!-- post-header-area end -->
						<div class="post-content-area">
							@if(empty($video))
							<div class="post-media mb-20">
								<a href="{{ $content['image'] }}" class="gallery-popup cboxElement">
									<img loading="lazy" src="{{ $content['image'] }}" alt="@if(!empty($content['caption'])) {{ html_entity_decode($content['caption']) }} @endif" class="img-fluid">
								</a>
								<span>
                                    @if(!empty($content['caption']))
                                        <p>SOLOPOS.COM - {{ html_entity_decode($content['caption']) }}</p>
                                    @else
                                        <p>SOLOPOS.COM - Panduan Informasi dan Inspirasi</p>
                                    @endif
                                </span>
							</div>
							@else
							<div class="post-media mb-20">
								<iframe id="ytplayer" type="text/html" width="640" height="360" src="https://www.youtube.com/embed/{{ $video }}?autoplay=1&origin=https://solopos.com" frameborder="0"></iframe>
							</div>
							@endif

							@include('includes.ads.desktop-sukun')

							@php
							$konten = Helper::konten(htmlspecialchars_decode($content['content'])) ;
							$contents = explode('</p>', $konten);
							$total_p = count(array_filter($contents));

							if($total_p > 13 && $total_p < 25 ):
							$p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 2);
							$p_iklan_2  = array_slice($contents, 4, 5);
							$p_iklan_3  = array_slice($contents, 9, 5);
							$last_p  = array_slice($contents, 14);

							elseif ($total_p > 25 && $total_p < 40 ) :
							$p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 2);
							$p_iklan_2  = array_slice($contents, 4, 6);
							$p_iklan_3  = array_slice($contents, 10, 7);
							$p_iklan_4  = array_slice($contents, 17, 7);
							$last_p  = array_slice($contents, 24);

							elseif ($total_p > 40 ) :
							$p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 4);
							$p_iklan_2  = array_slice($contents, 6, 9);
							$p_iklan_3  = array_slice($contents, 15, 9);
							$p_iklan_4  = array_slice($contents, 24, 9);
							$p_iklan_5  = array_slice($contents, 33, 9);
							$p_iklan_6  = array_slice($contents, 42, 9);
							$p_iklan_7  = array_slice($contents, 51, 9);
							$last_p  = array_slice($contents, 60);

							else :
							$p_promosi  = array_slice($contents, 0, 2);
							$p_iklan_1  = array_slice($contents, 2, 1);
							$p_iklan_2  = array_slice($contents, 3, 4);
							$p_iklan_3  = array_slice($contents, 7, 4);
							$last_p  = array_slice($contents, 11);
							endif;
							@endphp
							<!-- ads top -->

							<!-- ads parallax -->
							{!! implode('</p>', $p_promosi) !!}
							<p><em><strong><span>Promosi</span></span><a href="https://www.solopos.com/{{ $promosi['slug'] }}-{{ $promosi['id'] }}?utm_source=read_promote" title="{{ $promosi['title'] }}" target="_blank" rel="noopener">{{ $promosi['title'] }}</a></strong></em>
							</p>

							{!! implode('</p>', $p_iklan_1) !!}
							@include('includes.ads.desktop-article-1')

							{!! implode('</p>', $p_iklan_2) !!}
							@include('includes.ads.desktop-article-2')

							{!! implode('</p>', $p_iklan_3) !!}
							@include('includes.ads.desktop-article-3')

							@if($total_p > 25 && $total_p < 40 )
							{!! implode('</p>', $p_iklan_4) !!}
							@include('includes.ads.desktop-article-4')

							@elseif($total_p > 40 )
							{!! implode('</p>', $p_iklan_4) !!}
							@include('includes.ads.desktop-article-4')

							{!! implode('</p>', $p_iklan_5) !!}
							@include('includes.ads.desktop-article-5')

							{!! implode('</p>', $p_iklan_6) !!}
							@include('includes.ads.desktop-article-6')

							{!! implode('</p>', $p_iklan_7) !!}
							@include('includes.ads.desktop-article-7')
						
							@endif

							{!! implode('</p>', $last_p) !!}

							<div align="center" style="margin: 20px 0;">
								@include('includes.ads.last-paragraf-article')
							</div>

						</div><!-- post-content-area end -->
						@php if($header['source'] != ''): @endphp
						<b>Sumber:</b> {{ $header['source'] }}
						@php endif @endphp
						{{-- <b>Editor: <a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://www.solopos.com/arsip @endif" class="editor-only">{{ $content['editor'] }}</a></b>  --}}
						@include('includes.ads.internal-promotion')

						@include('includes.ads.video-daylimotion')
						
				        <div class="social-share-btn d-flex align-items-center flex-wrap">
				          SHARE :
				          <a class="btn-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-facebook"></i></a>
				          <a class="btn-twitter" href="https://twitter.com/home?status={{ $header['link'] }}" target="_blank" rel="noopener" title="social"><i class="fa fa-twitter"></i></a>
				          <a class="btn-instagram" href="https://www.instagram.com/koransolopos" title="social"><i class="fa fa-instagram"></i></a>
				          <a class="btn-whatsapp" href="whttps://web.whatsapp.com/send?text=*{{ urlencode($header['title']) }}* | {{ $header['ringkasan'] }} |  _selengkapnya baca di sini_ {{ $header['link'] }}" title="social"><i class="fa fa-whatsapp"></i></a>
				          <a class="btn-quora" href="mailto:?subject=Artikel Menarik dari Solopos.com tentang &amp;body=Artikel ini sangat berguna bagi Anda, silahkan klik link berikut ini "><i class="fa fa-envelope"></i></a>
				        </div>


						<div class="post-footer">
							{{-- <div class="author-box d-flex">
								<div class="author-img">
								<!-- User Thumbnail-->
									@if(!empty($content['avatar']))
									<img src="{{ htmlentities($content['avatar']) }}" alt="Profile">
									@else
									<img src="https://images.solopos.com/2019/10/avatar-solopos-370x370.jpg" alt="Profile">
									@endif
								</div>
								<!-- User Content-->
								<div class="author-info">
									<h6>
										<a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://www.solopos.com/arsip @endif" class="editor-only">{{ $content['editor'] }}</a>
									</h6>

									<p>Jurnalis di Solopos Group. Menulis konten di Solopos Group yaitu Harian Umum Solopos, Koran Solo, Solopos.com.</p>

									<div class="post-list">
										<a href="@if(!empty($data['authors']['editor_url'])){{ url('/')}}/author/{{ $data['authors']['editor_url'] }} @else https://index.solopos.com @endif">Lihat Artikel Saya Lainnya</a>
									</div>
									<div class="author-social">
										<span>Follow Me: </span>
										<a href="https://www.twitter.com/soloposdotcom" target="_blank"><i class="fa fa-twitter"></i></a>
										<a href="https://www.facebook.com/soloposcom" target="_blank"><i class="fa fa-facebook"></i></a>
										<a href="https://www.instagram.com/koransolopos" target="_blank"><i class="fa fa-instagram"></i></a>
										<a href="https://www.youtube.com/channel/UC3jhKGRRBnIi5pfNIE8HOEw" target="_blank"><i class="fa fa-youtube"></i></a>
									</div>

								</div>
							</div> --}}
							<div class="tag-lists">
								<span>Tags: </span>
                                @if(isset($header['arrayTag']) AND empty($content['tag'][0]['term_id']))
									@foreach($header['arrayTag'] as $tag)
									@php
										$tag_name = ucwords($tag);
										$tag_slug = str_replace(' ', '-',$tag);
										$tag_slug = strtolower($tag_slug)
									@endphp
										<a href="{{ url("/tag/{$tag_slug}") }}">{{ $tag_name }}</a>
									@endforeach
								@else
									@foreach($header['arrayTag'] as $tag)
										<a href="{{ url("/tag/{$tag['slug']}") }}">{{ $tag['name'] }}</a>
									@endforeach
        						@endif
							</div><!-- tag lists -->

						</div>

					</div><!-- single-post end -->

					@include('includes.ads.under-article')

                    <!-- <div class="gap-50 d-none d-md-block"></div> -->

					{{-- <!-- realted post start -->
					@if(!empty($related))
					<div class="related-post">
						<h2 class="block-title">
							<span class="title-angle-shap"> {{$relatedtitle}} </span>
						</h2>
						<div class="row">
						@php $rel_loop = 1; @endphp
						@foreach($related as $rel) @if($rel_loop <= 5)

						@php
						$image = $rel['one_call']['featured_list']['source_url'] ?? 'https://www.solopos.com/images/solopos.jpg';
						$title = html_entity_decode($rel['title']['rendered']);
						@endphp
						<!-- related Post-->
						@if ($rel_loop==1)
						<div class="col-md-4">
                            <div class="post-block-style">
                                <div class="post-thumb">
                                    <a class="post-title" href="{{ url("/{$rel['slug']}-{$rel['id']}") }}?utm_source=bacajuga_desktop" title="{{ $rel['title']['rendered'] }}">
                                        <img src="{{$image }}" alt=""{{ $rel['title']['rendered'] }}" style="object-fit: cover; width: 195px; height: 128px;">
                                    </a>
                                    <div class="grid-cat">
                                        <a class="post-cat {{ $rel['one_call']['categories_list'][0]['slug'] }}" href="#">{{ $rel['one_call']['categories_list'][0]['name'] }}</a>
                                    </div>
                                </div>

                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a class="post-title" href="{{ url("/{$rel['slug']}-{$rel['id']}") }}?utm_source=bacajuga_desktop" title="{{ $rel['title']['rendered'] }}">{{ $rel['title']['rendered'] }}</a>
                                    </h2>
                                    <div class="post-meta mb-7 p-0">

                                    </div>
                                </div><!-- Post content end -->
                            </div>
                        </div><!-- col end -->

						@else
						<div class="col-md-4">
                            <div class="post-block-style">
                                <div class="post-thumb">
                                    <a class="post-title" href="{{ url("/{$rel['slug']}-{$rel['id']}") }}?utm_source=bacajuga_desktop" title="{{ $rel['title']['rendered'] }}">
                                        <img src="{{$image }}" alt=""{{ $rel['title']['rendered'] }}" style="object-fit: cover; width: 195px; height: 128px;">
                                    </a>
                                    <div class="grid-cat">
                                        <a class="post-cat {{ $rel['one_call']['categories_list'][0]['slug'] }}" href="#">{{ $rel['one_call']['categories_list'][0]['name'] }}</a>
                                    </div>
                                </div>

                                <div class="post-content">
                                    <h2 class="post-title">
                                        <a class="post-title" href="{{ url("/{$rel['slug']}-{$rel['id']}") }}?utm_source=bacajuga_desktop" title="{{ $rel['title']['rendered'] }}">{{ $rel['title']['rendered'] }}</a>
                                    </h2>
                                    <div class="post-meta mb-7 p-0">

                                    </div>
                                </div><!-- Post content end -->
                           </div>
                        </div>
						@endif @endif
						@php $rel_loop++; @endphp
						@endforeach
						</div>
					</div>
					@endif    --}}


					<!-- realted post end -->

					<div class="related-post">
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Terkait </span>
						</h2>
						<div class="row" id="relposts">
                            <div class="col-md-4"><div class="post-block-style"><div class="post-thumb"><a class="post-title" href="b20-indonesia-angkat-prinsip-bisnis-dan-investasi-berkesinambungan-1427824?utm_source=bacajuga_desktop" title="B20 Indonesia Angkat Prinsip Bisnis dan Investasi Berkesinambungan"><img loading="lazy" src="https://images.solopos.com/2022/09/rsz_1wismilak-150x100.jpg" alt="B20 Indonesia Angkat Prinsip Bisnis dan Investasi Berkesinambungan" style="object-fit: cover; width: 195px; height: 128px;"></a><div class="grid-cat"><a class="post-cat bisnis" href="#">Bisnis</a></div></div><div class="post-content"><h2 class="post-title"><a class="post-title" href="b20-indonesia-angkat-prinsip-bisnis-dan-investasi-berkesinambungan-1427824?utm_source=bacajuga_desktop" title=""> B20 Indonesia Angkat Prinsip Bisnis dan Investasi Berkesinambungan</a></h2><div class="post-meta mb-7 p-0"></div></div></div></div>

							<div class="col-md-4"><div class="post-block-style"><div class="post-thumb"><a class="post-title" href="https://www.semarangpos.com/dseason-premiere-hotel-segudang-fasilitas-dan-paling-recomended-di-jepara-1052538?utm_source=bacajuga_desktop" title="D’Season Premiere, Hotel Segudang Fasilitas dan Paling Recomended di Jepara"><img loading="lazy" src="https://www.semarangpos.com/files/2022/06/season-premier.jpg" alt="D’Season Premiere, Hotel Segudang Fasilitas dan Paling Recomended di Jepara" style="object-fit: cover; width: 195px; height: 128px;"></a><div class="grid-cat"><a class="post-cat bisnis " href="#">News</a></div></div><div class="post-content"><h2 class="post-title"><a class="post-title" href="https://www.semarangpos.com/dseason-premiere-hotel-segudang-fasilitas-dan-paling-recomended-di-jepara-1052538?utm_source=bacajuga_desktop" title=""> D’Season Premiere, Hotel Segudang Fasilitas dan Paling Recomended di Jepara</a></h2><div class="post-meta mb-7 p-0"></div></div></div></div>
                        </div>

					</div>

                    {{-- <div class="related-post">
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Terpopuler </span>
						</h2>
						<div class="row" id="populars">

                        </div>

					</div> --}}

                    <div class="related-post mt-5">
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Lainnya </span>
						</h2>
						<div class="row">
						<ul id="widgetbisnis" class="harjo-widget"></ul>
						<ul id="rekomendasi" class="harjo-widget"></ul>
						@if( date('Y-m-d H:i:s') >= '2022-06-08 00:00:01' && date('Y-m-d H:i:s') <= '2022-09-02 23:59:59')
						<ul id="lock" class="harjo-widget">
						<li><a href="https://jogjapolitan.harianjogja.com/read/2022/08/26/510/1109984/tinjau-sumber-mata-air-pdam-tirtamarta-di-lereng-merapi-penjabat-wali-kota-jogja-jamin-ketersediaan-air-bersih" title="Tinjau Sumber Mata Air PDAM Tirtamarta di Lereng Merapi, Penjabat Wali Kota Jogja Jamin Ketersediaan Air Bersih" target="_blank" rel="noopener noreferrer">Tinjau Sumber Mata Air PDAM Tirtamarta di Lereng Merapi, Penjabat Wali Kota Jogja Jamin Ketersediaan Air Bersih</a></li>
						<li><a href="https://jogjapolitan.harianjogja.com/read/2022/08/11/510/1108649/penataan-pedestrian-senopati-dukung-wisata-malioboro" title="Penataan Pedestrian Senopati Dukung Wisata Malioboro" target="_blank" rel="noopener noreferrer">Penataan Pedestrian Senopati Dukung Wisata Malioboro</a></li>
						</ul>
						@endif
                        </div>
					</div>

					<div class="gap-30 d-none d-md-block"></div>

					<!-- Block Konten Premium -->
					<div class="block style2 text-light mb-20 mt-10">
						<h2 class="block-title">
							<span class="title-angle-shap"> Espos Plus</span>
						</h2>

						<div class="row">
				            @php $pc_loop = 1; @endphp
				            @foreach($premium as $pc) @if($pc_loop <= 5)

				            @if($pc_loop == 1)
							<div class="col-lg-6 col-md-6">
								<div class="post-block-style">
									<div class="post-thumb">
										<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">
											<img loading="lazy" src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; object-position: center; width: 266px; height: 178px;">
										</a>
										<div class="grid-cat">
											<a class="post-cat premium" href="{{ url('/plus') }}">Espos Plus</a>
										</div>
									</div>

									<div class="post-content mt-3">
										<h2 class="post-title title-md">
											{{-- <span class="espos-plus">+ PLUS</span> --}}
											<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">{{ $pc['title'] }}</a>
										</h2>
										<p>{{ $pc['summary'] }}</p>
										<div class="post-meta mb-7">
											<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($pc['author']) {!! $pc['author'] !!} @endif</a></span>
											<span class="post-date"><i class="fa fa-clock-o"></i>{{ Helper::time_ago($pc['date']) }}</span>
										</div>
									</div><!-- Post content end -->
								</div><!-- Post block a end -->
							</div><!-- Col 1 end -->

							<div class="col-lg-6 col-md-6">
								<div class="row ts-gutter-20">
							@endif
							@if( $pc_loop > 1 && $pc_loop <= 5 )

									<div class="col-md-6">
										<div class="post-block-style">
											<div class="post-thumb">
												<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">
													<img loading="lazy" src="{{ $pc['images']['thumbnail'] }}" alt="{{ $pc['title'] }}" style="object-fit: cover; object-position: center; width: 118px; height: 84px;">
												</a>
											</div>

											<div class="post-content">
												<h2 class="post-title mb-2">
													{{-- <span class="espos-plus">+ PLUS</span> --}}
													<a href="{{ url("/{$pc['slug']}-{$pc['id']}") }}" title="{{ $pc['title'] }}">{{ $pc['title'] }}</a>
												</h2>
											</div><!-- Post content end -->
										</div><!-- Post block a end -->
									</div><!-- .col -->

							@endif
							@if($pc_loop == 5)
								</div><!-- .row -->
							</div><!-- Col 2 end -->
						@endif @endif @php $pc_loop++; @endphp @endforeach
						</div><!-- Row end -->
					</div><!-- Block Konten Premium end -->

					<!-- Composite Start -->
					{{-- <div id="M628318ScriptRootC990853"></div>
					<script src="https://jsc.mgid.com/s/o/solopos.com.990853.js" async></script> --}}
					<!-- Composite End  -->

					<div class="gap-50 d-none d-md-block"></div>

					<!-- Begin Dable bottom_new / For inquiries, visit http://dable.io -->
                    {{-- <div id="dablewidget_x7y0b0o6" data-widget_id="x7y0b0o6">
                    <script>
                    (function(d,a,b,l,e,_) {
						if(d[b]&&d[b].q)return;d[b]=function(){(d[b].q=d[b].q||[]).push(arguments)};e=a.createElement(l);
						e.async=1;e.charset='utf-8';e.src='//static.dable.io/dist/plugin.min.js';
                         _=a.getElementsByTagName(l)[0];_.parentNode.insertBefore(e,_);
                        })(window,document,'dable','script');
                        dable('setService', 'solopos.com');
                        dable('sendLogOnce');
                        dable('renderWidget', 'dablewidget_x7y0b0o6');
                    </script>
                    </div> --}}
                    <!-- End bottom_new / For inquiries, visit http://dable.io -->

					<div class="gap-50 d-none d-md-block"></div>

					<div>
						<h2 class="block-title">
							<span class="title-angle-shap"> Berita Terkini </span>
						</h2>
						<div class="row ts-gutter-20 loadmore-frame">
				          @php $no = 0; @endphp
				          @foreach($breakingcat as $posts)
						  @if($no <=15 )
							<div class="col-12 mb-10 content-box">
								<div class="post-block-style">
									<div class="row">
										<div class="col-md-5">
											<div class="post-thumb post-list_feed">
												<img loading="lazy" src="{{ $posts['images']['thumbnail'] }}" alt="{{ html_entity_decode($posts['title']) }}" style="object-fit: cover; object-position: center; height: 167px; width: 250px;" onerror="javascript:this.src='https://www.solopos.com/images/no-thumb.jpg'">
												<a class="post-cat-box {{ $posts['category'] }}" href="{{ url("/{$posts['category']}") }}">{{ $posts['category'] }}</a>
											</div>
										</div>
										<div class="col-md-7 pl-0">
											<div class="post-content">
												<h2 class="post-title title-md">
													<a href="{{ url("/{$posts['slug']}-{$posts['id']}") }}" title="{{ html_entity_decode($posts['title']) }}">{{ html_entity_decode($posts['title']) }}</a>
												</h2>
												<div class="post-meta mb-7">
													<span class="post-author"><a href="#"><i class="fa fa-user"></i> @if($posts['author']) {!! $posts['author'] !!} @endif</a></span>
													<span class="post-date"><i class="fa fa-clock-o"></i> {{ Helper::time_ago($posts['date']) }}</span>
												</div>
												<p>@if($posts['summary']) {{ $posts['summary'] }} @endif</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							@endif
						  @php $no++; @endphp
						  @endforeach
							<div class="col-12 mt-3 align-items-center" style="text-align: center;">
					            <a href="javascript:void(0)" class="btn btn-primary btn-sm load-more" title="Kumpulan Berita">Cek Berita Lainnya</a>
					            <a href="https://www.solopos.com/arsip" class="btn btn-primary btn-sm load-more-arsip" style="display: none;" title="Kumpulan Berita">Arsip Berita</a>
							</div><!-- col end -->
						</div>
					</div><!-- Content Col end -->
				</div><!-- col-lg-8 -->

				<!-- sidebar start -->
				@include('includes.sidebar-category')
				<!-- sidebar end -->
			</div><!-- row end -->
		</div><!-- container end -->
	</section><!-- category-layout end -->

	{{-- <iframe src="https://api.solopos.com/set-view?id={{ $content['id'] }}" style="position: absolute;width:0;height:0;border:0;bottom:0;"></iframe> --}}
    @push('custom-scripts')
    <script>
		$(document).ready(function() {
				$.ajax({ //create an ajax request related post
				type: "GET",
				url: "https://api.solopos.com/api/wp/v2/posts?tags={{ $relatedTags }}&exclude={{ $content['id'] }}&per_page=4", //72325
				dataType: "JSON",
				success: function(data) {
					// console.log(data);
						var relatedPosts = $("#relposts");

						$.each(data, function(i, item) {
							//alert(data[i].item.id);
							// console.log(data[i]['title']['rendered']);
							// relatedPosts.append("<li><a href='" + data[i]['slug'] + "-" + data[i]['id'] + "'>" + data[i]['title']['rendered'] + "</a></li>");

							relatedPosts.append("<div class=\"col-md-4\"><div class=\"post-block-style\"><div class=\"post-thumb\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=bacajuga_desktop\" title=\"" + data[i]['title']['rendered'] + "\"><img loading=\"lazy\" src=\"" + data[i]['one_call']['featured_list']['media_details']['sizes']['thumbnail']['source_url'] +"\" alt=\"" + data[i]['title']['rendered'] + "\" style=\"object-fit: cover; width: 195px; height: 128px;\"></a><div class=\"grid-cat\"><a class=\"post-cat " + data[i]['one_call']['categories_list'][0]['slug'] + " \" href=\"#\">" + data[i]['one_call']['categories_list'][0]['name'] + "</a></div></div><div class=\"post-content\"><h2 class=\"post-title\"><a class=\"post-title\" href=\"" + data[i]['slug'] + "-" + data[i]['id'] + "?utm_source=bacajuga_desktop\" title=\"\"> " + data[i]['title']['rendered'] + "</a></h2><div class=\"post-meta mb-7 p-0\"></div></div></div>");
						});
				}
			});
		});

        $(document).ready(function () {
            $.ajax({
                url: 'https://www.harianjogja.com/rss',
                type: 'GET',
                dataType: "xml"
            }).done(function(xml) {

                $.each($("item", xml), function(i, e) {

                    var postNumber = i + 1 + ". ";

                    var itemURL = ($(e).find("link"));
                    var postURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";

                    // var itemImg = ($(e).find("enclosure"));
                    // var imgUrl = itemImg.getAttribute('url');

                    var itemTitle = ($(e).find("title"));
                    var postTitle = "<a href='" + itemURL.text() + "'>" + itemTitle.text() + "</a>";

                    if(postNumber <= 3) {

                    $("#rekomendasi").append("<li><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\" rel=\"noopener noreferrer\">" + itemTitle.text() +"</a></li>");

                    // $("#rekomendasi").append("<li><div class=\"post-block-style media\"><!--<div class=\"post-thumb\"><a href=\"" + itemURL.text() + "\" title=\"\"><img class=\"img-fluid\" src=\"\" style=\"object-fit: cover; object-position: center; height: 85px; width: 85px;\"></a></div>--><div class=\"post-content media-body\"><h2 class=\"post-title\"><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\">" + itemTitle.text() +"</a></h2><!--<div class=\"post-meta mb-7\"><span class=\"post-date\"><i class=\"fa fa-clock-o\"></i> </span></div>--></div></div></li>");

                    }
                });
            });
        });

        $(document).ready(function () {
             $.ajax({
                 url: 'https://rss.bisnis.com',
                 type: 'GET',
                 dataType: "xml"
             }).done(function(xml) {

                 $.each($("item", xml), function(i, e) {

                     var postNumber = i + 1 + ". ";

                     var itemURL = ($(e).find("link"));
                     var postURL = "<a href='" + itemURL.text() + "'>" + itemURL.text() +"</a>";

                     // var itemImg = ($(e).find("enclosure"));
                     // var imgUrl = itemImg.getAttribute('url');

                     var itemTitle = ($(e).find("title"));
                     var postTitle = "<a href='" + itemURL.text() + "'>" + itemTitle.text() + "</a>";

                     if(postNumber <= 4) {

                     $("#widgetbisnis").append("<li><a href=\"" + itemURL.text() + "\" title=\""+ itemTitle.text() +"\" target=\"_blank\" rel=\"noopener noreferrer\">" + itemTitle.text() +"</a></li>");

                     }
                 });
             });
         });

		// $(document).ready(function() {
		// 		$.ajax({
		// 		xhrFields: {
      	// 			withCredentials: true
   		// 		},
		// 		crossDomain: true,
		// 		cache: false,
		// 		type: "GET",
		// 		url: "https://id.solopos.com/is_login", //72325
		// 		dataType: "JSON",
		// 		success: function(data) {
		// 			//alert(data);
		// 			console.log(data);
		// 		}
		// 	});
		// });

		// $(window).load(function() {
		// 	$.ajax({
		// 		type: "GET",
		// 		url: 'https://api.solopos.com/set-view?id={{ $content['id'] }}',
		// 	});
		// });
    </script>
	<script>
				// function addCredit(){var t=$(".title-only").text(),e=document.location.href,a=$(".penulis").text(),i=$(".editor-only").text(),dt=$(".tgl-terbit").text(),n="",o="";a&&(n="Author : "+a),i&&(o="<br />"+i);var r,l=n.concat(o),d=document.getElementsByTagName("body")[0],p=(r=window.getSelection())+'<br /><br />-------------------------------------------------<br /> Artikel ini telah tayang di <a href="https://www.solopos.com/">Solopos.com</a> dengan judul "'+t+'", Klik selengkapnya di sini: <a href="'+e+'">'+e+"</a> <br />"+'Author: '+a+'<br />Editor: '+i+'<br />Tayang: '+dt+'<br/><br/> Mau Mobil SUV Idaman hanya dengan Rp.328/hari? <a href="https://id.solopos.com/register">Langganan Espos Plus Sekarang</a><br/>Silakan berlangganan dan dapatkan berbagai konten menarik di Espos Plus.<br/><br/>Solopos.com - Panduan Insformasi & Inspirasi',s=document.createElement("div");s.style.position="absolute",s.style.left="-99999px",d.appendChild(s),s.innerHTML=p,r.selectAllChildren(s),window.setTimeout(function(){d.removeChild(s)},0);}
        // document.addEventListener('copy',addCredit);


        document.oncopy = function () {
        var _title = $(".title-only").text(),
            _editor = $(".editor-only").text(),
            _author = $(".penulis").text(),
            _publish = $(".tgl-terbit").text(),
            _href = document.location.href,
                _result,
                _body = document.getElementsByTagName("body")[0];
                _text =
                    (nl2br(_result = window.getSelection())) +
                    '<br /><br /><br /> Baca artikel Solopos.com <b>"' + _title.trim() +
                    '"</b> selengkapnya di sini: <a href="' +
                    _href +
                    '">' +
                    _href +
                    '</a>.'+
                    '<br />Editor  : '+_editor+'<br />Penulis: '+_author+'<br />Publish: '+_publish+
                    '<br/><br/> Mau Mobil SUV Idaman hanya dengan Rp.328/hari? <a href="https://id.solopos.com/register">Langganan Espos Plus Sekarang</a><br/>Silakan berlangganan dan dapatkan berbagai konten menarik di Espos Plus.<br/><br/>Solopos.com - Panduan Insformasi & Inspirasi';

        if (window.clipboardData && window.clipboardData.setData) {
            // IE specific code path to prevent textarea being shown while dialog is visible.
            return clipboardData.setData(p);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {

            var ss = document.createElement("div");

                (ss.style.position = "absolute"),
                (ss.style.left = "-99999px"),
                _body.appendChild(ss),
                (ss.innerHTML = _text),
                _result.selectAllChildren(ss);
                window.setTimeout(function () {
                    _body.removeChild(ss);
                }, 0);
        	}
        };


        function nl2br (str, is_xhtml) {
        if (typeof str === 'undefined' || str === null) {
        	return '';
        }
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        }
	</script>
    @endpush

    @push('tracking-scripts')
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
        url:'https://tf.solopos.com/api/v1/stats/store',
        method:'POST',
        data:{
                post_id:'{{ $content['id'] }}',
                post_title:'{{ $header['title'] }}',
                post_slug:'{{ $content['slug'] }}',
                post_date:'{{ $content['date'] }}',
                post_author:'{{ $header['author'] }}',
                post_editor:'{{ $header['editor'] }}',
                post_category:'{{ $header['category_parent'] }}',
                post_subcategory:'{{ $header['category_child'] }}',
                post_tag:'{!! serialize($content['tag']) !!}',
                post_thumb:'{{ $header['image'] }}',
                post_view_date:'{{ date('Y-m-d') }}',
                domain:'{{ 'solopos.com' }}'
            },
        success:function(response){
            if(response.success){
                console.log(response.message)
            }else{
                console.log(error)
            }
        },
        error:function(error){
            console.log(error)
        }
        });
        </script>
    @endpush


@endsection
